package pw.bowser.minecraft.entities

import net.minecraft.client.Minecraft
import net.minecraft.entity.Entity
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.entity.monster.EntityMob
import net.minecraft.entity.item.EntityItem
import net.minecraft.entity.passive.EntityAnimal
import pw.bowser.minecraft.wrapper.WrapperConversions
import scala.collection.JavaConversions._

/**
 * Manages minecraft `Entity`s
 * Date: 2/8/14
 * Time: 10:57 PM
 */
class EntityToolkit(minecraft: Minecraft) extends AnyRef with WrapperConversions {

  /**
   * Get all entities loaded in the world
   *
   * @return loaded entities
   */
  def getEntities: Array[Entity] = if(minecraft.theWorld != null) minecraft.theWorld.getLoadedEntityList.toArray(Array[Entity]()) else Array()

  /**
   * Get loaded players
   *
   * @return players
   */
  def getPlayers: Array[EntityPlayer] = getEntitiesOfType(classOf[EntityPlayer])

  /**
   * Get loaded hostile entities
   *
   * @return entities
   */
  def getHostileEntities: Array[EntityMob] = getEntitiesOfType(classOf[EntityMob])

  /**
   * Get all loaded passive entities
   *
   * @return loaded passive entitiesq
   */
  def getPassiveEntities: Array[EntityAnimal] = getEntitiesOfType(classOf[EntityAnimal])

  /**
   * Get loaded item entities
   *
   * @return item entities
   */
  def getItemEntities: Array[EntityItem] = getEntitiesOfType(classOf[EntityItem])

  /**
   * Get all entities inheriting the specified entity superclass
   *
   * @param `type`  type of entity
   * @tparam ET     entity type
   * @return        loaded entities matching that type
   */
  def getEntitiesOfType[ET <: Entity](`type`: Class[ET]): Array[ET] = {
    getEntities.takeWhile(ent => `type`.isAssignableFrom(ent.getClass)).asInstanceOf[Array[ET]]
  }

  /**
   * Get entities within a specified distance of an entity
   *
   * @param subject             entity
   * @param surroundingDistance distance
   * @return                    entities within specified distance of entity
   */
  def getEntitiesWithinDistanceOfEntity(subject: Entity, surroundingDistance: Double): Array[Entity] = {
    try {
      minecraft.theWorld.getEntitiesWithinAABBExcludingEntity(subject, subject.boundingBox.expand(surroundingDistance, surroundingDistance, surroundingDistance))
        .map(e => e.asInstanceOf[Entity])
        .filter(e => subject.getDistanceToEntity(e) <= surroundingDistance)
        .toArray
    } catch {
      case np: NullPointerException => Array()
    }
  }

  /**
   * Get entities of the specified type near the specified entity
   * @param `type`          entity type
   * @param near            entity to look near
   * @param withinDistance  distance
   * @tparam ET             entity type
   * @return                entities
   */
  def getEntitiesOfTypeNear[ET <: Entity](`type`: Class[ET], near: Entity, withinDistance: Double): Array[ET] = {
    getEntitiesWithinDistanceOfEntity(near, withinDistance).takeWhile(ent => `type`.isAssignableFrom(ent.getClass))
      .asInstanceOf[Array[ET]]
  }

}
