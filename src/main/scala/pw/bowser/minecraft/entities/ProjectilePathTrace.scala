package pw.bowser.minecraft.entities

import net.minecraft.world.World
import net.minecraft.block.material.Material
import pw.bowser.minecraft.wrapper.WrapperConversions
import net.minecraft.util.{AxisAlignedBB, MovingObjectPosition}
import net.minecraft.entity.Entity
import scala.collection.JavaConversions._
import gnu.trove.list.array.TDoubleArrayList
import net.minecraft.entity.projectile.EntityArrow
import pw.bowser.hysteria.engine.HysteriaServices
import net.minecraft.util.MovingObjectPosition.MovingObjectType

/**
 * Traces the path of an arrow
 *
 * Date: 3/7/14
 * Time: 9:54 AM
 */
object ProjectilePathTrace extends HysteriaServices {

  import WrapperConversions._

  val ACCELERATION_DRY  = 0.99D
  val ACCELERATION_WET  = 0.8D
  val ARROW_HALT_WEDGE  = 0.05000000074505806D

  val ITERATION_LIMIT = 1000

  def apply(world: World, entity: EntityArrow): (Int, Array[Double], Option[MovingObjectPosition]) =
    ProjectilePathTrace(entity.getInitialMotionX, entity.getInitialMotionY, entity.getInitialMotionZ,
                   entity.posX, entity.posY, entity.posZ, world, ProjectileType.ARROW)

  def apply(motionX: Double, motionY: Double, motionZ: Double,
            xInit: Double,   yInit: Double,   zInit: Double, world: World, projectileType: ProjectileType) : (Int, Array[Double], Option[MovingObjectPosition]) = {

    /*
     * Entity updated boilerplate
     */

    val strides = new TDoubleArrayList()

    var (traceX,   traceY,   traceZ)  = (xInit,   yInit,    zInit)
    var (traceMX,  traceMY,  traceMZ) = (motionX, motionY,  motionZ)

    var flying    = true

    var objAt: Option[MovingObjectPosition] = None

    var iterations = 0

    while(flying && iterations <= ITERATION_LIMIT){

      /*
       * Boilerplate
       */

      val arrowBoundingBox  = new AxisAlignedBB(traceX - projectileType.size, traceY - projectileType.size, traceZ - projectileType.size,
                                                traceX + projectileType.size, traceY + projectileType.size, traceZ + projectileType.size)

      // Used for material slowdown calculations
      val blockAtPosition   = world.getBlockAt(traceX.toInt, traceY.toInt, traceZ.toInt)

      if(traceY < -64) flying = false

      var vecNow      = (traceX,           traceY,           traceZ)
      var vecNext     = (traceX + traceMX, traceY + traceMY, traceZ + traceMZ)
          objAt       = Option(world.rayTraceBlocks(vecNow, vecNext))
          vecNow      = (traceX,           traceY,           traceZ)
          vecNext     = (traceX + traceMX, traceY + traceMY, traceZ + traceMZ)

      objAt match {
        case s: Some[MovingObjectPosition] =>
          flying = false
        case None =>

      }

      val entities = world.getEntitiesWithinAABBExcludingEntity( minecraft.thePlayer, arrowBoundingBox.addCoord(traceMX, traceMY, traceMZ).expand(1, 1, 1) )

      entities.foreach({case(entity: Entity) =>
        val hitspecForEntity = entity.boundingBox.expand(0.3, 0.3, 0.3).calculateIntercept(vecNow, vecNext)
        if(entity.canBeCollidedWith
           && hitspecForEntity != null) {
          hitspecForEntity.typeOfHit = MovingObjectType.ENTITY
          objAt = Some(hitspecForEntity)
        }
      })

      objAt match {
        case s: Some[MovingObjectPosition] =>
          flying = false
        case None =>
      }

      traceX  += traceMX
      traceY  += traceMY
      traceZ  += traceMZ

      var accelerationCoefficient =
        if(blockAtPosition.getMaterial == Material.water ||  blockAtPosition.getMaterial == Material.lava) ACCELERATION_WET else ACCELERATION_DRY

      traceMX *= accelerationCoefficient
      traceMY *= accelerationCoefficient
      traceMZ *= accelerationCoefficient
      traceMY -= projectileType.ySinkFactor

      strides.add(traceX)
      strides.add(traceY)
      strides.add(traceZ)

      iterations += 1

    }

    (iterations, strides.toArray, objAt)
  }

  case class ProjectileType(ySinkFactor: Double, yTrimFactor: Double, size: Double)
  object ProjectileType {
    object ARROW extends ProjectileType(0.05, 1.0, 0.3 )
    object OTHER extends ProjectileType(0.03, 0.4, 0.25)
  }

}
