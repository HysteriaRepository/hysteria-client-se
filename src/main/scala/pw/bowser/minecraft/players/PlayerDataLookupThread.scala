package pw.bowser.minecraft.players

import scala.collection.mutable.{Map => MutableMap}

/**
 * Downloads data to a PlayerData object
 *
 * Date: 2/22/14
 * Time: 9:42 PM
 */
class PlayerDataLookupThread(lookup: String, mapTo: MutableMap[String, Any], sources: Array[PlayerInformationSource]) extends Runnable {

  def run(): Unit = {
    val retrievedData = sources.map(source => try source.getDataFor(lookup)).filterNot(_ == null)

    // Merge colliding data
    val mergedData = MutableMap[String, Any]()

    retrievedData.foreach(_.foreach({case(key, value) =>
      if(!mergedData.contains(key))
        mergedData.put(key, value)
    }))

    mergedData.foreach(pair => mapTo.put(pair._1, pair._2))

  }

}
