package pw.bowser.minecraft.wrapper.net.minecraft.world

import net.minecraft.world.WorldProvider

/**
 * Date: 2/14/16
 * Time: 3:49 PM
 */
class HWorldProvider(val mcProvider: WorldProvider) extends AnyVal {

    def lightBrightnessTable: Array[Float] = mcProvider.getLightBrightnessTable

    def setLightBrightnessTable(nVal: Array[Float]): Array[Float] = {
        classOf[WorldProvider].getField("lightBrightnessTable").set(mcProvider, nVal)
        lightBrightnessTable
    }

}
