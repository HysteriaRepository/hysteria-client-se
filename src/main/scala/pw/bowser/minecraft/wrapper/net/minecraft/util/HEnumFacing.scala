package pw.bowser.minecraft.wrapper.net.minecraft.util

import net.minecraft.util.EnumFacing

/**
 * Date: 7/13/16
 * Time: 10:23 AM
 */
class HEnumFacing(val wrapped: EnumFacing) extends AnyVal {

  def index: Int = wrapped.getIndex

  def horizontalIndex: Int = wrapped.getHorizontalIndex

  def frontOffsetX: Int = wrapped.getFrontOffsetX

  def frontOffsetY: Int = wrapped.getFrontOffsetY

  def frontOffsetZ: Int = wrapped.getFrontOffsetZ

  def name: String = wrapped.getName

}

object HEnumFacing {

  val Down  = EnumFacing.DOWN
  val Up    = EnumFacing.UP
  val North = EnumFacing.NORTH
  val South = EnumFacing.SOUTH
  val West  = EnumFacing.WEST
  val East  = EnumFacing.EAST

}