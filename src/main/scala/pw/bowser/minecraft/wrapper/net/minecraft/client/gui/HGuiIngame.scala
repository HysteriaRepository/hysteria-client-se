package pw.bowser.minecraft.wrapper.net.minecraft.client.gui

import net.minecraft.client.gui.{GuiNewChat, GuiIngame}

/**
 * Date: 2/13/16
 * Time: 9:44 PM
 */
final class HGuiIngame(val mcGuiIngame: GuiIngame) extends AnyVal {

    def chat: GuiNewChat = mcGuiIngame.getChatGUI

}

