package pw.bowser.minecraft.wrapper.net.minecraft.network.play.client

import net.minecraft.network.play.client.{C0BPacketEntityAction, C03PacketPlayer}

/**
 * Date: 2/14/16
 * Time: 1:49 PM
 */
trait HClientPacketConversions {

    implicit def HC03PacketPlayer4MC(mcPacket: C03PacketPlayer): HC03PacketPlayer = new HC03PacketPlayer(mcPacket)
    implicit def HC0B4MC(mcPacket: C0BPacketEntityAction): HC0BPacketEntityAction = new HC0BPacketEntityAction(mcPacket)

}
