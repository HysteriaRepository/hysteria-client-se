package pw.bowser.minecraft.wrapper.net.minecraft.client

import net.minecraft.client.Minecraft
import net.minecraft.client.gui.{GuiIngame, FontRenderer}
import net.minecraft.client.multiplayer.WorldClient
import net.minecraft.client.settings.GameSettings

/**
 * Date: 2/13/16
 * Time: 9:33 PM
 */
final class HMinecraft(val mc: Minecraft) extends AnyVal {

    def fontRenderer: FontRenderer = mc.fontRendererObj

    def guiIngame: GuiIngame = mc.ingameGUI

    def gameSettings: GameSettings = mc.gameSettings

    def world: WorldClient = mc.theWorld

}
