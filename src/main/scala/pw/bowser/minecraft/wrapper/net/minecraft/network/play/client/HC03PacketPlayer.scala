package pw.bowser.minecraft.wrapper.net.minecraft.network.play.client

import java.lang.reflect.Field

import net.minecraft.network.play.client.C03PacketPlayer
import net.minecraft.network.play.client.C03PacketPlayer.C06PacketPlayerPosLook
import pw.bowser.minecraft.wrapper.Wrapper

/**
 * Date: 2/14/16
 * Time: 1:21 PM
 */
class HC03PacketPlayer(val mcPacket: C03PacketPlayer) extends AnyVal {

    import Wrapper.getField

    def x: Double = mcPacket.getPositionX

    def y: Double = mcPacket.getPositionY

    def z: Double = mcPacket.getPositionZ

    def yaw: Float = mcPacket.getYaw

    def pitch: Float = mcPacket.getPitch

    def onGround: Boolean = mcPacket.func_149465_i()

    def moving: Boolean = mcPacket.func_149466_j()

    def rotating: Boolean = mcPacket.getRotating

}

object HC03PacketPlayer {

}