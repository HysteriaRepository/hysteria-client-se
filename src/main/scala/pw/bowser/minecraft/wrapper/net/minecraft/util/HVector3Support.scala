package pw.bowser.minecraft.wrapper.net.minecraft.util

import pw.bowser.minecraft.wrapper.MCTypes
import pw.bowser.minecraft.wrapper.MCTypes.{Vector3, HVec3}

/**
 * Date: 2/14/16
 * Time: 3:08 PM
 */
class HVector3Support(val hv3: HVec3) extends AnyVal {

    def x: Double = hv3._1

    def y: Double = hv3._2

    def z: Double = hv3._3

}
trait HVector3SupportConversions {
    implicit def hv3asmcv3(hv3: HVec3): MCTypes.Vector3 = new MCTypes.Vector3(hv3._1, hv3._2, hv3._3)

    implicit def unwrapmcv3d(mcv3: MCTypes.Vector3): HVec3 = (mcv3.zCoord, mcv3.yCoord, mcv3.zCoord)
}
object HVector3Support extends AnyRef with HVector3SupportConversions
