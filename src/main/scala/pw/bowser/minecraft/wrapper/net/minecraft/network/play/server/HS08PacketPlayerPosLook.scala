package pw.bowser.minecraft.wrapper.net.minecraft.network.play.server

import net.minecraft.network.play.server.S08PacketPlayerPosLook

/**
 * Date: 2/14/16
 * Time: 2:43 PM
 */
class HS08PacketPlayerPosLook(val mcPacket: S08PacketPlayerPosLook) extends AnyVal {

    import java.util.{Set => JSet}

    def x: Double = mcPacket.func_148932_c()

    def y: Double = mcPacket.func_148928_d()

    def z: Double = mcPacket.func_148933_e()

    def yaw: Float = mcPacket.func_148931_f()

    def pitch: Float = mcPacket.func_148930_g()

    def flags: JSet[S08PacketPlayerPosLook.EnumFlags] =
      mcPacket.func_179834_f().asInstanceOf[JSet[S08PacketPlayerPosLook.EnumFlags]]

}

object HS08PacketPlayerPosLook {
  object Flags {
    val X = S08PacketPlayerPosLook.EnumFlags.X
    val Y = S08PacketPlayerPosLook.EnumFlags.Y
    val Z = S08PacketPlayerPosLook.EnumFlags.Z

    val YRot = S08PacketPlayerPosLook.EnumFlags.Y_ROT
    val XRot = S08PacketPlayerPosLook.EnumFlags.X_ROT
  }
}
