package pw.bowser.minecraft.wrapper.net.minecraft.network.play.server

import net.minecraft.network.play.server.S12PacketEntityVelocity

class HS12PacketEntityVelocity(val mcPacket: S12PacketEntityVelocity) extends AnyVal {

    def entityID: Int = mcPacket.func_149412_c()

    def motionX: Int  = mcPacket.func_149411_d()

    def motionY: Int  = mcPacket.func_149410_e()

    def motionZ: Int  = mcPacket.func_149409_f()

}
