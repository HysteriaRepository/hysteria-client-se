package pw.bowser.minecraft.wrapper.net.minecraft.render.entity

import net.minecraft.client.renderer.entity.RenderManager

object HRenderManager {

    def renderPosX: Double = classOf[RenderManager].getField("renderPosX").getDouble(null)

    def renderPosY: Double = classOf[RenderManager].getField("renderPosY").getDouble(null)

    def renderPosZ: Double = classOf[RenderManager].getField("renderPosZ").getDouble(null)

}
