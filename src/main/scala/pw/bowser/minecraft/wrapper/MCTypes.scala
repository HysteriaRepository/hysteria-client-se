package pw.bowser.minecraft.wrapper

import _root_.net.minecraft.client.entity.{EntityOtherPlayerMP, EntityPlayerSP}
import _root_.net.minecraft.entity.player.EntityPlayer
import _root_.net.minecraft.util.Vec3

/**
 * Date: 2/13/16
 * Time: 10:32 PM
 */
object MCTypes {

    type Vector3              = Vec3
    type HVec3                = (Double, Double, Double)
    type Packet               = _root_.net.minecraft.network.Packet
    type Player               = EntityPlayer
    type LocalPlayer          = EntityPlayerSP
    type RemotePlayer         = EntityOtherPlayerMP
    type EnumFacing           = _root_.net.minecraft.util.EnumFacing
    type MovingObjectPosition = _root_.net.minecraft.util.MovingObjectPosition

}
