package pw.bowser.minecraft.wrapper

import java.lang.reflect.{Method, Field}
import java.util.logging.{Level, Logger}

import pw.bowser.hysteria.engine.{HysteriaBroker, Hysteria}

/**
 * Date: 2/14/16
 * Time: 1:40 PM
 */
object Wrapper {

  private val logger: Logger = HysteriaBroker.hysteria.log

  private def handleNSMOnClass(tClass: Class[_], name: String, exception: ReflectiveOperationException): Unit = {
    logger.log(
      Level.SEVERE,
      s"FATAL: Wrapper class attempted to access non-existent field $name on class $tClass",
      exception
    )
  }


  def getField[T <: Any : Manifest](fName: String): Field = {
    val targetClass = manifest[T].runtimeClass

    val field = try {
      targetClass.getDeclaredField(fName)
    } catch {
      case _: NoSuchFieldException =>
        logger.finest("Wrapper.getField: Class::getDeclaredField failed, trying Class::getField before failing")
        try {
          targetClass.getField(fName)
        } catch {
          case nsf: NoSuchFieldException =>
            handleNSMOnClass(targetClass, fName, nsf)
            throw nsf;
        }
    }

    field.setAccessible(true)
    field
  }

  def getMethod[T <: Any : Manifest](mName: String, params: Class[_]*): Method = {
    val targetClass = manifest[T].runtimeClass

    val method = try {
      targetClass.getDeclaredMethod(mName, params: _*)
    } catch {
      case _: NoSuchMethodException =>
        try {
          targetClass.getMethod(mName, params: _*)
        } catch {
          case nme: NoSuchMethodException =>
            handleNSMOnClass(targetClass, mName, nme)
            throw nme
        }
    }

    method.setAccessible(true)
    method
  }

  def invokeMethod[C, ReturnType](mName: String, params: Class[_]*)
                                 (instance: C, paramValues: Any*)
                                 (implicit manifest: Manifest[C]): ReturnType = {
      val method = getMethod[C](mName, params: _*)(manifest)

      //
      //  m    m   mm     mmm  m    m          mm   m      mmmmmm mmmmm mmmmmmm
      //  #    #   ##   m"   " #  m"           ##   #      #      #   "#   #
      //  #mmmm#  #  #  #      #m#            #  #  #      #mmmmm #mmmm"   #
      //  #    #  #mm#  #      #  #m          #mm#  #      #      #   "m   #
      //  #    # #    #  "mmm" #   "m        #    # #mmmmm #mmmmm #    "   #
      //
      method.invoke(instance, paramValues.asInstanceOf[Seq[Object]]: _*).asInstanceOf[ReturnType]
  }

}
