package pw.bowser.fishbans

import scala.util.parsing.json.{JSONArray, JSONObject}
import pw.bowser.util.JSONAt
import scala.collection.mutable.{Map => MutableMap}
import java.net.SocketTimeoutException

/**
 * Provides FishBans lookup functionality
 *
 * Date: 2/23/14
 * Time: 12:47 AM
 */
object FishBans {

  val BASE_URL        = "http://api.fishbans.com"

  val PLAYER_BANS     = s"$BASE_URL/bans/%s"

  val PLAYER_STATS    = s"$BASE_URL/stats/%s"

  val PLAYER_ID       = s"$BASE_URL/uuid/%s"

  val PLAYER_HISTORY  = s"$BASE_URL/history/%s"

  private val knownPlayers  = MutableMap[String, FishBansProfile]()

  def profileOf(player: String): Option[FishBansProfile] = {

    if(knownPlayers.contains(player.toLowerCase)) knownPlayers(player.toLowerCase)


    val profileRaw = try JSONAt(PLAYER_BANS.format(player)) catch {
      case t: SocketTimeoutException =>
        None
    }

    val historyRaw = try JSONAt(PLAYER_HISTORY.format(player)) catch {
      case s: SocketTimeoutException =>
        None
    }

    if(historyRaw == None || profileRaw == None) return None

    (profileRaw.get, historyRaw.get) match {
      case(nObject: JSONObject, hObject: JSONObject) =>
        val profileResult = nObject.obj
        val historyResult = hObject.obj

        // Check success
        // Ignore history success
        if (profileResult("success") != true) return None

        // Profile Information
        //--------------------------------------------------------------------------------------------------------------

        // Service info
        val profileSpec = profileResult("bans").asInstanceOf[JSONObject].obj

        // Username per minecraft
        val userName    = profileSpec("username").asInstanceOf[String]

        // UUID per minecraft
        val uuid        = profileSpec("uuid").asInstanceOf[String]

        // Map of services
        val serviceSpec = profileSpec("service").asInstanceOf[JSONObject].obj

        // Ban Information
        //--------------------------------------------------------------------------------------------------------------

        // Map of service names to ban data
        val userBans    = serviceSpec.map({case(service, serviceData: JSONObject) =>

          val serviceSpec = serviceData.obj

          val bansReasons = serviceSpec("ban_info") match {
            case obj: JSONObject => obj.obj.map(e=>(e._1, e._2.toString)).toMap
            case JSONArray|_     => Map[String, String]()
          }

          (service, new BanData(userName, uuid, service, bansReasons))
        }).toMap

        // Name History
        //--------------------------------------------------------------------------------------------------------------

        val nameHistory = historyResult("success") match {
          case true   =>
            historyResult("data").asInstanceOf[JSONObject].obj("history").asInstanceOf[JSONArray].list.map(_.toString).toArray
          case false  => Array[String]()
        }

        val profile   = new FishBansProfile(userName, uuid, userBans, nameHistory)
        knownPlayers.put(player.toLowerCase, profile)
        Some(profile)
      case _ => None
    }
  }

  def nameHistoryFor(player: String): Array[String] = {
    profileOf(player) match {
      case profile: Some[FishBansProfile] => profile.get.nameHistory
      case None                           => Array[String]()
    }
  }

  def isBanned(player: String): Boolean = {
    profileOf(player) match {
      case profile: Some[FishBansProfile] => profile.get.totalBans > 0
      case None                           => false
    }
  }

  def unCache(player: String) = knownPlayers.remove(player.toLowerCase)

  def clearCache() = knownPlayers.clear()

}
