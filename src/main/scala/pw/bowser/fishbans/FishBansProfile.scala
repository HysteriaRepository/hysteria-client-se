package pw.bowser.fishbans

/**
 * A user's fishbans profile
 * Date: 2/23/14
 * Time: 1:04 AM
 */
class FishBansProfile private[fishbans] (val username: String,  val userId: String,
                                         val serviceBans: Map[String, BanData],
                                         val nameHistory: Array[String]) {
  def hasAlts: Boolean = totalAlts > 0

  def totalAlts: Int = nameHistory.size

  def isBanned: Boolean = totalBans > 0

  def totalBans: Int = serviceBans.foldRight(0)({
    case (bData, count) => count + bData._2.banCount
  })

  def bansFor(service: String): BanData = serviceBans(service)

  def apply(service: String): BanData = bansFor(service)
}
