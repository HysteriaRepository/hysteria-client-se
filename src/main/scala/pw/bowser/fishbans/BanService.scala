package pw.bowser.fishbans

/**
 * FishBans ban services
 * Date: 2/23/14
 * Time: 1:01 AM
 */
object BanService {

  val MCBANS      = "mcbans"

  val MCBOUNCER   = "mcbouncer"

  val MCBLOCKIT   = "mcblockit"

  val MINEBANS    = "minebans"

  val GLIZER      = "glizer"

}
