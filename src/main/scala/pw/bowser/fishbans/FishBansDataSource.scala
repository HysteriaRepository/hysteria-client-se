package pw.bowser.fishbans

import pw.bowser.minecraft.players.PlayerInformationSource

/**
 * Date: 2/23/14
 * Time: 5:59 PM
 */
object FishBansDataSource extends PlayerInformationSource {
  /**
   * Get data about a player
   *
   * @param name  name of the player
   * @return      configuration data
   */
  def getDataFor(name: String): Map[String, Any] = FishBans.profileOf(name) match {
    case someProfile: Some[FishBansProfile] =>
      val profile = someProfile.get

      implicit def int2string(int: Int): String = int.toString

      Map(
        "fbans" -> profile,
        "fbans.bans"            -> profile.totalBans,
        "fbans.bans.mcbans"     -> profile(BanService.MCBANS).banCount,
        "fbans.bans.mcblockit"  -> profile(BanService.MCBLOCKIT).banCount,
        "fbans.bans.mcbouncer"  -> profile(BanService.MCBOUNCER).banCount,
        "fbans.bans.minebans"   -> profile(BanService.MINEBANS).banCount,
        "fbans.bans.glizer"     -> profile(BanService.GLIZER).banCount,
        "fbans.alts"            -> profile.nameHistory.length,
        "fbans.alts.names"      -> profile.nameHistory.mkString(",")
      )
    case None =>
      Map(
        "fbans.bans" -> "?",
        "fbans.bans.mcbans" -> "?",
        "fbans.bans.mcblockit" -> "?",
        "fbans.bans.mcbouncer" -> "?",
        "fbans.bans.minebans" -> "?",
        "fbans.bans.glizer" -> "?",
        "fbans.alts" -> "?",
        "fbans.alts.names" -> ""
      )
  }
}
