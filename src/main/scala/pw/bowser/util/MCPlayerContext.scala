package pw.bowser.util

import pw.bowser.hysteria.module.internal.util.friends.PlayerDataContext
import net.minecraft.entity.player.EntityPlayer
import pw.bowser.hysteria.engine.{HysteriaServices, HysteriaBroker}
import pw.bowser.minecraft.wrapper.WrapperConversions._

/**
 * Evaluation context with MC-level knowledge of a player
 *
 * Date: 2/24/14
 * Time: 7:33 PM
 */
class MCPlayerContext(pl: EntityPlayer)
  extends PlayerDataContext(HysteriaBroker.hysteria.playerData.lookupPlayer(pl.commandSenderName)) with HysteriaServices {

  prop("player", new ContextJail)

  /**
   * MVEL Execution context for the player.
   */
  private class ContextJail {
    def distance(): Int = distanceF().toInt

    def distanceF(): Float = minecraftToolkit.thePlayer.getDistanceToEntity(pl)

    def vanished(): Boolean = (pl.getDataWatcher.getWatchableObjectByte(0) & 1 << 5) != 0

    def sneaking(): Boolean = pl.isSneaking

    def blocking(): Boolean = pl.isBlocking

    def swinging(): Boolean = pl.isUsingItem

    def onFire(): Boolean = pl.isBurning

    def onLadder(): Boolean = pl.isOnLadder

    def swimming(): Boolean = pl.isWet

    def posXD(): Double = pl.posX

    def posYD(): Double = pl.posY

    def posZD(): Double = pl.posZ

    def posX(): Int = posXD().toInt

    def posY(): Int = posYD().toInt

    def posZ(): Int = posZD().toInt
  }

}

import scala.collection.mutable.{Map => MutableMap}

object MCPlayerContext {
  private val knownPlayerContexts = MutableMap[String, MCPlayerContext]()

  def apply(pl: EntityPlayer): MCPlayerContext = knownPlayerContexts.get(pl.commandSenderName) match {
    case d: Some[MCPlayerContext] => d.get
    case None =>
      knownPlayerContexts.put(pl.commandSenderName, new MCPlayerContext(pl))
      apply(pl)
  }

  def flushDataCached() = knownPlayerContexts.clear()
}
