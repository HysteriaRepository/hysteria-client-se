package pw.bowser.util

import scala.collection.mutable.{Map => MutableMap}
import scala.collection.JavaConversions._
import java.io.{Serializable => JSerializable}
import org.mvel2.MVEL

/**
 * MVEL Transformer with compiled expression caching
 * Date: 2/23/14
 * Time: 7:15 PM
 */
object MVFLex {

  private val expressionCache = MutableMap[String, JSerializable]()

  def apply(toLex: String, tokens: Map[String, AnyRef]): String = MVEL.executeExpression(getExpression(toLex), mapAsJavaMap(tokens)).toString

  def apply(toLex: String, context: Context): String = MVEL.executeExpression(getExpression(toLex), context.executionContext, mapAsJavaMap(context.contextVars)).toString

  def getExpression(exprSource: String): JSerializable = {
    if(!expressionCache.contains(exprSource)){
      expressionCache.put(exprSource, MVEL.compileExpression(exprSource))
    }

    expressionCache(exprSource)
  }

  def flushExpression(expr: String) = expressionCache.remove(expr)

  def flushExpressionCache() = expressionCache.clear()

  trait Context {

    val contextVars = MutableMap[String, AnyRef]()

    protected final def prop(name: String, value: AnyRef) = contextVars.put(name, value)

    def executionContext: AnyRef = null

  }

}
