package pw.bowser.util

import pw.bowser.hysteria.chat.Formatting
import pw.bowser.hysteria.engine.HysteriaBroker

/**
 * Standard formatting context for MVFlex chat parsing
 * Date: 2/23/14
 * Time: 8:36 PM
 */
class ContextBase extends MVFLex.Context {

  prop("c_black", Formatting.BLACK)
  prop("c_navy", Formatting.DARK_BLUE)
  prop("c_darkGreen", Formatting.DARK_GREEN)
  prop("c_teal", Formatting.TEAL)
  prop("c_darkRed", Formatting.DARK_RED)
  prop("c_purple", Formatting.PURPLE)
  prop("c_gold", Formatting.GOLD)
  prop("c_grey", Formatting.GREY)
  prop("c_darkGrey", Formatting.DARK_GREY)
  prop("c_blue", Formatting.BLUE)
  prop("c_green", Formatting.BRIGHT_GREEN)
  prop("c_cyan", Formatting.AQUA)
  prop("c_red", Formatting.RED)
  prop("c_pink", Formatting.PINK)
  prop("c_yellow", Formatting.YELLOW)
  prop("c_white", Formatting.WHITE)
  prop("c_bold", Formatting.BOLD)
  prop("c_underline", Formatting.UNDERLINE)
  prop("c_italic", Formatting.ITALIC)
  prop("c_strike", Formatting.STRIKEOUT)
  prop("c_junk", Formatting.OBFUSCATED)
  prop("c_reset", Formatting.RESET)

  prop("hysteria", new HysteriaJail)

  class HysteriaJail {

    def info(): VersionData = HysteriaBroker.hysteria.versionData

  }

}