package pw.bowser.util

/**
 * Contains software versioning information
 * Date: 2/21/14
 * Time: 4:36 PM
 */
trait VersionData {

  /**
   * Get the version string for the software
   *
   * @return version
   */
  def version: String

  /**
   * Get the VCS revision id for the software, if applicable.
   *
   * @return revision
   */
  def vcsRevision: String

  /**
   * Get the VCS revision time for the software, if applicable
   *
   * @return revision date
   */
  def vcsRevisionTime: String

  /**
   * Get the time at which the software was built
   *
   * @return build time
   */
  def buildTime: String

}
