package pw.bowser.hysteria.commands

import pw.hysteria.input.dashfoo.command.FlagDispatcher
import java.util

import scala.collection.JavaConversions._

/**
 * FlagDispatcher that will not throw an error for flags that do not exist
 *
 * Date: 2/6/14
 * Time: 2:15 PM
 */
class NonStrictFlagDispatcher extends FlagDispatcher {

  override def dispatch(flagmap: util.Map[String, Array[String]]): Unit = flagmap.foreach({pair =>
      if(getFlags.containsKey(pair._1))
        getFlags.get(pair._1).invokeFlag(pair._2:_*)
    })

}
