package pw.bowser.hysteria.logging

import java.util.logging.{Level, SimpleFormatter, FileHandler, Logger}
import java.io.File

/**
 * Contains information about storing log information and such
 *
 * Date: 1/24/14
 * Time: 11:51 PM
 */
class LoggingToolkit(val storageFolder: File) {

  final def getLogger(name: String): Logger = getLogger(name, null)

  def getLogger(name: String, bundle: String): Logger = {
    val log: Logger = Logger.getLogger(name, bundle)
    val file: FileHandler = new FileHandler(new File(storageFolder, s"$name.log").getAbsolutePath, true)
    file.setFormatter(new SimpleFormatter)
    file.setLevel(Level.ALL)

    log.addHandler(file)

    log
  }

}
