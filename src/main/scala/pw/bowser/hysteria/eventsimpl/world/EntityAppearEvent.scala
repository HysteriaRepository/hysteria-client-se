package pw.bowser.hysteria.eventsimpl.world

import pw.bowser.hysteria.events.PersistentEvent
import net.minecraft.entity.Entity

/**
 * Event that is published when an entity appears in the loaded world
 *
 * Date: 2/13/14
 * Time: 9:27 PM
 */
class EntityAppearEvent(entity: Entity) extends PersistentEvent {

  def getName: String = s"EntityAppear(entity = $entity)"

}
