package pw.bowser.hysteria.eventsimpl.world

import pw.bowser.hysteria.events.Event

/**
 * Fired when the player clicks a block.
 * Date: 2/16/14
 * Time: 12:41 PM
 */
class PlayerClickBlockEvent(val blockX: Int,
                            val blockY: Int,
                            val blockZ: Int,
                            val side: Int) extends Event {
  def getName: String = "BlockClick"
}
