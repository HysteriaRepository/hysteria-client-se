package pw.bowser.hysteria.eventsimpl.world

import pw.bowser.hysteria.events.Event
import net.minecraft.entity.Entity

/**
  * Date: 3/21/14
  * Time: 3:16 PM
  */
class ClientPlayerInteractEvent(var target: Entity) extends Event {
   def getName: String = "PlayerInteractOther"
 }
