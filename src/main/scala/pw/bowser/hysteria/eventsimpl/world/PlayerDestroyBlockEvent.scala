package pw.bowser.hysteria.eventsimpl.world

import pw.bowser.hysteria.events.Event

/**
 * Broadcast when a player destroys a block
 * Date: 2/16/14
 * Time: 2:21 PM
 */
class PlayerDestroyBlockEvent(val blockX: Int,
                              val blockY: Int,
                              val blockZ: Int,
                              val side: Int) extends Event {

  def getName: String = "DestroyBlock"
}
