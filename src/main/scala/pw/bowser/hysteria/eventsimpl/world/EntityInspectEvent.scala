package pw.bowser.hysteria.eventsimpl.world

import net.minecraft.entity.Entity
import pw.bowser.hysteria.events.Event

/**
 * Pushed by `EntityObserver` thread every time it collects entities for research
 * Date: 2/15/14
 * Time: 8:15 PM
 */
class EntityInspectEvent(val entity: Entity) extends Event {
  def getName: String = "EntityInspect"
}
