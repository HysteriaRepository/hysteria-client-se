package pw.bowser.hysteria.eventsimpl.world.worldaccess

import net.minecraft.entity.Entity
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.util.BlockPos
import net.minecraft.world.IWorldAccess
import pw.bowser.hysteria.events.EventHub

/**
 * Date: 2/14/16
 * Time: 2:22 PM
 */
final class HWorldAccessWatcher(eventHub: EventHub) extends AnyRef with IWorldAccess {

  override def onEntityAdded(entityIn: Entity): Unit = eventHub.broadcast(EntityAddedToWorldEvent(entityIn))

  override def onEntityRemoved(entityIn: Entity): Unit = eventHub.broadcast(EntityRemovedFromWorldEvent(entityIn))

  // SHIT WE WILL PROBABLY NEVER NEED BELOW THIS LINE

  override def markBlockRangeForRenderUpdate(x1: Int, y1: Int, z1: Int, x2: Int, y2: Int, z2: Int): Unit = ()

  override def sendBlockBreakProgress(breakerId: Int, pos: BlockPos, progress: Int): Unit = ()

  override def notifyLightSet(pos: BlockPos): Unit = ()

  override def playSound(soundName: String, x: Double, y: Double, z: Double, volume: Float, pitch: Float): Unit = ()



  override def playSoundToNearExcept(except: EntityPlayer, soundName: String, x: Double, y: Double, z: Double,
                                     volume: Float, pitch: Float): Unit = ()

  override def markBlockForUpdate(pos: BlockPos): Unit = ()

  // spawnparticle
  override def func_180442_a(var1: Int, var2: Boolean, var3: Double, var5: Double, var7: Double, var9: Double,
                             var11: Double, var13: Double, var15: Int*): Unit = ???

  override def func_180439_a(var1: EntityPlayer, var2: Int, var3: BlockPos, var4: Int): Unit = ???

  override def func_174961_a(var1: String, var2: BlockPos): Unit = ???

  override def func_180440_a(var1: Int, var2: BlockPos, var3: Int): Unit = ???
}
