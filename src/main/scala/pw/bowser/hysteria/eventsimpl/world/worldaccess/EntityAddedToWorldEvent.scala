package pw.bowser.hysteria.eventsimpl.world.worldaccess

import net.minecraft.entity.Entity
import pw.bowser.hysteria.events.PersistentEvent

/**
 * Date: 2/14/16
 * Time: 2:21 PM
 */
case class EntityAddedToWorldEvent(entity: Entity) extends PersistentEvent {

    override def getName: String = "EntityAddedToWorld ($entity)"
}
