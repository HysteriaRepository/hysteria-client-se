package pw.bowser.hysteria.eventsimpl.network

/**
 * Event fired when an event is received
 * Date: 3/13/14
 * Time: 10:11 PM
 */
class PacketReceiveEvent(thePacket: PacketEvent.Packet) extends PacketEvent(thePacket) {
  def getName: String = "PacketReceive"
}
