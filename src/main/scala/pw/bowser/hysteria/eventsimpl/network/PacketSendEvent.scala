package pw.bowser.hysteria.eventsimpl.network

/**
 * Date: 3/13/14
 * Time: 10:23 PM
 */
class PacketSendEvent(thePacket: PacketEvent.Packet) extends PacketEvent(thePacket) {
  def getName: String = "PacketSend"
}
