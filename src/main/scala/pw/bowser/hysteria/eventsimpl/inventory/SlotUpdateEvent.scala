package pw.bowser.hysteria.eventsimpl.inventory

import net.minecraft.item.ItemStack
import net.minecraft.inventory.Container

/**
 * Describes a slot changing from one item to another
 * Date: 3/14/14
 * Time: 11:00 PM
 */
class SlotUpdateEvent(aContainer:   Container,
                      slot:         Int,
                      val oldStack: Option[ItemStack],
                      var newStack: Option[ItemStack]) extends SlotEvent(aContainer, slot) {
  def getName: String = "SlotChangeEvent"
}
