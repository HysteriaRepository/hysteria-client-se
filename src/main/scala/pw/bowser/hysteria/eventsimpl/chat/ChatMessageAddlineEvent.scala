package pw.bowser.hysteria.eventsimpl.chat

import pw.bowser.hysteria.events.Event

/**
 * Date: 2/3/14
 * Time: 10:33 PM
 */
class ChatMessageAddlineEvent(text: String) extends Event {

  def getText: String = text

  def getName: String = "chat.add"

}
