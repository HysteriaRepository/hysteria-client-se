package pw.bowser.hysteria.eventsimpl.chat

import pw.bowser.hysteria.events.Event
import net.minecraft.util.IChatComponent

/**
 * Date: 2/3/14
 * Time: 10:32 PM
 */
class ChatMessageReceiveEvent(var chatComponent: IChatComponent, var flags: Byte) extends Event {

  def message: String = chatComponent.getFormattedText

  def getName: String = "chat.receive"

  lazy val isSystemChat = (flags == 1) | (flags == 2)

}
