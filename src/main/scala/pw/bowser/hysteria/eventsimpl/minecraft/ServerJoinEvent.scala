package pw.bowser.hysteria.eventsimpl.minecraft

import pw.bowser.hysteria.events.Event

import net.minecraft.client.multiplayer.{ServerData => MCServerData}

/**
 * Date: 3/24/14
 * Time: 10:12 PM
 */
class ServerJoinEvent(var serverData: MCServerData) extends Event {
  def getName: String = "ServerJoin"

  /**
   * Set server data
   * @param sData server data
   */
  def setServerData(sData: MCServerData): Unit = serverData = sData
}
