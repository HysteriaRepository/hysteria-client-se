package pw.bowser.hysteria.eventsimpl.minecraft.player

import pw.bowser.hysteria.events.PersistentEvent

/**
 * Sent after a motion update
 * Date: 5/16/14
 * Time: 11:01 PM
 */
class PostMotionUpdate extends PersistentEvent {
  override def getName: String = "PostMotionUpdate"
}
