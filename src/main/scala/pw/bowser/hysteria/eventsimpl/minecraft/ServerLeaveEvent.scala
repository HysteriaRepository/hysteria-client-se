package pw.bowser.hysteria.eventsimpl.minecraft

import pw.bowser.hysteria.events.Event

import net.minecraft.client.multiplayer.{ServerData => MCServerData}
import pw.bowser.hysteria.eventsimpl.minecraft.ServerLeaveEvent.DisconnectReason

/**
 * Date: 3/24/14
 * Time: 10:13 PM
 */
class ServerLeaveEvent(val serverData: MCServerData, val reason: ServerLeaveEvent.DisconnectReason.DisconnectReason) extends Event {
  def getName: String = "ServerLeave"

  override def cancel(): Unit = {
    if(reason == DisconnectReason.VOLUNTARY) super.cancel()
    else throw new IllegalStateException("Server disconnects cannot be cancelled")
  }
}

object ServerLeaveEvent {
  object DisconnectReason extends Enumeration {
    type DisconnectReason = Value

    val VOLUNTARY = Value("voluntary")
    val SERVER    = Value("server")
  }
}
