package pw.bowser.hysteria.spm

import pw.bowser.spm.Instantiator
import com.google.inject.Injector

/**
 * Use Guice to instantiate plugin classes
 * The advantages of doing this include:
 * - Allowing the plugin to "request" facilities in the constructor, like the extension manager
 * - Member injection
 */
final class GuiceInstantiator(injector: Injector) extends Instantiator {

  /**
   * Call to create an instance of `clazz`
   * @param clazz       class
   * @param parameters  parameters (if applicable, implementation may not use these)
   * @tparam T          object type (inferred from Clazz)
   * @return            an instance of clazz
   */
  def instantiate[T](clazz: Class[T], parameters: AnyRef*): T = injector.getInstance(clazz) // It's just that easy

}
