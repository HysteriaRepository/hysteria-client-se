package pw.bowser.hysteria.chat

import pw.hysteria.input.dashfoo.command.parsing.CommandRegexProvider
import java.util.regex.Pattern
import pw.bowser.libhysteria.config.Configuration

/*
 * TODO merge to DashFoo
 */

/**
 * Date: 2/19/14
 * Time: 12:33 PM
 */
class NoHandleRegexProvider(conf:   Configuration,
                            pFlag:  String,
                            pEsc:   String,
                            pQuot:  String) extends CommandRegexProvider {

  implicit def string2pattern(string: String): Pattern  = Pattern.compile(string)
  implicit def pattern2string(pattern: Pattern): String = pattern.pattern()

  def getExplosionPattern: Pattern            = s"(?<!$escSeq)(?:$quoteSeq)(.*?)(?<!$escSeq)$quoteSeq|(?:[^\\s]+)"

  def getFlagPattern: Pattern                 = s"^$flagSeq(\\w*).*"

  def getHandlePattern: Pattern               = "^(\\w*)"

  def postProcessString(post: String): String = post

  def isCommand(command: String): Boolean     = command matches getHandlePattern

  def getHandle(command: String): String      = {
    val matcher = getHandlePattern.matcher(command)
    if(matcher.find()) matcher.group(1) else null
  }

  def isFlag(flag: String): Boolean           = flag.startsWith(conf.getString(pFlag))

  def getFlag(flag: String): String           = {
    val matcher = getFlagPattern.matcher(flag)
    val flagF = if(matcher.find) matcher.group(1) else null
    flagF
  }

  private def quoteSeq: String  = Pattern.quote(conf.getString(pQuot))
  private def escSeq: String    = Pattern.quote(conf.getString(pEsc))
  private def flagSeq: String   = Pattern.quote(conf.getString(pFlag))

}
