package pw.bowser.hysteria.chat

import pw.hysteria.input.dashfoo.command.parsing.{CommandFormattingProvider, CommandRegexProvider}
import java.util.regex.Pattern
import pw.bowser.libhysteria.config.Configuration

/**
 * Provides formatting set by the user as the command configuration
 *
 * Date: 1/19/14
 * Time: 11:57 AM
 */
class CommandFormatFromConfig(config: Configuration,
                              flagPth: String,
                              escPath: String,
                              ignPath: String,
                              expldPt: String) extends CommandFormattingProvider {

  def getEscapeChar: String   = config.hasProperty(escPath) match {
    case true   => config.getString(escPath)
    case false  => "\\\\"
  }

  def getIgnoreChar: String   = config.hasProperty(ignPath) match {
    case true   => config.getString(ignPath)
    case false  => "\""
  }

  def getExplodeChar: String  = config.hasProperty(expldPt) match {
    case true   => config.getString(expldPt)
    case false  => "\\s"
  }

  def getFlagChar: String     = config.hasProperty(flagPth) match {
    case true   => config.getString(flagPth)
    case false  => ""
  }

  /*
   * We are not using a flag character because we want to pass commands in from console
   */
  def getHandleChar: String = ""

}
