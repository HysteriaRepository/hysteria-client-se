package pw.bowser.hysteria.minecraft.entities

import pw.bowser.hysteria.events.Event
import net.minecraft.entity.Entity

/**
 * Date: 2/20/14
 * Time: 5:47 PM
 */
class ObserverRemoveEntityEvent(val actor: EntityDataObserver,
                                val entity: Entity) extends Event {
  def getName: String = "ObserverRemoveEntity"
}
