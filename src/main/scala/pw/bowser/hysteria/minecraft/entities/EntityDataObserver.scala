package pw.bowser.hysteria.minecraft.entities

import com.mojang.authlib.GameProfile
import pw.bowser.minecraft.entities.EntityToolkit
import pw.bowser.hysteria.events.EventHub
import scala.collection.mutable.ArrayBuffer
import net.minecraft.entity.Entity
import net.minecraft.client.Minecraft
import pw.bowser.hysteria.eventsimpl.world.{EntityInspectEvent, EntityDisappearEvent, EntityAppearEvent}
import net.minecraft.entity.player.EntityPlayer
import scala.collection.mutable

/**
 * A thread that monitors entities, sorts them, and collects data
 *
 * Date: 2/13/14
 * Time: 9:17 PM
 */
class EntityDataObserver(toolkit: EntityToolkit, mc: Minecraft, events: EventHub) extends Runnable {

  import pw.bowser.minecraft.wrapper.WrapperConversions._

  // Data
  //--------------------------------------------------------------------------------------------------------------------

  private var alive = true

  /**
   * The tracking buffer stores the last entity snapshot taking by the collector
   */
  private var trackingBuffer = new ArrayBuffer[Entity]()

  /**
   * Known players for by-name lookups
   */
  private val knownPlayers  = new mutable.HashMap[GameProfile, EntityPlayer]()

  /**
   * The number of MS to wait before beginning another sweep
   */
  private val collectionInterval = 400L

  // Collection logic
  //--------------------------------------------------------------------------------------------------------------------

  def run(): Unit = while(alive) {

    /*
     * This is where we track entities
     */
    val entitiesGrabbed = toolkit.getEntities.filter({ent =>
      var result = true
      try {
        result = !events.broadcast(new EntityInspectEvent(ent)).isCancelled
      } catch {
        case t: Throwable => // Nothing, I guess.
      }
      result
    })

    // Find the new entities by differencing the grabbed entities from the cached entities
    val newlyGrabbed    = entitiesGrabbed.diff(trackingBuffer)

    // Find the entities that disappeared by differencing the cached entities from the grabbed entities
    val entitiesMissing = trackingBuffer.diff(entitiesGrabbed)

    trackingBuffer ++= newlyGrabbed
    trackingBuffer --= entitiesMissing

    /*
     * Broadcast our findings
     */

    newlyGrabbed.foreach({ent =>
      try {
        events.broadcast(new EntityAppearEvent(ent))
      }
      catch {
        case e: Exception =>
          println("Ignoring an error created caused during an event broadcast in the entity observer thread")
      }

      ent match {
        case player: EntityPlayer => knownPlayers.put(player.gameProfile, player)
        case _ =>
      }
    })

    entitiesMissing.foreach({ent =>
      try {
        events.broadcast(new EntityDisappearEvent(ent))
      }
      catch {
        case e: Exception =>
          println("Ignoring an error created caused during an event broadcast in the entity observer thread")
      }

      ent match {
        case player: EntityPlayer => knownPlayers.remove(player.gameProfile)
        case _ =>
      }
    })

    /*
     * Cleanup far-off entities (outside of chunk range)
     */
//    toolkit.getEntities.foreach({entity =>
//      if(mc.thePlayer.getDistanceToEntity(entity) > 16 * mc.gameSettings.renderDistanceChunks){
//        try {
//          val event = events.broadcast(new ObserverRemoveEntityEvent(this, entity))
//          if(!event.isCancelled){
//            entity.setDead()
//            mc.theWorld.removeEntity(entity)
//          }
//        } catch {
//          case e: EventException =>
//        }
//      }
//    })

    Thread sleep collectionInterval
  }

  // Utility methods
  //--------------------------------------------------------------------------------------------------------------------

  /**
   * Get a player who's pointer has been cached for fast lookup
   *
   * @param name  name
   * @return      player entity
   */
  def getPlayer(name: String): Option[EntityPlayer] = knownPlayers.find(_._1.getName == name).map(_._2)

  /**
   * Tell the observer to stop
   */
  def shutdownObserver() = alive = false
}
