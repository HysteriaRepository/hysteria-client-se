package pw.bowser.hysteria.minecraft.abstractionlayer

import net.minecraft.client.Minecraft
import net.minecraft.client.entity.EntityPlayerSP
import pw.bowser.minecraft.entities.EntityToolkit
import net.minecraft.client.gui._
import pw.bowser.hysteria.chat.Formatting
import net.minecraft.client.multiplayer.GuiConnecting
import pw.bowser.minecraft.MinecraftToolkit
import com.google.inject.Inject
import pw.bowser.minecraft.wrapper.MCTypes
import scala.collection.mutable.ArrayBuffer
import scala.util.matching.Regex

/**
 * Hysteria's minecraft toolkit
 * Date: 2/8/14
 * Time: 11:44 PM
 */
@com.google.inject.Singleton
final class HysteriaMinecraftToolkit @Inject() (minecraft: Minecraft) extends MinecraftToolkit {


  import pw.bowser.minecraft.wrapper.WrapperConversions._

  private val entityToolkit = new EntityToolkit(minecraft)
  private var inventoryKit  = Option(null.asInstanceOf[PlayerInventoryToolkit])

  /**
   * Add a line to chat
   *
   * @param what text
   */
  def tellPlayer(what: String): Unit = minecraft.guiIngame.chat.addChatMessage(what)

  /**
   * Send a chat message
   *
   * @param what chat message
   */
  def sendChatMessage(what: String): Unit = {
    val messages = what.split("\n")
    messages.foreach(message =>
      thePlayer.sendChatMessage(message)
    )
  }

  /**
   * Get the player
   *
   * @return player
   */
  def thePlayer: EntityPlayerSP = minecraft.thePlayer

  /**
   * Send a packet to the server
   *
   * @param packet packet
   */
  def sendPacket(packet: MCTypes.Packet): Unit = minecraft.getNetHandler.addToSendQueue(packet)

  /**
   * Connect to a server
   *
   * @param host host
   * @param port port
   */
  def connectToServer(host: String, port: Int): Unit = {
    disconnectFromServer()
    minecraft.displayGuiScreen(new GuiConnecting(getCurrentGuiScreen, minecraft, host, port))
  }

  /**
   * Disconnect from the server currently connected to
   */
  def disconnectFromServer(): Unit = {
    minecraft.theWorld.sendQuittingDisconnectingPacket()
    minecraft.loadWorld(null)
    displayGuiScreen(new GuiMainMenu)
  }

  /**
   * Display a screen to the player
   *
   * @param screen screen instance
   */
  def displayGuiScreen(screen: GuiScreen): Unit = minecraft.displayGuiScreen(screen)

  /**
   * Get the entity toolkit
   *
   * @return entity toolkit
   */
  def getEntityToolkit: EntityToolkit = entityToolkit

  /**
   * Get the screen currently being displayed
   *
   * @return UI being displayed
   */
  def getCurrentGuiScreen: GuiScreen = minecraft.currentScreen

  /**
   * Get ingame UI and components, etc...
   *
   * @return ingame UI
   */
  def getIngameUI: GuiIngame = minecraft.ingameGUI

  /**
   * Whether or not the player is in a world
   *
   * @return is in world
   */
  def isInWorld: Boolean = minecraft.theWorld != null



  def stripColoursAccordingToChatRules(text: String): String = {
    if(minecraft.gameSettings.useControlCodesInChat) text else Formatting.stripFormatting(text)
  }


  /**
   * Split text in to lines that fit chat
   *
   * @param text  text
   * @return      split text
   */
  def makeChatLines(text: String): Array[String] = {
    val widthThreshold  = (minecraft.guiIngame.chat.width / minecraft.guiIngame.chat.scale).floor.toInt 
    var lineWorking     = text
    val lines           = ArrayBuffer[String]()

    while(minecraft.fontRenderer.getStringWidth(lineWorking) > widthThreshold) {

      var trimmedTo = minecraft.fontRenderer.trimStringToWidth(lineWorking, widthThreshold, false)
      if(trimmedTo.lastIndexOf(" ") > 0
         && minecraft.fontRenderer.getStringWidth(trimmedTo.substring(0, trimmedTo.lastIndexOf(" "))) > 0){
        trimmedTo = trimmedTo.substring(0, trimmedTo.lastIndexOf(" "))
      }

      val lastFmt   = HysteriaMinecraftToolkit.LastFormatExpr.findFirstMatchIn(trimmedTo) match {
        case matchData: Some[Regex.Match] =>
          Formatting.SIGIL + matchData.get.group("formatting_code")
        case None => Formatting.RESET // Unreachable? Maybe.
      }

      lineWorking = lastFmt + lineWorking.substring(trimmedTo.length)

      lines += trimmedTo
    }

    lines += lineWorking.trim

    lines.toArray
  }



  /**
   * Get the player inventory toolkit instance
   *
   * @return player inventory toolkit
   */
  def inventoryToolkit: PlayerInventoryToolkit = if(minecraft.thePlayer != null) {
    if(inventoryKit.isEmpty) inventoryKit = Some(new PlayerInventoryToolkit(minecraft))
    inventoryKit.get
  } else {
    throw new IllegalArgumentException("The player inventory toolkit is unavailable because minecraft is not ready ")
  }

  /**
   * Inform the game engine to re render the world
   */
  def markTerrainForUpdate(): Unit = {
    minecraft.renderGlobal.loadRenderers()
    minecraft.theWorld.markBlockRangeForRenderUpdate( thePlayer.posX.floor.toInt - 1000, 0, thePlayer.posZ.floor.toInt - 1000,
      thePlayer.posX.floor.toInt + 1000, minecraft.theWorld.getHeight, thePlayer.posZ.floor.toInt + 1000)
    minecraft.entityRenderer.updateRenderer()
  }
}
object HysteriaMinecraftToolkit {
  val LastFormatExpr = ".*§(.).*$".r("formatting_code")
}