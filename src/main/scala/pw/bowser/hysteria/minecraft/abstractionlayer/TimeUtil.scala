package pw.bowser.hysteria.minecraft.abstractionlayer

/**
 * Utility for measuring time
 * Date: 4/11/14
 * Time: 9:31 AM
 */
class TimeUtil {

  private var lastFrame = TimeUtil.systemTime

  def hasPassed(delta: Long): Boolean = if(TimeUtil.systemTime - lastFrame >= delta){
      resetFrame()
      true
    } else false

  def resetFrame(): Unit = lastFrame = TimeUtil.systemTime

}
object TimeUtil {

  def systemTime: Long = System.nanoTime() / 1000000L

}
