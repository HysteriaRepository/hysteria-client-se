package pw.bowser.hysteria.config

import pw.hysteria.input.dashfoo.command.{HelpProvider, Flag}
import pw.bowser.hysteria.engine.HysteriaServices
import pw.bowser.hysteria.commands.SimpleHelpProvider
import pw.bowser.hysteria.chat.Formatting
import pw.bowser.libhysteria.config.Configuration

/**
 * Creates a command flag that sets/prints configuration values
 * Date: 2/15/14
 * Time: 4:16 PM
 */
object ConfigurationFlag extends HysteriaServices {

  /**
   * Create a Configuration modifying flag
   *
   * @param handles         handles
   * @param config          configuration implementation
   * @param path            configuration key
   * @param actionConvert   logic for converting a series of strings to a sane value
   * @param actionOnModify  invoked when the value is modified
   * @param actionWhenEmpty invoked when no value is passed
   * @tparam VT             type parameter for string conversion
   * @return                configuration flag
   */
  def apply[VT](handles: Array[String], config: Configuration, path: String, actionConvert: Array[String] => VT,
                          actionOnModify: VT => Unit, actionWhenEmpty: => Unit): Flag = {
    new Flag {
      private val helper = new SimpleHelpProvider("[Value...]", s"Get(0) or Set(1+) the value for $path")

      def invokeFlag(args: String*): Unit = {
        try {
          if(args != null && args.length > 0){
            val result: VT = actionConvert(args.toArray)
            config.setProperty(path, result)
            actionOnModify(result)
          } else actionWhenEmpty
        } catch {
          case e: Throwable => e.printStackTrace()
        }
      }

      def getArity: Int = 1

      def getHandles: Array[String] = handles

      def getHelpProvider: HelpProvider = helper
    }
  }

  /**
   * Create a Configuration modifying flag.
   * This will create a flag with the same action for modification and nullary
   *
   * @param handles         handles
   * @param config          configuration implementation
   * @param path            configuration key
   * @param actionConvert   logic for converting a series of strings to a sane value
   * @param actionEither    invoked when edited and when nullary
   * @tparam VT             type parameter for string conversion
   * @return                configuration flag
   */
  def apply[VT](handles: Array[String], config: Configuration, path: String, actionConvert: Array[String] => VT,
                         actionEither: => Unit): Flag = {

    /*
     * Scala quirk causes shadowing due to arity-gobbling nature of closures
     */
    apply[VT](handles, config, path, actionConvert, {(arg: VT) => actionEither}, actionEither)
  }

  /**
   * Create a Configuration modifying flag.
   * This will create a flag that prints the value when it is modified and when it is nullary
   *
   * @param handles         handles
   * @param config          configuration implementation
   * @param path            configuration key
   * @param actionConvert   logic for converting a series of strings to a sane value
   * @tparam VT             type parameter for string conversion
   * @return                configuration flag
   */
  def apply[VT](handles: Array[String], config: Configuration, path: String, actionConvert: Array[String] => VT): Flag = {
    apply[VT](handles, config, path, actionConvert, InformUpdateAction(config, path))
  }

  /**
   * Create a Configuration modifying flag.
   * This will create a flag that modifies string values
   *
   * @param handles         handles
   * @param config          configuration implementation
   * @param path            configuration key
   * @param glue            string that will be used to stick the arguments together, default is ' '
   * @return                configuration flag
   */
  def apply(handles: Array[String], config: Configuration, path: String, glue: String = " "): Flag = {
    this.apply[String](handles, config, path,
                      {(args: Array[String]) => args.mkString(glue)})
  }

  object Transforms {

    import java.lang.{Long => JLong}

    val DOUBLE = {
      (args: Array[String]) => args(0).toDouble
    }

    val FLOAT = {
      (args: Array[String]) => args(0).toFloat
    }

    val LONG = {(args: Array[String]) =>
      if(args(0).startsWith("0x")){
        JLong.valueOf(args(0).substring(2), 16)
      } else {
        args(0).toLong
      }
    }

    val INT = {(args: Array[String]) =>
      if(args(0).startsWith("0x")) {
        Integer.valueOf(args(0).substring(2), 16)
      } else {
        args(0).toInt
      }
    }

    val BOOLEAN = {
      (args: Array[String]) =>
        args(0).toLowerCase == "true" || args(0).toLowerCase == "t"
    }

    def EnumerationTransform[X <: Enumeration#Value](enumeration: Enumeration, default: X): Array[String] => X = {
      args =>
        enumeration.values.foreach(v => println(v.toString))
        enumeration.withName(args(0)).asInstanceOf[X]
    }

    def IntBetween(lower: Int, upper: Int): Array[String] => Int = {
      args =>
        val intRead = args(0).toInt

        if (intRead < lower || intRead > upper) throw new IndexOutOfBoundsException(s"Specified integer `$intRead` is not between bounds `$lower` and `$upper`")
        else intRead
    }

    def StringLike(strings: String*): Array[String] => String = {
      args =>
        if(!strings.contains(args(0))) throw new IllegalArgumentException(s"Invalid string value (was not ${strings.mkString("|")})")
        args(0)
    }
  }

  def InformUpdateAction(config: Configuration, path: String) = {
    val value = config.hasProperty(path) match {
      case true   => config.getProperty(path, classOf[Object])
      case false  => "N/A"
    }
    tellPlayer(s"${Formatting.ITALIC}$path${Formatting.RESET} is set to ${Formatting.ITALIC}$value")
  }
}
