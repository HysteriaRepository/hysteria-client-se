package pw.bowser.hysteria.display

import org.lwjgl.opengl.GL11._

/**
 * Date: 1/17/14
 * Time: 11:56 AM
 */
object GLUtil {

  def beginLayerless() {
    glEnable(GL_BLEND)
    glEnable(GL_DEPTH_TEST)
    glDepthMask(false)
  }

  def endLayerless() {
    glDepthMask(false)
    glEnable(GL_DEPTH_TEST)
  }

  def beginUntextured() {
    glEnable(GL_BLEND)
    glDisable(GL_TEXTURE_2D)
    glDisable(GL_LIGHTING)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
  }

  def endUntextured() {
    glDisable(GL_BLEND)
    glEnable(GL_TEXTURE_2D)
    glEnable(GL_LIGHTING)
  }

  def stopTexturing() = glDisable(GL_TEXTURE_2D)

  def startTexturing() = glEnable(GL_TEXTURE_2D)

  def renderSmooth() = glShadeModel(GL_SMOOTH)

  def renderFlat() = glShadeModel(GL_FLAT)

  def enableFullBlending() {
    glEnable(GL_BLEND)

    glDisable(GL_LIGHTING)
    glDisable(GL_FOG)

    glDisable(GL_CULL_FACE)

    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
  }

  def componentsOf(colour: Int): (Float, Float, Float, Float) =
    ( (colour >> 16 & 255) / 255F,
      (colour >> 8  & 255) / 255F,
      (colour & 255) / 255F,
      if((colour & 0xFC000000) == 0) 1F else (colour >> 24 & 0xFF) / 255F
    )

  def setColour(colour: Int) = {
    val components = componentsOf(colour)
    setColorF(components._1, components._2, components._3, components._4)
  }

  def setColorF(channels: Float*) = channels.length match {
    case 3 => glColor3f(channels(0), channels(1), channels(2))
    case 4 => glColor4f(channels(0), channels(1), channels(2), channels(3))
    case _ => throw new IllegalArgumentException(s"Invalid list dimension ${channels.size} ($channels)")
  }

  def configureInterfaceRendering(screenWidth: Double, screenHeight: Double){
    glClear(GL_ACCUM)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()

    glOrtho(0D, screenWidth, screenHeight, 0D, 1000D, 3000D)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()
    glTranslatef(0F, 0F, -2000F)
  }

  def clearBuffers() {
    glClear(GL_COLOR_BUFFER_BIT)
    glClear(GL_DEPTH_BUFFER_BIT)
    glClear(GL_ACCUM_BUFFER_BIT)
    glClear(GL_STENCIL_BUFFER_BIT)
  }

  def start3d() {
    glPushMatrix()
    glEnable(GL_BLEND)
    glDisable(GL_TEXTURE_2D)
    glDisable(GL_LIGHTING)
    glDisable(GL_DEPTH_TEST)
    glDepthMask(false)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
    glEnable(GL_LINE_SMOOTH)
    glHint(GL_LINE_SMOOTH_HINT, GL_NICEST)
  }

  def finish3d() {
    glDisable(GL_LINE_SMOOTH)
    glDepthMask(true)
    glEnable(GL_DEPTH_TEST)
    glEnable(GL_LIGHTING)
    glEnable(GL_TEXTURE_2D)
    glDisable(GL_BLEND)
    glPopMatrix()
  }

  def with3D(action: => Unit) {
    start3d()
    action
    finish3d()
  }

}