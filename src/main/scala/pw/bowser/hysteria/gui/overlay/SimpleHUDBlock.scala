package pw.bowser.hysteria.gui.overlay

/**
 * A block of text associated with the `MCTextHUD`
 * Date: 2/12/14
 * Time: 8:54 PM
 *
 * @param lines text lines
 * @param order sort order {@see MCTextHUDBlock#sortOrder()}
 */
class SimpleHUDBlock(val lines: Array[String], val order: Int, val colourOfString: Int = 0xFFFFFF) extends TextHUDBlock {

  /**
   * Lines of text belonging to the block
   *
   * @return lines
   */
  def blockLines: Array[String] = lines

  /**
   * Position priority (upward) in which the block would like to appear
   *
   * @return sort order
   */
  def sortOrder: Int = order

  /**
   * The colour of the string
   *
   * @return colour
   */
  def colour: Int = colourOfString
}
