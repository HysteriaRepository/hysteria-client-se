package pw.bowser.hysteria.gui.overlay

import pw.bowser.minecraft.wrapper.net.minecraft.client.gui.HFontRenderer

import scala.collection.mutable.ArrayBuffer
import pw.bowser.hysteria.gui.overlay.TextHUDPosition.TextHUDPosition
import pw.bowser.hysteria.minecraft.RenderJob
import com.google.inject.{Inject, Singleton}
import net.minecraft.client.Minecraft
import pw.bowser.hysteria.display.DisplayUtil
import org.lwjgl.opengl.GL11

/**
 * Overlay that can render block of text in four corners of the screen
 *
 * Date: 2/12/14
 * Time: 8:53 PM
 */
@Singleton
class TextHUD @Inject() (mc: Minecraft) extends RenderJob {

  import pw.bowser.minecraft.wrapper.WrapperConversions._

  /**
   * Position arrays
   */
  var topLeft,
      topRight,
      bottomLeft,
      bottomRight
      = new ArrayBuffer[TextHUDBlock]()

  private var defaultOpacity: () => Int = {() => 0xFF}

  def setDefaultOpacityFunction(fn: () => Int) = defaultOpacity = fn

  def add(position: TextHUDPosition, theBlock: TextHUDBlock) = position match {
    case TextHUDPosition.TOP_LEFT =>
      if(!topLeft.contains(theBlock)) topLeft.append(theBlock)

      /*
       * Sort TOP arrays in descending order
       */
      topLeft = topLeft.sortWith((b1, b2) => b1.sortOrder > b2.sortOrder)
    case TextHUDPosition.TOP_RIGHT =>
      if(!topRight.contains(theBlock)) topRight.append(theBlock)

      /*
       * Sort TOP arrays in descending order
       */
      topRight = topRight.sortWith((b1, b2) => b1.sortOrder > b2.sortOrder)
    case TextHUDPosition.BOTTOM_LEFT =>
      if(!bottomLeft.contains(theBlock)) bottomLeft.append(theBlock)

      /*
       * Sort BOTTOM arrays in ascending order
       */
      bottomLeft = bottomLeft.sortWith((b1, b2) => b1.sortOrder < b2.sortOrder)
    case TextHUDPosition.BOTTOM_RIGHT =>
      if(!bottomRight.contains(theBlock)) bottomRight.append(theBlock)

      /*
       * Sort BOTTOM arrays in descending order
       */
      bottomRight = bottomRight.sortWith((b1, b2) => b1.sortOrder < b2.sortOrder)
  }

  def remove(position: TextHUDPosition, theBlock: TextHUDBlock) = position match {
    case TextHUDPosition.TOP_LEFT =>
      if(topLeft.contains(theBlock)) topLeft -= theBlock

      /*
       * Sort TOP arrays in descending order
       */
      topLeft = topLeft.sortWith((b1, b2) => b1.sortOrder > b2.sortOrder)
    case TextHUDPosition.TOP_RIGHT =>
      if(topRight.contains(theBlock)) topRight -= theBlock

      /*
       * Sort TOP arrays in descending order
       */
      topRight = topRight.sortWith((b1, b2) => b1.sortOrder > b2.sortOrder)
    case TextHUDPosition.BOTTOM_LEFT =>
      if(bottomLeft.contains(theBlock)) bottomLeft -= theBlock

      /*
       * Sort BOTTOM arrays in ascending order
       */
      bottomLeft = bottomLeft.sortWith((b1, b2) => b1.sortOrder < b2.sortOrder)
    case TextHUDPosition.BOTTOM_RIGHT =>
      if(bottomRight.contains(theBlock)) bottomRight -= theBlock

      /*
       * Sort BOTTOM arrays in descending order
       */
      bottomRight = bottomRight.sortWith((b1, b2) => b1.sortOrder < b2.sortOrder)
  }

  def purge(theBlock: TextHUDBlock) = Seq(topLeft, topRight, bottomLeft, bottomRight).foreach(b=> b -= theBlock)

  def render(): Unit = {

    GL11.glEnable(GL11.GL_BLEND)

    val dispWidth   = DisplayUtil.scaledDisplayWidth
    val dispHeight  = DisplayUtil.scaledDisplayHeight
    val opacityNorm = defaultOpacity()
    val drawStep    = HFontRenderer.FontHeight + 2

    var yWorking = 2

    /*
     * Render top-left
     */
    topLeft.foreach({block =>
      val colour = if((block.colour & 0xFC000000) == 0) block.colour | (opacityNorm << 8 * 3) else block.colour
      block.blockLines.foreach({
        line =>
          mc.fontRenderer.drawStringWithShadow(line, 2, yWorking, colour)
          yWorking += drawStep
      })
    })

    yWorking = 2

    /*
     * Render top-right
     */
    topRight.foreach({block =>
      val colour = if((block.colour & 0xFC000000) == 0) block.colour | (opacityNorm << 8 * 3) else block.colour
      block.blockLines.foreach({line =>
        mc.fontRenderer.drawStringWithShadow(line, dispWidth - (2 + mc.fontRenderer.stringWidth(line)), yWorking, colour)
        yWorking += drawStep
      })
    })

    yWorking = dispHeight - drawStep

    /*
     * Render bottom-left
     */
    bottomLeft.foreach({block =>
      val colour = if((block.colour & 0xFC000000) == 0) block.colour | (opacityNorm << 8 * 3) else block.colour
      block.blockLines.foreach({line =>
        mc.fontRenderer.drawStringWithShadow(line, 2, yWorking, colour)
        yWorking -= drawStep
      })
    })

    yWorking = dispHeight - drawStep

    /*
     * Render bottom-right
     */
    bottomRight.foreach({block =>
      val colour = if((block.colour & 0xFC000000) == 0) block.colour | (opacityNorm << 8 * 3) else block.colour
      block.blockLines.foreach({line =>
        mc.fontRenderer.drawStringWithShadow(line, dispWidth - (2 + mc.fontRenderer.stringWidth(line)), yWorking, colour)
        yWorking -= drawStep
      })
    })

  }
}

