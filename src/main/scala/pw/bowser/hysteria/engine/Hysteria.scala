package pw.bowser.hysteria.engine

import net.minecraft.client.Minecraft
import java.util.logging.{Level, Logger}
import pw.bowser.hysteria.minecraft.{TickGroup, RenderQueue}
import com.google.inject.{Inject, Key, Injector}
import pw.bowser.hysteria.inject.{ModLoaderBundle, Engine}
import java.io.File
import pw.bowser.hysteria.inject.minecraft.{Dimension, OutOfGame, InGame}
import pw.bowser.hysteria.events.{EventClient, EventHub}
import pw.bowser.hysteria.keyboard.BindingManager
import pw.bowser.hysteria.display.DisplayUtil
import net.minecraft.client.gui.ScaledResolution
import pw.bowser.hysteria.util.saving.GlobalStorage
import pw.bowser.hysteria.chat.Formatting
import pw.bowser.minecraft.MinecraftToolkit
import pw.bowser.hysteria.module.internal.hacks.world._
import pw.hysteria.input.dashfoo.command.CommandDispatcher
import pw.bowser.hysteria.module.internal.overlay.{ModWorldInfo, ModTextHud}
import pw.bowser.hysteria.gui.overlay.{TextHUDBlock, TextHUDPosition, TextHUD}
import pw.bowser.hysteria.module.internal.util._
import pw.bowser.hysteria.eventsimpl.world.{WorldPreExitEvent, WorldEnterEvent}
import pw.bowser.hysteria.minecraft.entities.EntityDataObserver
import pw.bowser.hysteria.module.internal.util.friends.ModFriends
import pw.bowser.hysteria.eventsimpl.minecraft.{ServerLeaveEvent, ServerJoinEvent, MinecraftShutdownEvent}
import pw.bowser.hysteria.module.internal.util.keyboard.ModKeyboard
import pw.bowser.util.VersionData
import pw.bowser.minecraft.players.PlayerDataManager
import pw.bowser.fishbans.ModFishBans
import pw.bowser.hysteria.module.internal.hacks.world.fly.ModFly
import pw.bowser.hysteria.module.internal.hacks.fun._
import pw.bowser.hysteria.module.internal.hacks.other._
import pw.bowser.minecraft.servers.{ServerData, ServerDataManager}
import pw.bowser.hysteria.module.internal.hacks.pvp.InstantUse
import pw.bowser.hysteria.module.internal.hacks.server.NoSetRotate
import pw.bowser.hysteria.module.internal.hacks.pvp.aura.Aura
import pw.bowser.libhysteria.storage.marshal.registrar.StorageService
import pw.bowser.libhysteria.config.Configuration
import pw.bowser.libhysteria.modules.ModuleLoader
import pw.bowser.hysteria.module.impl.GuiceInstantiationStrategy
import pw.bowser.libhysteria.toggles.ToggleService
import pw.bowser.spm.loaders.JarPluginLoader
import pw.bowser.hysteria.spm.GuiceInstantiator
import pw.bowser.spm.ScalaPluginManager

/**
 * Hysteria util class
 *
 * Date: 1/21/14
 * Time: 3:15 PM
 */
class Hysteria(private val hysteriaStorageDir: File, val injector: Injector) {

  // Hysteria
  //--------------------------------------------------------------------------------------------------------------------

  @Inject         val versionData: VersionData = null
  @Inject @Engine val configuration: Configuration = null
  @Inject @Engine val storageRegistrar: StorageService = null

  // Utils - Minecraft
  //--------------------------------------------------------------------------------------------------------------------

  @Inject             val tickGroup: TickGroup = null
  @Inject @InGame     val renderQueue: RenderQueue = null
  @Inject @OutOfGame  val renderQueueOutOfGame: RenderQueue = null
  @Inject @Dimension  val renderQueuePostWorld: RenderQueue = null
  @Inject             val minecraft: Minecraft = null
  @Inject             val keybinds: BindingManager = null
  @Inject             val toolkit: MinecraftToolkit = null

  // Utils - Engine
  //--------------------------------------------------------------------------------------------------------------------

  val log = Logger.getLogger(Hysteria.NAME)

  @Inject val toggleService: ToggleService = null
  @Inject val commandDispatcher: CommandDispatcher = null

  // Extensions
  //--------------------------------------------------------------------------------------------------------------------

  val extensionDataFolder = new File(hysteriaStorageDir, "extension_data")
  val modLoader           = new ModuleLoader(extensionDataFolder, new GuiceInstantiationStrategy(extensionDataFolder, injector), log)

  // Plugins
  //--------------------------------------------------------------------------------------------------------------------

  val pluginsFolder       = new File(hysteriaStorageDir, "plugins")
  pluginsFolder.mkdirs()
  val jarPluginLoader     = new JarPluginLoader(pluginsFolder, getClass.getClassLoader,
                                                new GuiceInstantiator(injector.createChildInjector(new ModLoaderBundle(modLoader))))

  ScalaPluginManager.registerLoader(jarPluginLoader)

  // Events
  //--------------------------------------------------------------------------------------------------------------------

  @Inject val events: EventHub = null

  // Overlay
  //--------------------------------------------------------------------------------------------------------------------

  @Inject val overlay: TextHUD = null

  // Player management
  //--------------------------------------------------------------------------------------------------------------------

  @Inject val playerData: PlayerDataManager = null

  // Server data
  //--------------------------------------------------------------------------------------------------------------------

  @Inject val serverData: ServerDataManager = null

  var currentServer: Option[ServerData] = None

  // Minecraft shit
  //--------------------------------------------------------------------------------------------------------------------

  var entityObserver: Option[EntityDataObserver] = None

  private var entityObserverThreadObject: Option[Thread] = None

  //--------------------------------------------------------------------------------------------------------------------

  def configureHysteria() {

    log setLevel Level.FINEST

    /* APPLICATION BOOT
     * INIT STEP 0
     * Configure plugin manager
     * Load available plugins
     */

    ScalaPluginManager.loadPlugins {(err, manifest, loader, plugin) =>
      log.log(Level.SEVERE, s"Could not load plugin ${manifest.groupId}.${manifest.name} v${manifest.version} due to an error", err)
    }

    /* APPLICATION BOOT
     * INIT STEP 1
     * Register static internal functionality
     */

    log info "Configuring static internal extensions"

    modLoader.loadModule(classOf[ChatCommandsBase])
    modLoader.loadModule(classOf[WarnInRadius])
    modLoader.loadModule(classOf[ModTextHud])
    modLoader.loadModule(classOf[ModManageToggles])
    modLoader.loadModule(classOf[ModRadar])
    modLoader.loadModule(classOf[FullBright])
    modLoader.loadModule(classOf[FastMine])
    modLoader.loadModule(classOf[ModFriends])
    modLoader.loadModule(classOf[ModKeyboard])
    modLoader.loadModule(classOf[NoInvisibilityEffect])
    modLoader.loadModule(classOf[FastWeb])
    modLoader.loadModule(classOf[ModFishBans])
    modLoader.loadModule(classOf[ModLexChat])
    modLoader.loadModule(classOf[ModFly])
    modLoader.loadModule(classOf[ModBreadCrumbs])
    modLoader.loadModule(classOf[ModWorldInfo])
    modLoader.loadModule(classOf[ArrowZones])
    modLoader.loadModule(classOf[NoFall])
    modLoader.loadModule(classOf[CancelVelocity])
    modLoader.loadModule(classOf[TeleVert])
    modLoader.loadModule(classOf[RejectItems])
    modLoader.loadModule(classOf[Sneak])
    modLoader.loadModule(classOf[Speed])
    modLoader.loadModule(classOf[Step])
    modLoader.loadModule(classOf[Wallhack])
    modLoader.loadModule(classOf[AutoMine])
    modLoader.loadModule(classOf[ReleaseMouse])
    modLoader.loadModule(classOf[Projectiles])
    modLoader.loadModule(classOf[InstantUse])
    modLoader.loadModule(classOf[Jump])
    modLoader.loadModule(classOf[NoSetRotate])
    modLoader.loadModule(classOf[ChatLogger])
    modLoader.loadModule(classOf[Aura])
    modLoader.loadModule(classOf[AntiCactus])
    modLoader.loadModule(classOf[Sprint])
    modLoader.loadModule(classOf[WaterWalk])
    modLoader.loadModule(classOf[AutoSwim])

    modLoader.enableAll()


    /* APPLICATION BOOT
     * INIT STEP 2
     * Load extension storage
     * Prepare event hooks to save and load extension storage
     */

    log info "Loading configuration post-init"

    GlobalStorage.load()

    events.subscribe(classOf[WorldPreExitEvent],
      EventClient[WorldPreExitEvent](ev => GlobalStorage.save(), 0))

    events.subscribe(classOf[MinecraftShutdownEvent],
      EventClient[MinecraftShutdownEvent](ev => GlobalStorage.save()), 0)

    /* APPLICATION BOOT
     * INIT STEP 3
     * Register game callbacks to change current server data object
     * TODO Abstract out of here
     */

    events.subscribe(classOf[ServerJoinEvent], EventClient[ServerJoinEvent]({event =>
      val serverDataForEvent = serverData.getDataFor(event.serverData)
      currentServer = Some(serverDataForEvent)
    }))

    events.subscribe(classOf[ServerLeaveEvent], EventClient[ServerLeaveEvent]({event =>
      currentServer = None
    }))


    /* APPLICATION BOOT
     * INIT STEP 4
     * Register callbacks to handle entity observer lifecycle
     * TODO Abstract out of here
     */

    events.subscribe(classOf[WorldEnterEvent], EventClient[WorldEnterEvent]({
      ev =>
        log fine "Starting entity observer thread"
        entityObserver = Some(new EntityDataObserver(toolkit.getEntityToolkit, minecraft, events))
        entityObserverThreadObject = Some(new Thread(entityObserver.get, "Hysteria Entity Observer"))
        entityObserverThreadObject.get.start()
        log fine "Started entity observer thread"
    }, Integer.MAX_VALUE))

    events.subscribe(classOf[WorldPreExitEvent], EventClient[WorldPreExitEvent]({
      ev =>
        log fine "Shutting down entity observer thread"
        if (entityObserver != None) {
          entityObserver.get.shutdownObserver()
          entityObserverThreadObject.get.join()
          entityObserverThreadObject = None
        }
        entityObserver = None
        log fine "Shut down entity observer thread"
    }, Integer.MIN_VALUE))


    /* APPLICATION BOOT
     * INIT STEP 5
     * Insert branding
     * Display scale hack
     */

    overlay.add(TextHUDPosition.TOP_LEFT, TextHUDBlock(Int.MaxValue, 0xFFFFFF, s"${Hysteria.NAME} ${Formatting.GREY + Formatting.ITALIC}${versionData.version}"))
  }

  injector.injectMembers(this)

}

object Hysteria {
  val NAME = "Hysteria"
}
