package pw.bowser.hysteria.engine

import net.minecraft.client.Minecraft

/**
 * Provides Hysteria and other such data
 *
 * Date: 2/21/14
 * Time: 6:14 PM
 */
trait HysteriaProvider {

  /**
   * Initialize hysteria and companion objects
   *
   * @param mc minecraft implementation
   */
  def initialize(mc: Minecraft)

  /**
   * Get hysteria
   *
   * @return Hysteria
   */
  @throws(classOf[UninitializedError])
  def hysteria: Hysteria

}
