package pw.bowser.hysteria.engine

import net.minecraft.client.Minecraft

/**
 * Provides globalized access to a HysteriaProvider's hysteria
 * Date: 2/21/14
 * Time: 6:19 PM
 */
object HysteriaBroker {

  private var hysteriaProvider: Option[HysteriaProvider]  = None

  def setProvider(provider: HysteriaProvider) = hysteriaProvider match {
    case None     => hysteriaProvider = Some(provider)
    case Some(_)  => throw new IllegalStateException("Hysteria Provider already Set!")
  }
  
  private def provider: HysteriaProvider = hysteriaProvider match {
    case None                       => throw UninitializedFieldError("Hysteria Provider is unset!")
    case p: Some[HysteriaProvider]  => p.get
  }

  def hysteria: Hysteria = provider.hysteria

  def configure(mc: Minecraft): Unit = provider.initialize(mc)

}
