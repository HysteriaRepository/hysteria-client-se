package pw.bowser.hysteria.module.internal.util.special.factions.models

/**
 * Represents faction ranks
 * Date: 4/8/14
 * Time: 12:06 PM
 */
sealed case class FactionRank(prefix: String)

object FactionRank {

  object RECRUIT  extends FactionRank("-")
  object MEMBER   extends FactionRank("+")
  object OFFICER  extends FactionRank("*")
  object LEADER   extends FactionRank("**")

  def withPrefix(name: String): FactionRank = name match {
    case "-"  => RECRUIT
    case "+"  => MEMBER
    case "*"  => OFFICER
    case "**" => LEADER
  }

}
