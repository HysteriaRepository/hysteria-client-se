package pw.bowser.hysteria.module.internal.util.waypoints

import pw.bowser.minecraft.servers.ServerData
import java.io.{OutputStream, InputStream}
import pw.bowser.minecraft.wrapper.net.minecraft.render.entity.HRenderManager

import scala.util.parsing.json.{JSON, JSONObject}
import scala.collection.{mutable => m}
import pw.bowser.hysteria.util.scala.json.Formatters
import scala.io.Source
import pw.bowser.hysteria.display.GLUtil
import org.lwjgl.opengl.GL11
import org.lwjgl.util.glu.{Disk, GLU, Cylinder}
import net.minecraft.client.renderer.entity.RenderManager
import pw.bowser.hysteria.commands.companion.{Flag, Command}
import pw.bowser.libhysteria.modules.ModuleManifest
import pw.bowser.hysteria.module.internal.HysteriaModule
import pw.bowser.hysteria.minecraft.RenderJob
import pw.bowser.libhysteria.storage.marshal.Marshalling

/**
 * Save locations in the world
 * Date: 3/28/14
 * Time: 9:23 AM
 */
@ModuleManifest(
  groupId     = "pw.bowser",
  name        = "WayPoints",
  version     = "0.1",
  commands    = Array("wp"),
  description = "Save locations in the world"
)
class WayPoints extends HysteriaModule with RenderJob {


  private val serverDataHook  = {(server: ServerData) =>
    server.addCompanion(classOf[WayPointStorage], new WayPointStorage)
  }

  private val command         = Command(Array("wp"),
    Flag(Array("add", "a"), {args =>
      val wpName  = if(args.size > 0) args(0) else s"#${waypointStorage.waypoints.size}"
      val wpPos   = if(args.size > 1) (args(1).toDouble, args(2).toDouble, args(3).toDouble) else (minecraft.thePlayer.posX, minecraft.thePlayer.boundingBox.minY, minecraft.thePlayer.posZ)
      val wpDim   = if(args.size > 5) args(4).toInt else minecraft.theWorld.provider.getDimensionId

      waypointStorage.addWaypoint(new WayPoint(wpPos._1, wpPos._2, wpPos._3, wpName, wpDim))

      tellPlayer(s"Added waypoint `$wpName`")
    }, "[Name: String] [x: Double] [y: Double] [z: Double] [dimension: Int]", "Add a waypoint"),
    Flag(Array("remove", "r", "d"), {args =>
      waypointStorage.waypoints.find(_.name == args(0)) match {
        case s: Some[WayPoint] =>
          waypointStorage.removeWaypoint(s.get)
          tellPlayer(s"Removed waypoint `${s.get.name}`")
        case None => tellPlayer("Waypoint not found")
      }
    }, "<Name: String>", "Remove a waypoint"),
    Flag(Array("@if_none", "list", "l"), {args =>
      tellPlayer("Waypoints: " + waypointStorage.waypoints.map(_.name).mkString(", "))
    }, null, "List waypoints (in this server)")
  )

  // Module implementation
  //--------------------------------------------------------------------------------------------------------------------

  override def initModule(): Unit = {
    super.initModule()
  }

  override def enableModule(): Unit = {
    super.enableModule()

    serverDataManager.registerInitializeHook(serverDataHook)
    worldRenderQueue.register(this)
    commandDispatcher.registerCommand(command)
  }

  override def disableModule(): Unit = {
    super.disableModule()

    serverDataManager.removeInitializeHook(serverDataHook)
    worldRenderQueue.remove(this)
    commandDispatcher.unRegisterCommand(command)
  }

  // Helpers
  //--------------------------------------------------------------------------------------------------------------------

  def waypointStorage: WayPointStorage = currentServer.get.getCompanion(classOf[WayPointStorage])

  // RenderJob implementation
  //--------------------------------------------------------------------------------------------------------------------

  private val disk    = new Disk
  disk.setDrawStyle(GLU.GLU_LINE)

  private val cone    = new Cylinder
  cone.setDrawStyle(GLU.GLU_FILL)

  def render(): Unit = GLUtil.with3D({
    GL11.glLineWidth(3F)
    GLUtil.setColour(0x880000FF)
    GL11.glEnable(GL11.GL_DEPTH_TEST)
    GL11.glDepthMask(true)
    waypointStorage.waypoints.foreach({wp =>
      GL11.glPushMatrix()
      val wpPos = wp.position
      GL11.glTranslated(
        wpPos._1 - HRenderManager.renderPosX,
        wpPos._2 - HRenderManager.renderPosY,
        wpPos._3 - HRenderManager.renderPosZ
      )

      GL11.glTranslated(0, 0.5, 0)
      GL11.glRotated(-90, 1, 0, 0)
      disk.draw(.5F, .3F, 32, 1)
      cone.draw(.5F, 0, .5F, 32, 32)
      GL11.glRotated(180, 1, 0, 0)
      cone.draw(.5F, 0, .5F, 32, 32)

      GL11.glPopMatrix()
    })
  })
}
final class WayPointStorage extends Marshalling {

  private var waypointsList = m.MutableList[WayPoint]()

  /**
   * Marshal self to output stream
   *
   * @param stream stream to write to
   */
  def writeTo(stream: OutputStream): Unit =
    stream.write(JSONObject(waypointsList.map({wp => wp.name -> wp.toJson}).toMap).toString(Formatters.prettyFormatter).getBytes)

  /**
   * Load self from input stream
   *
   * @param stream stream to read from
   */
  def readFrom(stream: InputStream): Unit = {
    val raw   = Source.createBufferedSource(stream).getLines().mkString("\n")
    val json  = JSON.parseRaw(raw).get.asInstanceOf[JSONObject]
    waypointsList.clear()
    json.obj.map({entry => WayPoint.fromJson(entry._2.asInstanceOf[JSONObject])}).foreach(waypointsList += _)
  }

  /**
   * Get the handle of the marshallable (for ref like filename etc)
   *
   * @return handle
   */
  def getHandle: String = "waypoints.json"

  /**
   * Add a waypoint
   * @param wp waypoint
   */
  def addWaypoint(wp: WayPoint): Unit = waypointsList += wp

  /**
   * Remove a waypoint
   * @param wp waypoint
   */
  def removeWaypoint(wp: WayPoint): Unit = waypointsList = waypointsList.filter(_ != wp)

  /**
   * Get an immutable list of waypoints
   * @return waypoints
   */
  def waypoints: List[WayPoint] = waypointsList.toList

}
