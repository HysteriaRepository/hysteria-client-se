package pw.bowser.hysteria.module.internal.hacks.pvp.aura

import net.minecraft.entity.Entity
import net.minecraft.client.entity.EntityOtherPlayerMP
import net.minecraft.entity.monster.IMob
import net.minecraft.entity.boss.IBossDisplayData
import net.minecraft.entity.passive.IAnimals
import net.minecraft.entity.projectile.EntityFireball

/**
 * Logic by which entity viability for attack is determined
 * @param name aura type name
 */
sealed abstract class AuraType(val name: String) {

  def shouldHit(entity: Entity): Boolean

}

/**
 * Date: 4/10/14
 * Time: 12:28 PM
 */
object AuraType {

  object Players extends AuraType("player") {
    def shouldHit(entity: Entity): Boolean = entity.isInstanceOf[EntityOtherPlayerMP]
  }

  object HostileMobiles extends AuraType("hostile") {
    def shouldHit(entity: Entity): Boolean =
      entity.getClass.isAssignableFrom(classOf[IMob]) || entity.getClass.isAssignableFrom(classOf[IBossDisplayData])
  }

  object OtherMobiles extends AuraType("mobile") {
    def shouldHit(entity: Entity): Boolean = entity.isInstanceOf[IAnimals]
  }

  object Fireballs extends AuraType("fireball") {
    def shouldHit(entity: Entity): Boolean = entity.isInstanceOf[EntityFireball]
  }

  object NullAura extends AuraType("null") {
    override def shouldHit(entity: Entity): Boolean = false
  }

  def withName(name: String): AuraType = name match {
    case "player"   => Players
    case "hostile"  => HostileMobiles
    case "mobile"   => OtherMobiles
    case "fireball" => Fireballs
  }

}