package pw.bowser.hysteria.module.internal.util.waypoints

import scala.math._
import scala.util.parsing.json.{JSONArray, JSONObject}

/**
 * Waypoint model.
 * @param posX X-position
 * @param posY Y-position
 * @param posZ Z-position
 * @param wpName Name of the waypoint
 * @param wpDimension Dimension that the waypoint resides in
 */
final class WayPoint(posX: Double, posY: Double, posZ: Double, wpName: String, wpDimension: Int){

  /**
   * Get the waypoint's position
   * @return position
   */
  def position: (Double, Double, Double) = (posX, posY, posZ)

  /**
   * Get the name of the waypoint
   * @return waypoint name
   */
  def name: String = wpName

  /**
   * Get the dimension in which the waypoint resides
   * @return waypoint dimension
   */
  def dimension: Int = wpDimension

  /**
   * Get the distance to another waypoint
   * @param wp2 waypoint
   * @return distance
   */
  def distanceTo(wp2: WayPoint): Double = distanceTo(wp2.position)

  /**
   * Get the distance to a Tuple3[Double, Double, Double]
   * @param tuple tuple
   * @return distance
   */
  def distanceTo(tuple: (Double, Double, Double)): Double = distanceTo(tuple._1, tuple._2, tuple._3)

  /**
   * Get the distance to a set of coordinates
   * @param x X-Coordinate
   * @param y Y-Coordinate
   * @param z Z-Coordinate
   * @return distance
   */
  def distanceTo(x: Double, y: Double, z: Double): Double = sqrt(pow(x - posX, 2) + pow(y - posY, 2) + pow(z - posZ, 2))

  /**
   * Convert the waypoint to a JSON format
   * @return json
   */
  def toJson: JSONObject = JSONObject(Map(
    "name"      -> name,
    "dimension" -> dimension,
    "position"  -> JSONArray(List(posX, posY, posZ))
  ))

}
object WayPoint {

  /**
   * Construct a waypoint from a JSON object
   * @param json json object
   * @return waypoint
   */
  def fromJson(json: JSONObject): WayPoint = {
    val wpName  = json.obj("name").asInstanceOf[String]
    val wpDim   = json.obj("dimension").asInstanceOf[Double].toInt
    val wpPos   = json.obj("position").asInstanceOf[JSONArray].list.map(_.asInstanceOf[Double])

    new WayPoint(wpPos(0), wpPos(1), wpPos(2), wpName, wpDim)
  }

}