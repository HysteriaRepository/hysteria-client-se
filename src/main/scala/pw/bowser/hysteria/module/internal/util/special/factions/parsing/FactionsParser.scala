package pw.bowser.hysteria.module.internal.util.special.factions.parsing

import pw.bowser.hysteria.module.internal.util.special.factions.models.FactionClaim
import scala.collection.{mutable => m}

/**
 * Date: 4/8/14
 * Time: 11:23 PM
 */
class FactionsParser {

  def createChunksFromMap(lines: List[String]): List[FactionClaim] = {

//    val (chunkX, chunkY, claimTitle)  = FactionsParser.MapExpressions.MAP_TITLE_PATTERN.findFirstMatchIn(lines(0)).get.subgroups

    null
  }

}
object FactionsParser {
  
  object MapExpressions {

    val MAP_TITLE_PATTERN = "\\[ \\((.*),(.*)\\) (.*)\\]".r("chunkX", "chunkY", "claimTitle")

    val SPECIAL_LINE_1    = "\\\\N\\/(.*)"  r "rawFactions"
    val SPECIAL_LINE_2    = "W\\+E(.*)"     r "rawFactions"
    val SPECIAL_LINE_3    = "\\/S\\\\(.*)"  r "rawFactions"
    val SPECIAL_LINE_4    = "(.*)+(.*)"     r ("rawFactions1", "rawFactions2")
    val LINE_GENERIC      = "(.*)"          r "rawFactions"

    val CLAIM_MAPPINGS    = "(.)\\:\\s(\\S*)" r("character", "name")

    val AUTO_STATUS       = "Map auto update (ENABLED|DISABLED)\\." r "status"

  }

  object PowerExpressions {

    val POWER_TITLE_PATTERN = "\\[ (\\S+) Player (\\-|\\+|\\*\\*|\\*)(?:(\\S*)\\s)?(\\S+)? \\]" r("playerType", "playerMemberLevel", "playerOrPrefix", "playerIfPrefixed")

    // Used to match the bar so we can ignore it
    val POWER_BAR           = "Power: [\\[|\\]]*".r

    val POWER_LEVEL         = "Power: ([\\-\\d\\.]*) \\/ ([\\-\\d.]*)" r("currentPower", "maxPower")
    val POWER_GAIN          = "Power per Hour: ([\\-\\d\\.]*) (?:\\((.*) left till max\\))?" r("increase", "time left")
    val POWER_LOSS          = "Power per Death:\\s([\\-\\d\\.]*)" r "decrease"

  }

  object InfoExpressions {

    val FACTION_TITLE_PATTERN = "\\[ (\\S*) Faction (\\S*) \\]" r("factionType", "factionName")

    val FACTION_AGE           = "Age: (.*)" r "factionAge"
    val FACTION_OPEN          = "Open: (Yes|No)" r "factionOpenness"
    val FACTION_STATS         = "Land \\/ Power \\/ Maxpower:  ([-\\d]*)\\/([-\\d]*)\\/([-\\d]*)" r("land", "power", "maxPower")

    val TRUCE_LIST            = "In Truce with: (.*)" r "truces"
    val ALLY_LIST             = "Allies: (.*)"        r "allies"
    val ENEMY_LIST            = "Enemies: (.*)"       r "enemies"

    val FOLLOWERS_ONLINE      = "Followers online \\((\\d*)\\): (.+)"
    val FOLLOWERS_OFFLINE     = "Followers offline \\((\\d*)\\): (.+)"

  }

  val NAME_FORMAT = "(\\-|\\+|\\*\\*|\\*)?(\\S*)(?: (\\S*))?" r("playerFactionRank", "playerTitleOrName", "playerNameIfHasTitle")
  
}