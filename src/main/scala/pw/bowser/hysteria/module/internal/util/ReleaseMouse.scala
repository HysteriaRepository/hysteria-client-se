package pw.bowser.hysteria.module.internal.util

import pw.bowser.hysteria.module.internal.HysteriaModule
import pw.bowser.libhysteria.modules.ModuleManifest
import pw.bowser.hysteria.commands.companion.{Flag, Command}

/**
 * Date: 3/23/14
 * Time: 5:40 PM
 */
@ModuleManifest(
  groupId     = "pw.hysteria",
  name        = "ReleaseMouse",
  version     = "0.1",
  commands    = Array("relm"),
  description = "Allows you to un-grab the mouse."
)
class ReleaseMouse extends HysteriaModule {



  val command = Command(Array("relm"), Flag(Array("@default"), {args => minecraft.setIngameNotInFocus()}))


  override def enableModule(): Unit = {
    super.enableModule()

    commandDispatcher.registerCommand(command)
  }

  override def disableModule(): Unit = {
    super.disableModule()

    commandDispatcher.unRegisterCommand(command)
  }
}
