package pw.bowser.hysteria.module.internal.hacks.pvp.aura

import pw.bowser.hysteria.util.scala.json.Formatters
import scala.util.parsing.json.{JSON, JSONArray}
import java.io.{OutputStream, InputStream}
import scala.io.Source
import net.minecraft.entity.Entity
import scala.collection.{mutable => m}
import pw.bowser.libhysteria.storage.marshal.Marshalling

/**
 * Logic muxer/State persistor for aura modes
 */
final class AuraProvider(handle: String = "aura_modes.json") extends Marshalling {

  private var auras = m.MutableList[AuraType]()

  /**
   * Marshal self to output stream
   *
   * @param stream stream to write to
   */
  def writeTo(stream: OutputStream): Unit = stream.write(JSONArray(auras.map(_.name).toList).toString(Formatters.prettyFormatter).getBytes)

  /**
   * Load self from input stream
   *
   * @param stream stream to read from
   */
  def readFrom(stream: InputStream): Unit = {
    val jsonSource  = Source.createBufferedSource(stream).getLines().mkString("\n")
    val jsonRaw     = JSON.parseRaw(jsonSource)
    val auraNames   = jsonRaw.get.asInstanceOf[JSONArray].list.map(_.asInstanceOf[String])
    auras.clear()
    auraNames.foreach(auras += AuraType.withName(_))
  }

  /**
   * Get the handle of the marshallable (for ref like filename etc)
   *
   * @return handle
   */
  def getHandle: String = handle

  /**
   * Determine whether or not the speicifed entity should be attacked
   * @param entity entity
   * @return should attack
   */
  def shouldAttack(entity: Entity): Boolean = if(!auras.isEmpty) auras.foldRight(false)((aura, result) => result | aura.shouldHit(entity)) else false

  /**
   * Add an aura provider
   * @param aura aura
   */
  def addAura(aura: AuraType): Unit = if(!auras.contains(aura)) auras += aura

  /**
   * Remove an aura provider
   * @param aura aura
   */
  def rmAura(aura: AuraType): Unit = auras = auras.filter(_ != aura)

  /**
   * Clear aura modes
   */
  def removeAll(): Unit = auras.clear()

  /**
   * Get all auras in use
   * @return auras
   */
  def enabledAuras: List[AuraType] = auras.toList
}
