package pw.bowser.hysteria.module.internal.hacks.world

import pw.bowser.hysteria.module.internal.HysteriaModule
import pw.bowser.hysteria.commands.companion.Command
import pw.bowser.hysteria.util.commands.ToggleFlag
import pw.bowser.libhysteria.modules.ModuleManifest
import pw.bowser.libhysteria.toggles.TogglesMixin

/**
 * NoFall
 * Date: 2/6/14
 * Time: 6:59 PM
 */
@ModuleManifest(
  groupId     = "pw.bowser",
  name        = "NoFall",
  version     = "0.1",
  commands    = Array("nf"),
  description = "Prevents the player from taking fall damage"
)
class NoFall extends HysteriaModule with TogglesMixin {

  private val command = Command(Array("nf"), ToggleFlag(Array("@if_none", "t"), this))

  override def enableModule(): Unit = {
    super.enableModule()
    toggleRegistry.enrollToggleable(this)
    commandDispatcher.registerCommand(command)


  }

  override def disableModule(): Unit = {
    super.disableModule()
    toggleRegistry.disenrollToggleable(this)
    commandDispatcher.unRegisterCommand(command)


  }

  /**
   * Get the name of the toggleable
   *
   * @return name
   */
  def displayName: String = "NoFall"

  def distinguishedName: String = "pw.hysteria.no_fall"
}
