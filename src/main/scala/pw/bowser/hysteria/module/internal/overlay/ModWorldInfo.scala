package pw.bowser.hysteria.module.internal.overlay

import pw.bowser.hysteria.gui.overlay.TextHUDBlock
import net.minecraft.util.{MathHelper}
import net.minecraft.util.MovingObjectPosition.MovingObjectType
import net.minecraft.block.{BlockSkull, Block}
import net.minecraft.entity.{Entity, EntityLiving}
import pw.bowser.hysteria.commands.HysteriaCommand
import pw.bowser.hysteria.commands.companion.Command
import pw.bowser.hysteria.util.commands.ToggleFlag
import pw.bowser.hysteria.config.ConfigurationFlag
import pw.bowser.hysteria.gui.overlay.TextHUDPosition.TextHUDPosition
import pw.bowser.hysteria.gui.overlay.TextHUDPosition
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.tileentity.TileEntitySkull
import pw.bowser.hysteria.chat.Formatting
import pw.bowser.libhysteria.modules.ModuleManifest
import pw.bowser.hysteria.module.internal.HysteriaModule
import pw.bowser.libhysteria.toggles.TogglesMixin

/**
 * Displays player coordinates and some other info
 * Date: 3/6/14
 * Time: 8:06 PM
 */
@ModuleManifest(
  groupId     = "pw.hysteria",
  name        = "WorldInfo",
  version     = "0.1",
  commands    = Array("wi"),
  description = "Displays information about player position and other things"
)
class ModWorldInfo extends HysteriaModule with TogglesMixin with TextHUDBlock {

  // Module Implementation
  //--------------------------------------------------------------------------------------------------------------------



  var command: HysteriaCommand = null

  override def initModule(): Unit = {
    super.initModule()

    configuration.setPropertyIfNew("worldinfo.sort",   0)
    configuration.setPropertyIfNew("worldinfo.colour", 0xAAAAAA)
    configuration.setPropertyIfNew("worldinfo.pos",    "top.left")

    command = Command(Array("wi"),
      ToggleFlag(Array("@if_none", "t"), this),
      ConfigurationFlag(Array("s", "sort"),   configuration, "worldinfo.sort",   ConfigurationFlag.Transforms.INT),
      ConfigurationFlag(Array("c", "colour"), configuration, "worldinfo.colour", ConfigurationFlag.Transforms.LONG),
      ConfigurationFlag(Array("p", "pos"),    configuration, "worldinfo.pos",    TextHUDPosition.TRANSFORM, {this.disable(); this.enable(); ConfigurationFlag.InformUpdateAction(configuration, "worldinfo.pos")})
    )
  }

  override def enableModule(): Unit = {
    super.enableModule()

    toggleRegistry.enrollToggleable(this)
    commandDispatcher.registerCommand(command)
  }

  override def disableModule(): Unit = {
    super.disableModule()

    toggleRegistry.disenrollToggleable(this)
    commandDispatcher.unRegisterCommand(command)
  }

  // Toggles Implementation
  //--------------------------------------------------------------------------------------------------------------------

  private var positionCurrentlyInstalled: Option[TextHUDPosition] = None

  /**
   * Get the name of the toggleable
   *
   * @return name
   */
  def displayName: String = "Coordinates"

  override def enable(): Unit = {
    positionCurrentlyInstalled = Some(positionInHud)
    headsUpDisplay.add(positionCurrentlyInstalled.get, this)
  }

  override def disable(): Unit = {
    headsUpDisplay.purge(this)
  }

  // Util
  //--------------------------------------------------------------------------------------------------------------------

  def positionInHud: TextHUDPosition =  TextHUDPosition.withName(configuration.getString("worldinfo.pos"))

  // HUD Block implementation
  //--------------------------------------------------------------------------------------------------------------------

  /**
   * Lines of text belonging to the block
   *
   * @return lines
   */
  def blockLines: Array[String] = {
      Array(
        s"Position(${minecraft.thePlayer.posX.toInt}, ${minecraft.thePlayer.posY.toInt}, ${minecraft.thePlayer.posZ.toInt})",
        s"Chunk(${minecraft.thePlayer.posX.toInt >> 4}, ${minecraft.thePlayer.posZ.toInt >> 4}) (${(minecraft.thePlayer.posX.toInt & 15) + 1}, ${(minecraft.thePlayer.posZ.toInt & 15) + 1})"
      )
  }

  /**
   * Position priority (upward) in which the block would like to appear
   *
   * @return sort order
   */
  def sortOrder: Int = configuration.getInt("worldinfo.sort")

  /**
   * The colour of the string
   *
   * @return colour
   */
  def colour: Int = configuration.getLong("worldinfo.colour").toInt

  /**
   * Should return the distinguished name of the toggles
   * The distinguished name should be unique to the toggles, ie, `pw.bowser.mods.mod.distinguished_name_here`
   * @return
   */
  override def distinguishedName: String = "pw.hysteria.world_information"
}
