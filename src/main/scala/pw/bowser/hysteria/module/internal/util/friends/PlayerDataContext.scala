package pw.bowser.hysteria.module.internal.util.friends

import pw.bowser.util.ContextBase
import pw.bowser.minecraft.players.PlayerData
import scala.collection.JavaConversions.mapAsJavaMap
import java.util.{Map => JMap}

/**
 * PlayerData context for MVEL
 * Date: 2/23/14
 * Time: 8:14 PM
 */
class PlayerDataContext(data: PlayerData) extends ContextBase {

  prop("tData", mapAsJavaMap(data.transientInfo))
  prop("sData", mapAsJavaMap(data.playerInfo))
  prop("info", data)

}
