package pw.bowser.hysteria.module.internal.util

import pw.bowser.hysteria.commands.companion.Command
import pw.bowser.hysteria.util.commands.ToggleFlag
import pw.bowser.hysteria.config.ConfigurationFlag
import pw.bowser.hysteria.module.internal.util.SneakType.SneakType
import pw.bowser.hysteria.minecraft.TickReceiver
import pw.bowser.hysteria.events.EventClient
import pw.bowser.hysteria.eventsimpl.network.PacketSendEvent
import net.minecraft.network.play.client.C0BPacketEntityAction
import net.minecraft.client.settings.KeyBinding
import org.lwjgl.input.Keyboard
import pw.bowser.libhysteria.modules.ModuleManifest
import pw.bowser.hysteria.module.internal.HysteriaModule
import pw.bowser.libhysteria.toggles.TogglesMixin
import pw.bowser.hysteria.commands.HysteriaCommand
import pw.bowser.minecraft.wrapper.net.minecraft.network.play.client.HC0BPacketEntityAction

/**
 * Date: 3/19/14
 * Time: 10:03 PM
 */
@ModuleManifest(
  groupId     = "pw.hysteria",
  name        = "Sneak",
  version     = "0.1",
  commands    = Array("sn"),
  description = "Sneak"
)
class Sneak extends HysteriaModule with TogglesMixin with TickReceiver {


  private var command: HysteriaCommand = null

  private val eventPacketRecv = EventClient[PacketSendEvent]({event =>
    if(isEnabled) event.packet match {
      case ea: C0BPacketEntityAction =>
        if(ea.action == HC0BPacketEntityAction.StopSneaking) event.cancel()
      case _ => // Ignored
    }
  })

  override def initModule(): Unit = {
    super.initModule()
    
    configuration.setPropertyIfNew("sneak.type", SneakType.REAL.toString)

    command = Command(Array("sn"),
      ToggleFlag(Array("@if_none", "t"), this),
      ConfigurationFlag(Array("mode", "m"), configuration, "sneak.type",
        {args =>
          try SneakType.withName(args(0)).toString catch {case nse: NoSuchElementException => throw new IllegalArgumentException("Invalid Sneak Mode")}
        },
        {
          sneakType match {
            case SneakType.PACKET =>
              minecraft.thePlayer.setShouldConstrainWhenSneaking(true)
              minecraft.getNetHandler.addToSendQueue(new C0BPacketEntityAction(minecraft.thePlayer, C0BPacketEntityAction.Action.START_SNEAKING))
              tickService.remove(this)
            case SneakType.REAL =>
              minecraft.thePlayer.setShouldConstrainWhenSneaking(true)
              if(isEnabled) tickService.register(this)
            case SneakType.REAL_NOCATCH =>
              if(isEnabled) tickService.register(this)
          }

          ConfigurationFlag.InformUpdateAction(configuration, "sneak.type")
        })
    )
  }

  override def enableModule(): Unit   = {
    super.enableModule()

    commandDispatcher.registerCommand(command)
    toggleRegistry.enrollToggleable(this)
    eventSystem.subscribe(classOf[PacketSendEvent], eventPacketRecv)


  }

  override def disableModule(): Unit  = {
    super.disableModule()

    commandDispatcher.unRegisterCommand(command)
    toggleRegistry.disenrollToggleable(this)
    eventSystem.unSubscribe(classOf[PacketSendEvent], eventPacketRecv)


  }

  // Helper Methods
  //--------------------------------------------------------------------------------------------------------------------

  def sneakType: SneakType = SneakType.withName(configuration.getString("sneak.type"))

  // TickReceiver Implementation
  //--------------------------------------------------------------------------------------------------------------------

  def onTick(): Unit = if(isEnabled && minecraft.thePlayer != null) {
    sneakType match {
      case SneakType.REAL =>
        KeyBinding.setKeyBindState(minecraft.gameSettings.keyBindSneak.getKeyCode, true)
      case SneakType.REAL_NOCATCH =>
        KeyBinding.setKeyBindState(minecraft.gameSettings.keyBindSneak.getKeyCode, true)
        minecraft.thePlayer.setShouldConstrainWhenSneaking(false)
    }
  }

  // Toggles Implementation
  //--------------------------------------------------------------------------------------------------------------------

  /**
   * Get the name of the toggleable
   *
   * @return name
   */
  def displayName: String = "Sneak"

  /**
   * Should return the distinguished name of the toggles
   * The distinguished name should be unique to the toggles, ie, `pw.bowser.mods.mod.distinguished_name_here`
   * @return
   */
  override def distinguishedName: String = "pw.hysteria.sneak"

  override def statusText: Option[String] = Some(sneakType.toString)

  override def enable(): Unit = {
    sneakType match {
      case SneakType.REAL | SneakType.REAL_NOCATCH => tickService.register(this)
      case SneakType.PACKET => minecraft.getNetHandler.addToSendQueue(new C0BPacketEntityAction(minecraft.thePlayer, C0BPacketEntityAction.Action.START_SNEAKING))
    }
  }

  override def disable(): Unit = {
    tickService.remove(this)
    if(!Keyboard.isKeyDown(minecraft.gameSettings.keyBindSneak.getKeyCode)) sneakType match {
      case SneakType.REAL | SneakType.REAL_NOCATCH => KeyBinding.setKeyBindState(minecraft.gameSettings.keyBindSneak.getKeyCode, false)
      case SneakType.PACKET => minecraft.getNetHandler.addToSendQueue(new C0BPacketEntityAction(minecraft.thePlayer, C0BPacketEntityAction.Action.STOP_SNEAKING))
    }
  }

}
object SneakType extends Enumeration {
  type SneakType = Value

  val REAL          = Value("real")
  val REAL_NOCATCH  = Value("nocatch")
  val PACKET        = Value("packet")
}
