package pw.bowser.hysteria.module.internal.hacks.world

import pw.bowser.hysteria.module.internal.HysteriaModule
import pw.bowser.hysteria.commands.HysteriaCommand
import pw.bowser.hysteria.commands.companion.Command
import pw.bowser.hysteria.util.commands.ToggleFlag
import pw.bowser.hysteria.config.ConfigurationFlag
import pw.bowser.hysteria.engine.HysteriaServices
import net.minecraft.world.WorldProvider
import pw.bowser.hysteria.events.EventClient
import pw.bowser.hysteria.eventsimpl.world.WorldEnterEvent
import pw.bowser.libhysteria.modules.ModuleManifest
import pw.bowser.libhysteria.toggles.{ToggleState, Toggles, TogglesMixin}
import pw.bowser.minecraft.wrapper.WrapperConversions

// TODO Cleanup

/**
 * Module FullBright
 *
 * Amplifies game light map by a specified value.
 */
@ModuleManifest(
  groupId = "pw.hysteria",
  name    = "FullBright",
  version = "0.2",
  description = "Brightness Toy",
  commands = Array("fb")
)
class FullBright extends HysteriaModule with TogglesMixin {

  var command: HysteriaCommand = null

  /*
   * ZCS: Add bright entities toggle
   */
  /*
   * Bowser
   * TODO Consider a refactor such that this resides in another module
   */
  val brightEntitiesToggle = Toggles("pw.hysteria.bright_entities", "BrightEntities")


  private val worldEnterEventListener = EventClient[WorldEnterEvent]({event =>
    val updatedTable = minecraft.theWorld.provider.lightBrightnessTable.map(level => if (configuration.getFloat("brightness.level") > level) {
      configuration.getFloat("brightness.level")
    } else {
      level
    })
    if(isEnabled) minecraft.theWorld.provider.setLightBrightnessTable(updatedTable)
  })

  override def initModule(): Unit = {
    super.initModule()

    configuration.setPropertyIfNew("brightness.level", 3F)
    configuration.setPropertyIfNew("fader.iterations", 100)

    command = Command(
      Array("fb", "fullbright"),
      ToggleFlag(Array("@if_none", "t"),  this),
      ToggleFlag(Array("be", "bents"),    brightEntitiesToggle),
      ConfigurationFlag[Float](
        Array("level", "l"), configuration, "brightness.level",
        {(args: Array[String]) => if(args(0).toFloat > -1) args(0).toFloat else 50F},
        {(nVal: Float) =>
          if(this.getState == ToggleState.Enabled) {
            val currentValue = minecraft.entityRenderer.getLightingCoefficient
            val faderThread = new GammaFader(minecraft.theWorld.provider, minecraft.theWorld.provider.lightBrightnessTable, Array.fill(minecraft.theWorld.provider.lightBrightnessTable.size)(nVal), configuration.getInt("fader.iterations"))
            new Thread(faderThread).start()
          }
          tellPlayer(s"Brightness is set to $nVal")
        },
        {tellPlayer(s"Brightness is set to ${configuration.getFloat("brightness.level")}")}
      ),
      ConfigurationFlag[Int](Array("stepping", "s"), configuration, "fader.iterations", {(args: Array[String]) => if(args(0).toInt > -1) args(0).toInt else 100})
    )

  }

  override def enableModule(): Unit = {
    super.enableModule()

    toggleRegistry.enrollToggleable(this)
    toggleRegistry.enrollToggleable(brightEntitiesToggle)

    commandDispatcher.registerCommand(command)
    eventSystem.subscribe(classOf[WorldEnterEvent], worldEnterEventListener)
  }

  override def disableModule(): Unit = {
    commandDispatcher.unRegisterCommand(command)

    toggleRegistry.disenrollToggleable(this)
    toggleRegistry.disenrollToggleable(brightEntitiesToggle)

    eventSystem.unSubscribe(classOf[WorldEnterEvent], worldEnterEventListener)
    super.disableModule()
  }

  //--------------------------------------------------------------------------------------------------------------------

  private var priorLightmap: Option[Array[Float]] = None
  private var activeFader: Option[GammaFader] = None

  override def enable(): Unit = {
    val currentValue  = minecraft.entityRenderer.getLightingCoefficient
    priorLightmap     = Some(minecraft.theWorld.provider.lightBrightnessTable)
    val fader         = new GammaFader(minecraft.theWorld.provider,
                                       minecraft.theWorld.provider.lightBrightnessTable, Array.fill(minecraft.theWorld.provider.lightBrightnessTable.size)(configuration.getFloat("brightness.level")),
                                       configuration.getInt("fader.iterations"))
    activeFader = Some(fader)
    fader.onFinish({activeFader = None})
    val faderThread   = new Thread(fader)
    faderThread.start()
  }

  override def disable(): Unit = {
    val currentValue  = minecraft.entityRenderer.getLightingCoefficient
    val fader         = new GammaFader(minecraft.theWorld.provider,
                                                 minecraft.theWorld.provider.lightBrightnessTable, priorLightmap.get,
                                                 configuration.getInt("fader.iterations"))
    activeFader     = Some(fader)
    fader.onFinish({
      minecraft.theWorld.provider.setLightBrightnessTable(priorLightmap.get)
      priorLightmap = None
      activeFader   = None
    })
    new Thread(fader).start()
  }


  override def canPersist: Boolean = false

  /**
   * Get the name of the toggleable
   *
   * @return name
   */
  def displayName: String = "Fullbright"

  /**
   * Should return the distinguished name of the toggles
   * The distinguished name should be unique to the toggles, ie, `pw.bowser.mods.mod.distinguished_name_here`
   * @return
   */
  override def distinguishedName: String = "pw.hysteria.full_bright"
}

class GammaFader(wp: WorldProvider, from: Array[Float], to: Array[Float], iterations: Int) extends Runnable with HysteriaServices with WrapperConversions {

  var whenFinished = {}

  def run(): Unit = {
    (0 to iterations).foreach({iter =>
      var arrayPos = 0
      wp.setLightBrightnessTable(wp.lightBrightnessTable.map({b =>
        val next = calculateNext(iter, from(arrayPos), to(arrayPos) - from(arrayPos), iterations)
        val _posNow = arrayPos
        arrayPos += 1
        if(to(_posNow) > from(_posNow)) {if(b < next) next else b} else next
      }))
      Thread.sleep(3L)
    })

    whenFinished
  }

  protected def calculateNext(currentIter: Float, from: Float, valueChange: Float, maxIterations: Float): Float = valueChange > 0 match {
    case false  => (valueChange * Math.pow(currentIter / maxIterations, 5) + from).toFloat
    case true   => (valueChange * (Math.pow(currentIter / maxIterations - 1, 5) + 1) + from).toFloat
  }


  final def onFinish(action: => Unit) = whenFinished = action
}