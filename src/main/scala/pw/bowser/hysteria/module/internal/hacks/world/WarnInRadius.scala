package pw.bowser.hysteria.module.internal.hacks.world

import pw.bowser.hysteria.commands.HysteriaCommand
import pw.bowser.hysteria.engine.HysteriaBroker
import net.minecraft.entity.player.EntityPlayer
import pw.bowser.hysteria.util.commands.ToggleFlag
import pw.bowser.hysteria.events.EventClient
import pw.bowser.hysteria.eventsimpl.world.{WorldPreExitEvent, EntityInspectEvent}
import pw.bowser.hysteria.chat.Formatting
import pw.bowser.hysteria.config.ConfigurationFlag
import pw.bowser.hysteria.commands.companion.Command
import scala.collection.mutable
import net.minecraft.client.entity.EntityOtherPlayerMP
import pw.bowser.libhysteria.modules.ModuleManifest
import pw.bowser.hysteria.module.internal.HysteriaModule
import pw.bowser.libhysteria.toggles.TogglesMixin

/**
 * Date: 2/9/14
 * Time: 12:31 AM
 */
@ModuleManifest(
  groupId = "pw.hysteria",
  name = "WarnInRadius",
  version = "0.2",
  description = "Warns when players come in to proximity",
  commands = Array("wr")
)
class WarnInRadius extends HysteriaModule with TogglesMixin {

  //--------------------------------------------------------------------------------------------------------------------

  private val observedPlayers = new mutable.HashSet[EntityPlayer]()

  private val eventListenerInspect = EventClient[EntityInspectEvent]({ent =>
    ent.entity match {
      case player: EntityOtherPlayerMP =>
        val distanceTest  = configuration.getDouble("players.range")
        val pData         = PlayerDataFor(player)
        if(HysteriaBroker.hysteria.minecraft.thePlayer.getDistanceToEntity(player) <= distanceTest && !observedPlayers.contains(player)){
          observedPlayers.add(player)
          tellPlayer(s"${Formatting.ITALIC}${pData.friendshipOrd.prefix + pData.nameShown}${Formatting.RESET} is within $distanceTest blocks")
        } else if(observedPlayers.contains(player) && HysteriaBroker.hysteria.minecraft.thePlayer.getDistanceToEntity(player) >= distanceTest){
          observedPlayers.remove(player)
          tellPlayer(s"${Formatting.ITALIC}${pData.friendshipOrd.prefix + pData.nameShown}${Formatting.RESET} is no longer within $distanceTest blocks")
        }
    }
  })

  private val eventListenerLeaveWorld = EventClient[WorldPreExitEvent]({worldEvent =>
    observedPlayers.clear()
  })

  private var command: HysteriaCommand = null

  //--------------------------------------------------------------------------------------------------------------------

  //--------------------------------------------------------------------------------------------------------------------

  /*
   * Module stuff
   * init
   * enable
   * disable
   */

  override def initModule(): Unit = {

    super.initModule()

    configuration.setProperty("players.range", 18D)

    command =
      Command(Array("wr"),
        ToggleFlag(Array("t", "@if_none"), this),
        ConfigurationFlag[Double](Array("dist", "d"), configuration, "players.range", {(args: Array[String]) => args(0).toDouble})
      )

  }



  //--------------------------------------------------------------------------------------------------------------------

  override def enableModule(): Unit = {
    super.enableModule()

    commandDispatcher.registerCommand(command)

    eventSystem.subscribe(classOf[WorldPreExitEvent], eventListenerLeaveWorld)

    toggleRegistry.enrollToggleable(this)
  }

  override def disableModule(): Unit = {
    commandDispatcher.unRegisterCommand(command)

    eventSystem.unSubscribe(classOf[WorldPreExitEvent], eventListenerLeaveWorld)

    toggleRegistry.disenrollToggleable(this)

    super.disableModule()
  }

  override def enable(): Unit = {
    eventSystem.subscribe(classOf[EntityInspectEvent],    eventListenerInspect)
  }

  override def disable(): Unit = {
    eventSystem.unSubscribe(classOf[EntityInspectEvent],  eventListenerInspect)
  }

  //--------------------------------------------------------------------------------------------------------------------



  /**
   * Get the name of the toggleable
   *
   * @return name
   */
  def displayName: String = "WarnRadius"

  def distinguishedName: String = "pw.hysteria.warn_in_radius"
}
