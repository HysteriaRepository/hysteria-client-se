package pw.bowser.hysteria.module.internal.hacks.pvp.aura

import pw.bowser.hysteria.commands.HysteriaCommand
import pw.bowser.hysteria.commands.companion.{Flag, Command}
import pw.bowser.hysteria.util.commands.ToggleFlag
import net.minecraft.network.play.client.{C02PacketUseEntity, C09PacketHeldItemChange, C0APacketAnimation, C03PacketPlayer}
import pw.bowser.hysteria.config.ConfigurationFlag
import pw.bowser.hysteria.events.{EventException, EventClient}
import pw.bowser.hysteria.eventsimpl.network.{PacketReceiveEvent, PacketSendEvent}
import net.minecraft.entity.{EntityLivingBase, Entity}
import java.lang.Math._
import java.lang.Math.PI
import net.minecraft.util.MathHelper
import net.minecraft.entity.projectile.EntityFireball
import net.minecraft.init.Items
import net.minecraft.item.ItemStack
import pw.bowser.hysteria.minecraft.abstractionlayer.{TimeUtil, PlayerInventoryToolkit}
import pw.bowser.hysteria.eventsimpl.world.ClientPlayerAttackEvent
import pw.bowser.hysteria.engine.HysteriaBroker
import java.util.logging.Level
import net.minecraft.network.play.server.S08PacketPlayerPosLook
import pw.bowser.libhysteria.modules.ModuleManifest
import pw.bowser.hysteria.module.internal.HysteriaModule
import pw.bowser.hysteria.minecraft.TickReceiver
import pw.bowser.libhysteria.toggles.TogglesMixin

/**
 * Date: 4/4/14
 * Time: 10:47 PM
 */
@ModuleManifest(
  groupId     = "pw.bowser",
  name        = "Aura",
  version     = "0.1",
  description = "Kill things",
  commands    = Array("aura")
)
class Aura extends HysteriaModule with TickReceiver with TogglesMixin {

  private val timer         = new TimeUtil

  private val auraProvider  = new AuraProvider()

  private var command: HysteriaCommand = null

  private var (pitchNow, yawNow)  = (0F, 0F)

  private val packetSendEvent = EventClient[PacketSendEvent]({event =>
    if(isEnabled && currentQueue.load > 0) event.packet match {
      case look: C03PacketPlayer.C05PacketPlayerLook =>
        event.packet = new C03PacketPlayer.C05PacketPlayerLook(yawNow, pitchNow, look.onGround)
      case posLook: C03PacketPlayer.C06PacketPlayerPosLook =>
        event.packet = new C03PacketPlayer.C06PacketPlayerPosLook(posLook.x, posLook.y, posLook.z, yawNow, pitchNow, posLook.onGround)
      case _ =>
    }
  })

  private val packetReceiveEvent = EventClient[PacketReceiveEvent]({event =>
    if(isEnabled) event.packet match {
        case position: S08PacketPlayerPosLook =>
          // Cancel tiny setbacks caused by jumping and hitting with ncp-shit
//          if(minecraft.thePlayer.getDistance(position.func_148932_c(), position.func_148928_d(), position.func_148933_e()) <= 4.3) event.cancel()
        case _ =>
      }
  })

  // Module implementation
  //--------------------------------------------------------------------------------------------------------------------

  override def initModule(): Unit = {
    super.initModule()

    configuration.setPropertyIfNew("aura.aps",           8)
    configuration.setPropertyIfNew("aura.qtype",         AuraQueue.RoundRobin.name)
    configuration.setPropertyIfNew("aura.sword",         true)
    configuration.setPropertyIfNew("aura.swing_silent",  false)
    configuration.setPropertyIfNew("aura.show_stats",    false)
    configuration.setPropertyIfNew("aura.reach",         3.5D)

    command = Command(Array("aura", "ka"),
      ToggleFlag(Array("@if_none", "t"), this),
      ConfigurationFlag(Array("aps"),         configuration, "aura.aps",           ConfigurationFlag.Transforms.INT),
      ConfigurationFlag(Array("sword", "as"), configuration, "aura.sword",         ConfigurationFlag.Transforms.BOOLEAN),
      ConfigurationFlag(Array("swing", "ss"), configuration, "aura.swing_silent",  ConfigurationFlag.Transforms.BOOLEAN),
      ConfigurationFlag(Array("stats"),       configuration, "aura.show_stats",    ConfigurationFlag.Transforms.BOOLEAN),
      ConfigurationFlag(Array("r", "reach"),  configuration, "aura.reach",         ConfigurationFlag.Transforms.DOUBLE, {
        currentQueue.distanceFunction = {() => configuration.getDouble("aura.reach")}
        ConfigurationFlag.InformUpdateAction(configuration, "aura.reach")
      }),
      ConfigurationFlag(Array("qt"), configuration, "aura.qtype", ConfigurationFlag.Transforms.StringLike("roundrobin"), {
        currentQueue.setAuraProvider(auraProvider)
        currentQueue.distanceFunction = {() => configuration.getDouble("aura.reach")}
        ConfigurationFlag.InformUpdateAction(configuration, "aura.qtype")
      }),
      ConfigurationFlag(Array("to"), configuration, "aura.ticks_wait", ConfigurationFlag.Transforms.INT),
      Flag(
        Array("reset"),
        {args =>
          auraProvider.removeAll()
          tellPlayer("Removed all aura modes")
        }
      ),
      Flag(
        Array("am", "addmode"),
        {args =>
          auraProvider.addAura(AuraType.withName(args(0)))
          tellPlayer(s"Added mode ${args(0)}")
        }
      ),
      Flag(
        Array("dm", "delmode"),
        {args =>
          auraProvider.rmAura(AuraType.withName(args(0)))
          tellPlayer(s"Removed mode ${args(0)}")
        }
      ),
      Flag(
        Array("lm", "listmode"),
        {args =>
          tellPlayer("Active Modes: " + auraProvider.enabledAuras.map(a => a.name).mkString(", "))
        }
      )
    )
  }

  override def enableModule(): Unit = {
    super.enableModule()

    getStorageService.enroll(auraProvider)
    commandDispatcher.registerCommand(command)
    toggleRegistry.enrollToggleable(this)
    tickService.register(this)
    eventSystem.subscribe(classOf[PacketSendEvent],     packetSendEvent)
    eventSystem.subscribe(classOf[PacketReceiveEvent],  packetReceiveEvent)

    currentQueue.setAuraProvider(auraProvider)
    currentQueue.distanceFunction = {() => configuration.getDouble("aura.reach")}


  }

  override def disableModule(): Unit = {
    super.disableModule()

    getStorageService.disenroll(auraProvider)
    commandDispatcher.unRegisterCommand(command)
    toggleRegistry.disenrollToggleable(this)
    tickService.remove(this)
    eventSystem.unSubscribe(classOf[PacketSendEvent],     packetSendEvent)
    eventSystem.unSubscribe(classOf[PacketReceiveEvent],  packetReceiveEvent)


  }

  // TickReceiver implementation
  //--------------------------------------------------------------------------------------------------------------------

  private var tickCounter         = 0

  def onTick(): Unit = if(isEnabled && minecraftToolkit.thePlayer != null && (minecraftToolkit.thePlayer.onGround || minecraft.thePlayer.isInWater)) {
    if(timer.hasPassed(1000L / configuration.getInt("aura.aps"))) {

      currentQueue.updateQueue()

      if (currentQueue.load > 0) {

        var attacking = currentQueue.nextEntity

        // Hysteria - Player attack other
        //--------------------------------------------------------------------------------------------------------------

        var attackCancelled = false

        try {
          val event: ClientPlayerAttackEvent = HysteriaBroker.hysteria.events.broadcast(new ClientPlayerAttackEvent(attacking))
          attackCancelled = event.isCancelled
          attacking = event.target
        }
        catch {
          case e: EventException =>
            HysteriaBroker.hysteria.log.log(Level.WARNING, "Unable to broadcast attack event", e)
        }

        //--------------------------------------------------------------------------------------------------------------

        if (!attackCancelled && attacking.isEntityAlive) {

          val (pitch, yaw) = calculateFacingRotation(attacking)

          if (configuration.getBoolean("aura.sword")) {
            val goodSlot = minecraftToolkit.inventoryToolkit.firstSlotWithItem(Items.diamond_sword).getOrElse(
              minecraftToolkit.inventoryToolkit.firstSlotWithItem(Items.golden_sword).getOrElse(
                minecraftToolkit.inventoryToolkit.firstSlotWithItem(Items.iron_sword).getOrElse(
                  minecraftToolkit.inventoryToolkit.firstSlotWithItem(Items.stone_sword).getOrElse(
                    minecraftToolkit.inventoryToolkit.firstSlotWithItem(Items.wooden_sword).getOrElse((null.asInstanceOf[ItemStack], null.asInstanceOf[Int]))
                  )
                )
              )
            )

            if (goodSlot._2 != null && goodSlot._2 <= PlayerInventoryToolkit.HOT_BAR_MAX_SLOT) minecraftToolkit.thePlayer.inventory.currentItem = goodSlot._2
          }

          // Sync up held item with server
          minecraftToolkit.sendPacket(new C09PacketHeldItemChange(minecraftToolkit.thePlayer.inventory.currentItem))

          // Look at the entity
          minecraftToolkit.sendPacket(new C03PacketPlayer.C05PacketPlayerLook(yaw.toFloat, pitch.toFloat, minecraftToolkit.thePlayer.onGround))

          pitchNow = pitch.toFloat
          yawNow = yaw.toFloat

          // Swing
          if (configuration.getBoolean("aura.swing_silent"))
            minecraftToolkit.sendPacket(new C0APacketAnimation)
          else
            minecraftToolkit.thePlayer.swingItem()

          // Attack
          minecraftToolkit.sendPacket(new C02PacketUseEntity(attacking, C02PacketUseEntity.Action.ATTACK))

          tickCounter = 0

        }
      }
    }
  }

  // Toggles implementation
  //--------------------------------------------------------------------------------------------------------------------

  /**
   * Get the name of the toggleable
   *
   * @return name
   */
  def displayName: String = "Aura"

  def distinguishedName: String = "pw.hysteria.kill_aura"

  override def statusText: Option[String] = if(configuration.getBoolean("aura.show_stats")) Some(s"${currentQueue.load}") else None

  override def canPersist: Boolean = false

  // Helpers
  //--------------------------------------------------------------------------------------------------------------------

  def currentQueue: AuraQueue = AuraQueue.withName(configuration.getString("aura.qtype"))

  def calculateFacingRotation(entity: Entity): (Double, Double) = {

    val xDistance = entity.posX - minecraftToolkit.thePlayer.posX
    val zDistance = entity.posZ - minecraftToolkit.thePlayer.posZ

    val yDistance = entity match {
      case el: EntityLivingBase =>
        el.posY + el.getEyeHeight - (minecraftToolkit.thePlayer.posY + minecraftToolkit.thePlayer.getEyeHeight)
      case fb: EntityFireball =>
        fb.posY - (minecraftToolkit.thePlayer.posY + minecraftToolkit.thePlayer.getEyeHeight)
      case _ =>
        (entity.boundingBox.minY + entity.boundingBox.maxY) / 2D - (minecraftToolkit.thePlayer.posY + minecraftToolkit.thePlayer.getEyeHeight)
    }

    val dist2d  = MathHelper.sqrt_double(pow(xDistance, 2) + pow(zDistance, 2))
    val yaw     =     (atan2(zDistance, xDistance)  * 180D / PI) - 90F
    val pitch   =    -(atan2(yDistance, dist2d)     * 180D / PI)

    (pitch, yaw)
  }
}