package pw.bowser.hysteria.module.internal.hacks.other

import pw.bowser.hysteria.commands.HysteriaCommand
import pw.bowser.hysteria.commands.companion.{Flag, Command}
import pw.bowser.hysteria.util.commands.ToggleFlag
import pw.bowser.hysteria.config.ConfigurationFlag
import pw.bowser.libhysteria.modules.ModuleManifest
import pw.bowser.hysteria.module.internal.HysteriaModule
import pw.bowser.libhysteria.toggles.TogglesMixin

/**
 * Date: 3/21/14
 * Time: 9:32 PM
 */
@ModuleManifest(
  groupId     = "pw.hysteria",
  name        = "Jump",
  version     = "0.1",
  commands    = Array("jump"),
  description = "Jump 1"
)
class Jump extends HysteriaModule with TogglesMixin {


  var command: HysteriaCommand = null

  override def initModule(): Unit = {
    super.initModule()
    configuration.setPropertyIfNew("jump.height",  0.437D)
    command = Command(Array("jump"), ToggleFlag(Array("@if_none", "t"), this),
      ConfigurationFlag(Array("h", "height"), configuration, "jump.height", ConfigurationFlag.Transforms.DOUBLE),
      Flag(Array("nc", "nocheat"), {args => ConfigurationFlag(Array("h", "height"), configuration, "jump.height", ConfigurationFlag.Transforms.FLOAT).invokeFlag("0.437")})
    )
  }

  override def enableModule(): Unit = {
    super.enableModule()

    toggleRegistry.enrollToggleable(this)
    commandDispatcher.registerCommand(command)
  }

  override def disableModule(): Unit = {
    super.disableModule()

    toggleRegistry.disenrollToggleable(this)
    commandDispatcher.unRegisterCommand(command)
  }

  // Helpers
  //--------------------------------------------------------------------------------------------------------------------

  def jumpFactor: Float = configuration.getFloat("jump.height")

  // Toggles Implementation
  //--------------------------------------------------------------------------------------------------------------------

  /**
   * Get the name of the toggleable
   *
   * @return name
   */
  def displayName: String = "Jump"

  /**
   * Should return the distinguished name of the toggles
   * The distinguished name should be unique to the toggles, ie, `pw.bowser.mods.mod.distinguished_name_here`
   * @return
   */
  override def distinguishedName: String = "pw.hysteria.jump"

  override def statusText: Option[String] = Some(configuration.getFloat("jump.height").toString)

}
