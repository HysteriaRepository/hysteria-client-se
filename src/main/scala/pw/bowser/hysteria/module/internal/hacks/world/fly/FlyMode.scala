package pw.bowser.hysteria.module.internal.hacks.world.fly

/**
 * Date: 2/27/14
 * Time: 10:00 PM
 */
trait FlyMode {

  /**
   * Calculate the results of the movement filter
   *
   * @param   pitch   player pitch
   * @param   yaw     player yaw
   * @param   strafe  player strafe value
   * @param   forward player forward movement value
   * @param   motionY player vertical movement value
   *
   * @return  (mvX, mvY, mvZ) modifiers
   */
  def computeModifiers(pitch: Double, yaw: Double, strafe: Float, forward: Float, motionX: Double, motionY: Double, motionZ: Double, wedge: Double): (Double, Double, Double)
}
