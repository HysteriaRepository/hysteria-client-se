package pw.bowser.hysteria.module.internal.util.keyboard

import pw.bowser.hysteria.keyboard.BindingManager
import java.io.{OutputStream, InputStream}
import scala.collection.mutable.{HashSet => MutableHashSet}
import pw.bowser.minecraft.MinecraftToolkit
import scala.util.parsing.json.{JSON, JSONObject}
import pw.bowser.hysteria.util.scala.json.Formatters
import scala.io.Source
import org.lwjgl.input.Keyboard
import pw.bowser.libhysteria.storage.marshal.Marshalling

/**
 * Manages chat bindings
 * 
 * Date: 2/19/14
 * Time: 9:28 AM
 */
final class ChatBindingsManager(service: BindingManager, toolkit: MinecraftToolkit) extends Marshalling {

  private var knownBindings = new MutableHashSet[ChatTextKeyBinding]()

  // Bindings
  //--------------------------------------------------------------------------------------------------------------------

  def lookup(sequence: Array[Int]): Option[ChatTextKeyBinding] = knownBindings.find(_.sequence.sameElements(sequence))

  def bind(sequence: Array[Int], to: String) = lookup(sequence) match {
    case None =>
      val chatBinding = new ChatTextKeyBinding(toolkit, to, sequence)
      knownBindings.add(chatBinding)
      service.registerBinding(chatBinding)
    case Some(_) =>
      throw new IllegalArgumentException(s"Sequence ${sequence.mkString("+")} was already bound!")
  }

  def remove(sequence: Array[Int]) = lookup(sequence) match {
    case binding: Some[ChatTextKeyBinding] =>
      service.deRegisterBinding(binding.get)
      knownBindings.remove(binding.get)
    case None => // Ignore
  }

  // Management
  //--------------------------------------------------------------------------------------------------------------------

  def bindings: Array[ChatTextKeyBinding]     = knownBindings.toArray

  def disenrollAllBindings()                  = knownBindings.foreach(service.deRegisterBinding(_))
  private[keyboard] def enrollAllBindings()   = knownBindings.foreach(service.registerBinding(_))

  def resetBindings() = {
    disenrollAllBindings()
    enrollAllBindings()
  }

  // Serialization
  //--------------------------------------------------------------------------------------------------------------------
  
  /**
   * Marshal self to output stream
   *
   * @param stream stream to write to
   */
  def writeTo(stream: OutputStream): Unit = {
    val bindingSequences  = knownBindings.map(binding => (binding.sequence.map(Keyboard.getKeyName).mkString("+"), binding.text)).toMap

    stream.write(Formatters.prettyFormatter(JSONObject(bindingSequences)).getBytes)
  }

  /**
   * Load self from input stream
   *
   * @param stream stream to read from
   */
  def readFrom(stream: InputStream): Unit = {
    val dataRaw = Source.fromInputStream(stream).getLines().mkString("\n")
    val jsonRaw = JSON.parseRaw(dataRaw)

    if(jsonRaw == None || !jsonRaw.get.isInstanceOf[JSONObject]) return

    val jsonObj     = jsonRaw.get.asInstanceOf[JSONObject]
    val newBindings = new MutableHashSet[ChatTextKeyBinding]()

    jsonObj.obj.foreach{
      case(seq, text: String) =>
        val sequence  = seq.split("\\+").map(Keyboard.getKeyIndex)
        newBindings.add(new ChatTextKeyBinding(toolkit, text, sequence))
      case _ =>
    }

    disenrollAllBindings()
    knownBindings = newBindings
    enrollAllBindings()
  }

  /**
   * Get the handle of the marshallable (for ref like filename etc)
   *
   * @return handle
   */
  def getHandle: String = "TextBindings"
}
