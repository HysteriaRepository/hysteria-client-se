package pw.bowser.hysteria.module.internal.hacks.pvp.aura

import pw.bowser.hysteria.engine.HysteriaServices
import net.minecraft.entity.Entity

import scala.collection.{mutable => m}
import net.minecraft.entity.player.EntityPlayer
import pw.bowser.minecraft.players.PlayerFriendship

/**
 * Logic by which entities are selected for killing
 * Date: 4/10/14
 * Time: 12:28 PM
 */
sealed abstract class AuraQueue(val name: String) {

  protected var auraProvider: Option[AuraProvider] = None

  var distanceFunction: () => Double = () => 3.4


  /**
   * Reset the entity queue
   */
  def resetQueue(): Unit

  /**
   * Get the next entity to attack
   * @return next entity
   */
  def nextEntity: Entity

  /**
   * Update the entity queue
   */
  def updateQueue(): Unit

  /**
   * Get the number of entities queued
   * @return entity count
   */
  def load: Int

  /**
   * Set the aura type
   * This is used by the selector to select entities
   * @param aura aura type
   */
  final def setAuraProvider(aura: AuraProvider): Unit = auraProvider = Some(aura)

}

object AuraQueue extends HysteriaServices {

  /**
   * Prioritizes by threat level (if living)
   */
  object RoundRobin extends AuraQueue("roundrobin") {

    private val stack     = new m.SynchronizedStack[Entity]

    /**
     * Reset the entity queue
     */
    override def resetQueue(): Unit = stack.clear()

    /**
     * Get the next entity to attack
     * @return next entity
     */
    override def nextEntity: Entity = {

      // Retrieve next acceptable entity from the stack
      //----------------------------------------------------------------------------------------------------------------

      var entityNow = stack.pop()
      while(!auraProvider.get.shouldAttack(entityNow)) entityNow = stack.pop()

      // Return target
      //----------------------------------------------------------------------------------------------------------------

      entityNow
    }

    /**
     * Update the entity queue
     */
    override def updateQueue(): Unit =
      if(stack.isEmpty) {
        minecraftToolkit.getEntityToolkit.getEntitiesWithinDistanceOfEntity(minecraftToolkit.thePlayer, distanceFunction())
          .filter(auraProvider.get.shouldAttack)
          .filter {
            case player: EntityPlayer => PlayerDataFor(player).friendshipOrd != PlayerFriendship.FRIEND
            case _ => true
          }
          .sortWith {(e1, e2) =>
            e1.getDistanceToEntity(minecraftToolkit.thePlayer) > e2.getDistanceToEntity(minecraft.thePlayer)
          }
          .foreach(stack.push)
      }

    /**
     * Get the number of entities queued
     * @return entity count
     */
    override def load: Int = stack.size

  }
//
//  object NameList extends AuraQueue("namelist") {
//
//    private val stack = new m.SynchronizedStack[Entity]
//
//    /**
//     * Get the number of entities queued
//     * @return entity count
//     */
//    override def load: Int = stack.size
//
//    /**
//     * Update the entity queue
//     */
//    override def updateQueue(): Unit =
//
//    /**
//     * Get the next entity to attack
//     * @return next entity
//     */
//    override def nextEntity: Entity =
//
//    /**
//     * Reset the entity queue
//     */
//    override def resetQueue(): Unit = ???
//  }

  def withName(name: String): AuraQueue = name match {
    case "roundrobin" | "robin" => RoundRobin
  }

}