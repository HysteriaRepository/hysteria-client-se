package pw.bowser.hysteria.module.internal.util


import java.io.File
import pw.bowser.util.mvel.LazyFileTemplateRegistry
import pw.bowser.hysteria.events.EventClient
import pw.bowser.hysteria.eventsimpl.chat.ChatMessageSendEvent
import pw.bowser.util.{ContextBase, MVFTemplate}
import pw.bowser.hysteria.commands.companion.Command
import pw.bowser.hysteria.util.commands.ToggleFlag
import pw.bowser.hysteria.chat.Formatting
import pw.bowser.libhysteria.modules.ModuleManifest
import pw.bowser.hysteria.module.internal.HysteriaModule
import pw.bowser.libhysteria.toggles.{ToggleState, TogglesMixin}

/**
 * Date: 2/25/14
 * Time: 9:47 AM
 */
@ModuleManifest(
  groupId = "pw.bowser",
  name = "LexOutChat",
  version = "0.1",
  commands = Array("lexchat"),
  description = "Run MVLex templating on outgoing chat. Supports templates in the data folder."
)
class ModLexChat extends HysteriaModule with TogglesMixin {

  // Fields
  //--------------------------------------------------------------------------------------------------------------------

  private val templateFolder = new File(dataFolder, "templates")

  private val templateRegistry = new LazyFileTemplateRegistry(templateFolder)

  private val mvflexContext = new ContextBase

  private val eventOnChatSend = EventClient[ChatMessageSendEvent]({
    event =>

      if (getState == ToggleState.Enabled && canLexChatMessage(event.message)) {
        try {
          event.message = MVFTemplate(event.message, mvflexContext, templateRegistry)
        } catch {
          case t: Throwable =>
            tellPlayer(Formatting.GOLD + "An error occurred while lexing the chat message")
            event.cancel()
        }

        if (event.message.length == 0) event.cancel()
      }

  }, Integer.MAX_VALUE)

  private val command =
    Command(Array("lexchat"), null.asInstanceOf[String], "Chat Lexing", ToggleFlag(Array("@if_none"), this))

  // Module
  //--------------------------------------------------------------------------------------------------------------------

  override def initModule(): Unit = {
    super.initModule()

    // Expressions to exclude from parsing
    configuration.setPropertyIfNew("excludes", Array("^.wr .+"))

    if (templateFolder.exists()) templateFolder.mkdirs()
  }

  override def enableModule(): Unit = {
    super.enableModule()
    commandDispatcher.registerCommand(command)
    eventSystem.subscribe(classOf[ChatMessageSendEvent], eventOnChatSend)
  }

  override def disableModule(): Unit = {
    commandDispatcher.unRegisterCommand(command)
    eventSystem.unSubscribe(classOf[ChatMessageSendEvent], eventOnChatSend)
    super.disableModule()
  }

  // Utility Stuff
  //--------------------------------------------------------------------------------------------------------------------

  def canLexChatMessage(message: String): Boolean =
    configuration.getProperty("excludes", classOf[Array[String]])
      .foldLeft(0)({
      case (count, pattern) =>
        if (message.matches(pattern)) count + 1 else 0
    }) == 0

  // Toggles
  //--------------------------------------------------------------------------------------------------------------------

  /**
   * Get the name of the toggleable
   *
   * @return name
   */
  def displayName: String = "LexOutChat"

  /**
   * Should return the distinguished name of the toggles
   * The distinguished name should be unique to the toggles, ie, `pw.bowser.mods.mod.distinguished_name_here`
   * @return
   */
  override def distinguishedName: String = "pw.hysteria.meta.lex_chat"
}
