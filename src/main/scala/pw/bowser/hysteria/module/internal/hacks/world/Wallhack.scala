package pw.bowser.hysteria.module.internal.hacks.world

import pw.bowser.hysteria.commands.HysteriaCommand
import pw.bowser.hysteria.commands.companion.{Flag, Command}
import pw.bowser.hysteria.util.commands.ToggleFlag
import pw.bowser.hysteria.config.ConfigurationFlag
import pw.bowser.minecraft.MinecraftToolkit
import net.minecraft.block.Block
import java.io.{OutputStream, InputStream}

import scala.collection.{mutable => m}
import scala.util.parsing.json.{JSON, JSONArray}
import pw.bowser.hysteria.util.scala.json.Formatters
import scala.io.Source
import net.minecraft.client.renderer.Tessellator
import pw.bowser.libhysteria.modules.ModuleManifest
import pw.bowser.hysteria.module.internal.HysteriaModule
import pw.bowser.libhysteria.toggles.TogglesMixin
import pw.bowser.libhysteria.storage.marshal.Marshalling

/**
 * TODO re-write using in-world overlay!
 * Date: 3/21/14
 * Time: 10:31 PM
 */
@ModuleManifest(
  groupId     = "pw.bowser",
  name        = "Wallhack",
  version     = "0.1",
  commands    = Array("wh"),
  description = "X-Ray"
)
class Wallhack extends HysteriaModule with TogglesMixin with Marshalling {



  private var command: HysteriaCommand = null

  private var blocks = m.MutableList[Int]()

  override def initModule(): Unit = {
    super.initModule()

    configuration.setPropertyIfNew("wallhack.opacity", 130)

    command = Command(Array("wh", "x"),
      ToggleFlag(Array("@if_none", "t"), this),
      ConfigurationFlag(Array("o", "opacity"), configuration, "wallhack.opacity",
          ConfigurationFlag.Transforms.IntBetween(0, 255),
          {
            ConfigurationFlag.InformUpdateAction(configuration, "wallhack.opacity")
            setupEngine()
          }),
      Flag(Array("a", "add"), {args =>
        var countAdded = 0
        args.foreach({arg =>
          MinecraftToolkit.StringTransforms.Block(arg) match {
            case b: Some[Block] if !blocks.contains(Block.getIdFromBlock(b.get)) =>
              blocks += Block.getIdFromBlock(b.get)
              countAdded += 1
            case _ => // Ignored
          }
        })

        if(countAdded > 1 && isEnabled) setupEngine()

        getStorageService.save()

        tellPlayer(s"Added $countAdded blocks")
      }),
      Flag(Array("r", "d", "remove", "delete"), {args =>
        var countAdded = 0
        args.foreach({arg =>
          MinecraftToolkit.StringTransforms.Block(arg) match {
            case b: Some[Block] if blocks.contains(Block.getIdFromBlock(b.get)) =>
              blocks = blocks.filter(_ != Block.getIdFromBlock(b.get))
              countAdded += 1
            case _ => // Ignored
          }
        })

        if(countAdded > 1 && isEnabled) setupEngine()

        getStorageService.save()

        tellPlayer(s"Removed $countAdded blocks")
      }),
      Flag(Array("cl", "clear"), {args =>
        blocks.clear()
        getStorageService.save()
        tellPlayer("Cleared block list")
      }),
      Flag(Array("l", "list"), {args =>
        tellPlayer("Blocks: " + blocks.map({id => Block.getBlockById(id).getLocalizedName}).mkString(", "))
      })
    )
  }

  override def enableModule(): Unit = {
    super.enableModule()

    getStorageService.enroll(this)
    toggleRegistry.enrollToggleable(this)
    commandDispatcher.registerCommand(command)
  }

  override def disableModule(): Unit = {
    super.disableModule()

    getStorageService.disenroll(this)
    toggleRegistry.disenrollToggleable(this)
    commandDispatcher.unRegisterCommand(command)
  }

  // Helpers
  //--------------------------------------------------------------------------------------------------------------------

  def shouldShowBlockFaces(id: Int): Boolean = blocks.contains(id)

  def worldOpacity: Int = configuration.getInt("wallhack.opacity")

  def setupEngine() = {
    minecraftToolkit.markTerrainForUpdate()
  }

  // Toggles implementation
  //--------------------------------------------------------------------------------------------------------------------

  override def enable(): Unit = {
    setupEngine()
  }

  override def disable(): Unit = {
    setupEngine()
  }


  override def canPersist: Boolean = false

  override def statusText: Option[String] = Some(worldOpacity.toString)

  /**
   * Get the name of the toggleable
   *
   * @return name
   */
  def displayName: String = "Wallhack"

  /**
   * Should return the distinguished name of the toggles
   * The distinguished name should be unique to the toggles, ie, `pw.bowser.mods.mod.distinguished_name_here`
   * @return
   */
  override def distinguishedName: String = "pw.hysteria.wallhack"

  override def requiredToggles: List[String] = List("pw.hysteria.full_bright")

  // Marshalling implementation
  //--------------------------------------------------------------------------------------------------------------------

  /**
   * Marshal self to output stream
   *
   * @param stream stream to write to
   */
  def writeTo(stream: OutputStream): Unit = stream.write(JSONArray(blocks.toList).toString(Formatters.prettyFormatter).getBytes)

  /**
   * Load self from input stream
   *
   * @param stream stream to read from
   */
  def readFrom(stream: InputStream): Unit = {
    val sourceStr = Source.createBufferedSource(stream).getLines().mkString("\n")
    val jsonRaw   = JSON.parseRaw(sourceStr)

    jsonRaw match {
      case ja: Some[JSONArray] =>
        blocks.clear()
        ja.get.list.foreach({
          case i: Int => blocks += i
          case _ => // Ignored
        })
      case None => // Do nothing
    }
  }

  /**
   * Get the handle of the marshallable (for ref like filename etc)
   *
   * @return handle
   */
  def getHandle: String = "blocks.list"
}
