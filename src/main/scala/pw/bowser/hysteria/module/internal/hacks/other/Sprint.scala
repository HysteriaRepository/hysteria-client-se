package pw.bowser.hysteria.module.internal.hacks.other

import pw.bowser.hysteria.commands.HysteriaCommand
import pw.bowser.hysteria.commands.companion.Command
import pw.bowser.hysteria.util.commands.ToggleFlag
import net.minecraft.client.settings.KeyBinding
import pw.bowser.hysteria.minecraft.TickReceiver
import pw.bowser.libhysteria.modules.ModuleManifest
import pw.bowser.hysteria.module.internal.HysteriaModule
import pw.bowser.libhysteria.toggles.TogglesMixin


/**
  * Sprint
  * @author zencantsnipe
  * Date: 5/04/14
  * Time: 4:20 PM
  */

@ModuleManifest(
  groupId     = "pw.hysteria.zencantsnipe",
  name        = "Sprint",
  version     = "0.1",
  commands    = Array("sprint", "spr"),
  description = "Enables Minecraft Sprint functionality automatically when moving."
)
class Sprint extends HysteriaModule with TogglesMixin with TickReceiver {
  var command: HysteriaCommand = Command(Array("sprint", "spr"), ToggleFlag(Array("@if_none", "t"), this))

  def onTick(): Unit = if(this.isEnabled && minecraft.thePlayer != null
                          && minecraft.thePlayer.moveForward > 0.0 && !minecraft.thePlayer.isSneaking
                          && !minecraft.thePlayer.isCollidedHorizontally
                          && minecraft.thePlayer.getFoodStats.getFoodLevel > 6) {

    KeyBinding.setKeyBindState(minecraft.gameSettings.keyBindSprint.getKeyCode, true)
  }

  override def enableModule(): Unit = {
    super.enableModule()
    toggleRegistry.enrollToggleable(this)
    commandDispatcher.registerCommand(command)
  }

  override def disableModule(): Unit = {
    super.disableModule()
    toggleRegistry.disenrollToggleable(this)
    commandDispatcher.unRegisterCommand(command)
  }

  // Toggles Implementation
  //--------------------------------------------------------------------------------------------------------------------
  /**
   * Get the name of the toggleable
   *
   * @return name
   */
  def displayName: String = "Sprint"

  /**
   * Should return the distinguished name of the toggles
   * The distinguished name should be unique to the toggles, ie, `pw.bowser.mods.mod.distinguished_name_here`
   * @return
   */
  override def distinguishedName: String = "pw.hysteria.sprint"

  override def conflictingToggles: List[String] = List("pw.hysteria.speed", "pw.hysteria.sneak", "pw.hysteria.fly")

  override def enable(): Unit = {
    tickService.register(this)
  }

  override def disable(): Unit = {
    KeyBinding.setKeyBindState(minecraft.gameSettings.keyBindSprint.getKeyCode, false)
    tickService.remove(this)
  }

}
