package pw.bowser.hysteria.module.internal.util

import pw.bowser.hysteria.module.internal.HysteriaModule
import pw.bowser.hysteria.chat.Formatting
import pw.bowser.hysteria.gui.overlay.{TextHUDBlock, TextHUDPosition}
import pw.bowser.hysteria.gui.overlay.TextHUDPosition.TextHUDPosition
import pw.bowser.hysteria.util.commands.ToggleFlag
import pw.bowser.hysteria.commands.companion.{Flag, Command}
import pw.bowser.hysteria.engine.HysteriaServices
import pw.bowser.hysteria.config.ConfigurationFlag
import java.io.{OutputStream, InputStream}
import pw.bowser.minecraft.wrapper.WrapperConversions

import scala.collection.{mutable => m}
import scala.util.parsing.json.{JSON, JSONArray}
import pw.bowser.hysteria.util.scala.json.Formatters
import scala.io.Source
import pw.bowser.libhysteria.modules.ModuleManifest
import pw.bowser.libhysteria.toggles.{ToggleState, ToggleService, Toggles}
import pw.bowser.libhysteria.storage.marshal.Marshalling

/**
 * Allows for management of registered toggles
 * Date: 2/13/14
 * Time: 11:27 AM
 */
@ModuleManifest(
  groupId = "pw.hysteria",
  name = "TogglesUtil",
  description = "Manages toggles",
  version = "0.1",
  commands = Array("togs")
)
class ModManageToggles extends HysteriaModule {

  // Fields
  //--------------------------------------------------------------------------------------------------------------------

  var theBlock: TogglesOverlayBlock = null

  var lastPosition = TextHUDPosition.TOP_LEFT

  val inOverlay =
    Toggles(
      "pw.hysteria.meta.toggles_in_overlay",
      "TogglesInHud",
      {
        lastPosition = enumPositionForOverlay
        headsUpDisplay.add(enumPositionForOverlay, theBlock)
      },
      {
        headsUpDisplay.remove(lastPosition, theBlock)
      }
    )

  // Module Stuff
  //--------------------------------------------------------------------------------------------------------------------

  override def initModule() {

    super.initModule()

    getStorageService.enroll(IgnoredEntries)

    configuration.setPropertyIfNew("toggles.overlay.corner", "top.left")
    configuration.setPropertyIfNew("toggles.overlay.sort",   1)
    configuration.setPropertyIfNew("toggles.overlay.colour", 0x555555.toLong)
    configuration.setPropertyIfNew("toggles.notify",         true)
    configuration.setPropertyIfNew("toggles.status",         true)

    ToggleFlag.setNotifyClause(() => configuration.getBoolean("toggles.notify"))

    toggleRegistry.enrollToggleable(inOverlay)

    theBlock = new TogglesOverlayBlock(toggleRegistry, blockPriority, colourForText, {t => IgnoredEntries.shouldDisplay(t.distinguishedName)}, {t => configuration.getBoolean("toggles.status")})

    // TODO Code clean up
    // TODO DRY up code

    commandDispatcher.registerCommand(
      Command(
        Array("togs"),
        "List toggles",
        Flag(Array("@if_none", "l"), {args =>
          var theList = "Registered Toggles: "
          toggleRegistry.enrolledToggles.foreach({t=>
            theList +=
              (if(t.getState == ToggleState.Enabled) Formatting.BRIGHT_GREEN else Formatting.RED) +
              (t.getState match {
                case ToggleState.Required   => Formatting.ITALIC
                case ToggleState.Suppressed => Formatting.STRIKEOUT
                case _ => ""
              }) + t.displayName + s"${Formatting.RESET}, "
          })

          tellPlayer(theList)
        }, null, "List toggles"),
        Flag(Array("t", "toggle"), {args =>
          toggleRegistry.toggle(args(0)) match {
            case ToggleState.Enabled =>
              tellPlayer(s"Toggle '${args(0)} enabled'")
            case ToggleState.Disabled =>
              tellPlayer(s"Toggle '${args(0)} disabled'")
          }
        }, "<toggle: String>", "Toggle a Toggle"),
        ConfigurationFlag(Array("oc", "colour"), configuration, "toggles.overlay.colour", ConfigurationFlag.Transforms.LONG),
        ConfigurationFlag(Array("s", "sort"), configuration, "toggles.overlay.sort", ConfigurationFlag.Transforms.INT),
        ConfigurationFlag(Array("p", "position"), configuration, "toggles.overlay.corner", TextHUDPosition.TRANSFORM, {toggleRegistry.toggle(inOverlay); toggleRegistry.toggle(inOverlay); ConfigurationFlag.InformUpdateAction(configuration, "toggles.overlay.corner")}),
        ConfigurationFlag(Array("n", "notify"), configuration, "toggles.notify", ConfigurationFlag.Transforms.BOOLEAN),
        ConfigurationFlag(Array("st", "status"), configuration, "toggles.status", ConfigurationFlag.Transforms.BOOLEAN),
        ToggleFlag(Array("ot", "overlay"), inOverlay),
        Flag(Array("lookup", "search"), {args =>
          val toggles = toggleRegistry.enrolledToggles.filter(_.displayName.startsWith(args(0))).map(_.distinguishedName)
          if(toggles.size > 0){
            tellPlayer(s"Found toggles: ${toggles.mkString(", ")}")
          } else {
            tellPlayer(s"No toggles found")
          }
        }, "Look up all distinguished names with display names that start with $0"),
        Flag(Array("about", "info", "a"), {args =>
          val toggle =
            toggleRegistry.enrolledToggles.find(_.distinguishedName == args(0))
          toggle match {
            case None =>
              tellPlayer(s"No such Toggles as ${args(0)} found")
            case s: Some[Toggles] =>
              val toggles = s.get
              tellPlayer(
               s"""Toggles `${toggles.distinguishedName}`
                  |${toggles.requiredToggles.size} dependencies: ${toggles.requiredToggles.mkString(", ")}
                  |${toggles.conflictingToggles.size} conflicts: ${toggles.conflictingToggles.mkString(", ")}
                """.stripMargin)
          }
        }, "Display information about toggles $0"),
        Flag(Array("ignore", "i"), {args =>
          var addedCount = 0
          args.foreach({arg => if(IgnoredEntries.addIgnore(arg)) addedCount += 1})
          tellPlayer(s"Added $addedCount ignores")
        }),
        Flag(Array("unignore", "ui"), {args =>
          var removedCount = 0
          args.foreach({arg => if(IgnoredEntries.removeIgnore(arg)) removedCount += 1})
          tellPlayer(s"Removed $removedCount ignores")
        }),
        Flag(Array("ignores", "li"), {args =>
          tellPlayer("Ignores: " + IgnoredEntries.ignores.mkString(", "))
        })
      )
    )
  }

  // Utilities
  //--------------------------------------------------------------------------------------------------------------------

  final def enumPositionForOverlay: TextHUDPosition = TextHUDPosition.withName(configuration.getString("toggles.overlay.corner"))

  final def colourForText: Int = configuration.getLong("toggles.overlay.colour").toInt

  final def blockPriority: Int = configuration.getInt("toggles.overlay.sort")

  // Storage for chat ignores
  //--------------------------------------------------------------------------------------------------------------------

  object IgnoredEntries extends Marshalling {

    private var ignoredEntries = m.MutableList[String]()

    /**
     * Marshal self to output stream
     *
     * @param stream stream to write to
     */
    def writeTo(stream: OutputStream): Unit = stream.write(JSONArray(ignoredEntries.toList).toString(Formatters.prettyFormatter).getBytes)

    /**
     * Load self from input stream
     *
     * @param stream stream to read from
     */
    def readFrom(stream: InputStream): Unit = {
      val raw   = Source.createBufferedSource(stream).getLines().mkString("\n")
      ignoredEntries.clear()
      JSON.parseRaw(raw).get.asInstanceOf[JSONArray].list.map(_.asInstanceOf[String]).foreach(ignoredEntries += _)
    }

    /**
     * Get the handle of the marshallable (for ref like filename etc)
     *
     * @return handle
     */
    def getHandle: String = "ignored.json"

    /**
     * Returns true if the specified text should be displayed
     * @param s string
     * @return should display
     */
    def shouldDisplay(s: String): Boolean = !ignoredEntries.contains(s.toLowerCase)

    /**
     * Add a string to the ignore list
     * @param s string to ignore
     */
    def addIgnore(s: String): Boolean = if(ignoredEntries.contains(s.toLowerCase)) false else { ignoredEntries += s.toLowerCase; true }

    /**
     * Remove a string from the ignores list
     * @param s string
     */
    def removeIgnore(s: String): Boolean = if(ignoredEntries.contains(s.toLowerCase)){ ignoredEntries = ignoredEntries.filter(_ != s.toLowerCase); true } else false

    /**
     * Get an immutable list of the ignores
     * @return ignores
     */
    def ignores: List[String] = ignoredEntries.toList
  }

}
final class TogglesOverlayBlock(svc: ToggleService,
                                prioFunc: => Int,
                                colourFunc: => Int,
                                filterFunc: Toggles => Boolean,
                                statusFilter: Toggles => Boolean) extends TextHUDBlock with HysteriaServices with WrapperConversions {
  /**
   * Lines of text belonging to the block
   *
   * @return lines
   */
  def blockLines: Array[String] = {
    svc.enrolledToggles
      .filter(_.getState != ToggleState.Disabled)
      .filter(filterFunc)
      .map({
        t =>
          val text = t.statusText.getOrElse(null) match {
            case v: Any if statusFilter(t) => s"(${v.toString})"
            case _ => s""
          }

          val prefix = t.getState match {
            case ToggleState.Required   => Formatting.ITALIC
            case ToggleState.Suppressed => Formatting.STRIKEOUT
            case _ => ""
          }

          prefix + t.displayName + Formatting.RESET + text
      })
      .toArray
      .sortWith((str1, str2) => minecraft.fontRenderer.getStringWidth(str1) > minecraft.fontRenderer.getStringWidth(str2))
  }

  /**
   * Position priority (upward) in which the block would like to appear
   *
   * @return sort order
   */
  def sortOrder: Int = prioFunc

  /**
   * The colour of the string
   *
   * @return colour
   */
  def colour: Int = colourFunc
}