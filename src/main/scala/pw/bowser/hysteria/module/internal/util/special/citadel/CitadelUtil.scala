package pw.bowser.hysteria.module.internal.util.special.citadel

import pw.bowser.hysteria.gui.overlay.{TextHUDPosition, TextHUDBlock}
import pw.bowser.hysteria.events.EventClient
import pw.bowser.hysteria.eventsimpl.chat.ChatMessageReceiveEvent
import pw.bowser.minecraft.servers.ServerData
import pw.bowser.hysteria.commands.HysteriaCommand
import pw.bowser.hysteria.commands.companion.{Flag, Command}
import pw.bowser.hysteria.util.commands.ToggleFlag
import pw.bowser.hysteria.config.ConfigurationFlag
import net.minecraft.util.MovingObjectPosition
import pw.bowser.libhysteria.modules.ModuleManifest
import pw.bowser.hysteria.module.internal.HysteriaModule
import pw.bowser.libhysteria.toggles.TogglesMixin

/**
 * Date: 3/24/14
 * Time: 1:34 PM
 */
@ModuleManifest(
  groupId     = "pw.bowser",
  name        = "CitadelUtil",
  version     = "0.1",
  commands    = Array("cit"),
  description = "Displays status for the current citadel block"
)
class CitadelUtil extends HysteriaModule with TogglesMixin {



  // Events
  //--------------------------------------------------------------------------------------------------------------------

  private val eventMessageReceive = EventClient[ChatMessageReceiveEvent]({message =>
    if(isEnabled) {

    }
  })

  // Server data initialization hook
  //--------------------------------------------------------------------------------------------------------------------
  
  private val serverDataInitHook = {(data: ServerData) => data.addCompanion(classOf[CitadelProtectionCache], new CitadelProtectionCache)}

  // Command
  //--------------------------------------------------------------------------------------------------------------------

  private var command: HysteriaCommand = null

  // Others
  //--------------------------------------------------------------------------------------------------------------------

  private val overlayItem = new CitadelHUDBlock(this)

  // Module Implementation
  //--------------------------------------------------------------------------------------------------------------------

  override def initModule(): Unit = {
    super.initModule()

    configuration.setPropertyIfNew("citadel.highlight",  false)
    configuration.setPropertyIfNew("citadel.show_chat",  false)
    configuration.setPropertyIfNew("citadel.overlaypos", TextHUDPosition.TOP_RIGHT.toString)
    configuration.setPropertyIfNew("citadel.sortorder",  1)
    configuration.setPropertyIfNew("citadel.colour",     0xFFFFFF.toLong)

    command = Command(Array("cit"),
      ToggleFlag(Array("@if_none", "t"), this),
      ConfigurationFlag(Array("h", "highlight"),  configuration, "citadel.highlight",  ConfigurationFlag.Transforms.BOOLEAN),
      ConfigurationFlag(Array("sc", "showchat"),  configuration, "citadel.show_chat",  ConfigurationFlag.Transforms.BOOLEAN),
      ConfigurationFlag(Array("so", "sortorder"), configuration, "citadel.sortorder",  ConfigurationFlag.Transforms.INT),
      ConfigurationFlag(Array("oc", "colour"),    configuration, "citadel.color",      ConfigurationFlag.Transforms.LONG),
      ConfigurationFlag(Array("p", "hudcorner"),  configuration, "citadel.overlaypos", TextHUDPosition.TRANSFORM,  {
        headsUpDisplay.purge(overlayItem)
        if(isEnabled) headsUpDisplay.add(TextHUDPosition.withName(configuration.getString("citadel.overlaypos")), overlayItem)
        ConfigurationFlag.InformUpdateAction(configuration, "citadel.overlaypos")
      }),
      Flag(
        Array("cc"),
        {args =>
          currentServer.get.getCompanion(classOf[CitadelProtectionCache]).clearCache()
          tellPlayer("Cache Cleared")
        }, null, "Clear the cache for this server")
    )
  }

  override def enableModule(): Unit = {
    super.enableModule()

    serverDataManager.registerInitializeHook(serverDataInitHook)
    eventSystem.subscribe(classOf[ChatMessageReceiveEvent], eventMessageReceive)
    commandDispatcher.registerCommand(command)
  }

  override def disableModule(): Unit = {
    super.disableModule()

    serverDataManager.removeInitializeHook(serverDataInitHook)
    eventSystem.unSubscribe(classOf[ChatMessageReceiveEvent], eventMessageReceive)
    commandDispatcher.unRegisterCommand(command)
  }

  // Helpers
  //--------------------------------------------------------------------------------------------------------------------

  def getCurrentBlock: Option[CitadelBlock] = minecraft.objectMouseOver match {
    case null => None
    case mo: MovingObjectPosition => mo.typeOfHit match {
      case MovingObjectPosition.MovingObjectType.BLOCK =>

        currentCache.get.getBlockAt(mo.hitVec.xCoord.round.toInt, mo.hitVec.yCoord.round.toInt, mo.hitVec.zCoord.round.toInt)
      case _ => None
    }
  }

  def currentCache: Option[CitadelProtectionCache] = {
    currentServer match {
      case s: Some[ServerData] => Some(s.get.getCompanion(classOf[CitadelProtectionCache]))
      case None => None
    }
  }

  def sortOrder: Int = configuration.getInt("citadel.sortorder")

  def colour: Int = configuration.getLong("citadel.colour").toInt

  // Toggles Implementation
  //--------------------------------------------------------------------------------------------------------------------

  /**
   * Get the name of the toggleable
   *
   * @return name
   */
  def displayName: String = "CitadelUtil"

  /**
   * Should return the distinguished name of the toggles
   * The distinguished name should be unique to the toggles, ie, `pw.bowser.mods.mod.distinguished_name_here`
   * @return
   */
  override def distinguishedName: String = "pw.hysteria.integration.citadel"

  override def enable(): Unit = {
    headsUpDisplay.add(TextHUDPosition.withName(configuration.getString("citadel.overlaypos")), overlayItem)
  }

  override def disable(): Unit = {
    headsUpDisplay.purge(overlayItem)
  }
}

object CitadelUtil {

  val INFO_OFF  = "INFO mode off".r
  val INFO_ON   = "INFO mode on".r

//  val PROT_MODE

}

class CitadelHUDBlock(module: CitadelUtil) extends TextHUDBlock{
  /**
   * Lines of text belonging to the block
   *
   * @return lines
   */
  def blockLines: Array[String] = Array(
    module.getCurrentBlock match {
      case None => "No Protection"
      case s: Some[CitadelBlock] => s"${s.get.protectionType.name}/${s.get.protectionLevel.levelName} protection ${s.get.currentResilience}/${s.get.protectionType.resilience}"
    }
  )

  /**
   * Position priority (upward) in which the block would like to appear
   *
   * @return sort order
   */
  def sortOrder: Int = module.sortOrder

  /**
   * The colour of the string
   *
   * @return colour
   */
  def colour: Int = module.colour
}