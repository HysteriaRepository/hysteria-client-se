package pw.bowser.hysteria.module.internal.util

import pw.bowser.hysteria.events.EventClient
import com.google.inject.Inject
import pw.bowser.hysteria.inject.Engine
import pw.bowser.hysteria.chat.Formatting
import pw.bowser.hysteria.module.internal.HysteriaModule
import pw.bowser.hysteria.eventsimpl.chat.ChatMessageSendEvent
import pw.bowser.hysteria.commands.companion.{Flag, Command}
import java.util.logging.Level
import pw.bowser.libhysteria.modules.ModuleManifest
import pw.bowser.libhysteria.config.Configuration

/**
 * Date: 2/3/14
 * Time: 10:48 PM
 */
@ModuleManifest(
  groupId = "pw.hysteria",
  name = "CommandsBase",
  version = "0.1",
  description = "Chat command functionality",
  commands = Array("h", "ch")
)
class ChatCommandsBase extends HysteriaModule {

  @Inject
  @Engine
  var hConfig: Configuration    = null

  override def initModule(): Unit = {

    super.initModule()

    getLogger fine "Setting up configuration"

    hConfig.setPropertyIfNew("commands.form.handle",  "-")
    hConfig.setPropertyIfNew("commands.form.escape",  "\\\\")
    hConfig.setPropertyIfNew("commands.form.ignore",  "\"")
    hConfig.setPropertyIfNew("commands.form.flag",    "-")
    hConfig.setPropertyIfNew("commands.form.explode", " ")

    eventSystem.subscribe(classOf[ChatMessageSendEvent], EventClient[ChatMessageSendEvent]({cms: ChatMessageSendEvent =>
      val message = cms.message
      val handle  = hConfig.getString("commands.form.handle")

      if(message.startsWith(handle)){
        val commandText = message.substring(handle.length)
        getLogger fine s"Executing command: $commandText"

        try{
          commandDispatcher.dispatchCommand(commandText)
        } catch {
          case ia: IllegalArgumentException =>
            tellPlayer(s"Illegal Argument: ${ia.getMessage}")
            getLogger log(Level.SEVERE, "Command threw IllegalArgumentException!", ia)
          case oob: ArrayIndexOutOfBoundsException =>
            tellPlayer(s"OutOfBoundsException - Perhaps too few arguments were passed to the command or flag?")
            getLogger log(Level.SEVERE, "Caught a potential argument ", oob)
          case e: Throwable =>
            tellPlayer(Formatting.RED + s"Error: ${e.getClass.getSimpleName}: ${e.getMessage}")
            getLogger log(Level.SEVERE, "Caught an error while running command", e)
        }

        cms.cancel()
      }
    }, 999))

    commandDispatcher.registerCommand(injector.getInstance(classOf[CommandHelp]))
    commandDispatcher.registerCommand(injector.getInstance(classOf[CommandChat]))

    commandDispatcher.registerCommand(
      Command(
        Array("saveall"),
        null.asInstanceOf[String],
        "Save all known media",
        Flag(Array("@default"), {args =>
          saveAll()
          tellPlayer("Saved Global Storage")
        }))
    )

    commandDispatcher.registerCommand(
      Command(
        Array("loadall"),
        null.asInstanceOf[String],
        "Load all known media",
        Flag(Array("@default"), {args =>
          loadAll()
          tellPlayer("Loaded Global Storage")
        }))
    )

  }

}


