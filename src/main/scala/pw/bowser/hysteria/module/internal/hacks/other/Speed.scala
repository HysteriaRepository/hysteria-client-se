package pw.bowser.hysteria.module.internal.hacks.other

import pw.bowser.hysteria.commands.HysteriaCommand
import pw.bowser.hysteria.commands.companion.Command
import pw.bowser.hysteria.util.commands.ToggleFlag
import pw.bowser.hysteria.config.ConfigurationFlag
import pw.bowser.libhysteria.modules.ModuleManifest
import pw.bowser.hysteria.module.internal.HysteriaModule
import pw.bowser.libhysteria.toggles.TogglesMixin

/**
 * Run like boss
 * Date: 3/21/14
 * Time: 8:20 PM
 */
@ModuleManifest(
  groupId     = "pw.hysteria",
  name        = "Speed",
  version     = "0.1",
  commands    = Array("run", "speed"),
  description = "Increases the player's movement speed by a specified factor"
)
class Speed extends HysteriaModule with TogglesMixin {


  var command: HysteriaCommand = null

  override def initModule(): Unit = {
    super.initModule()
    configuration.setPropertyIfNew("speed.wedge",  1.7F)
    command = Command(Array("run", "speed"), ToggleFlag(Array("@if_none", "t"), this), ConfigurationFlag(Array("w", "wedge"), configuration, "speed.wedge", ConfigurationFlag.Transforms.FLOAT))
  }

  override def enableModule(): Unit = {
    super.enableModule()

    toggleRegistry.enrollToggleable(this)
    commandDispatcher.registerCommand(command)
  }

  override def disableModule(): Unit = {
    super.disableModule()

    toggleRegistry.disenrollToggleable(this)
    commandDispatcher.unRegisterCommand(command)
  }

  // TickReceiver Implementation
  //--------------------------------------------------------------------------------------------------------------------

  def speedWedge: Float = configuration.getFloat("speed.wedge")

  // Toggles Implementation
  //--------------------------------------------------------------------------------------------------------------------

  /**
   * Get the name of the toggleable
   *
   * @return name
   */
  def displayName: String = "Speed"

  /**
   * Should return the distinguished name of the toggles
   * The distinguished name should be unique to the toggles, ie, `pw.bowser.mods.mod.distinguished_name_here`
   * @return
   */
  override def distinguishedName: String = "pw.hysteria.speed"

  override def conflictingToggles: List[String] = List("pw.hysteria.sprint", "pw.hysteria.fly")

  override def statusText: Option[String] = Some(configuration.getFloat("speed.wedge").toString)

}
