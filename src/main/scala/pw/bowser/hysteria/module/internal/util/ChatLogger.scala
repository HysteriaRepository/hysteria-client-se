package pw.bowser.hysteria.module.internal.util

import java.io.File
import java.util.logging._
import pw.bowser.hysteria.module.internal.util.ChatLogger.ServerChatLogger
import pw.bowser.hysteria.events.EventClient
import pw.bowser.hysteria.eventsimpl.chat.ChatMessageReceiveEvent
import pw.bowser.hysteria.chat.Formatting
import java.util.{Date, Calendar}
import java.text.SimpleDateFormat
import pw.bowser.libhysteria.modules.ModuleManifest
import pw.bowser.hysteria.module.internal.HysteriaModule
import pw.bowser.minecraft.servers.ServerData

/**
 * Module that logs chat
 * Date: 4/9/14
 * Time: 8:38 AM
 */
@ModuleManifest(
  groupId       = "pw.bowser",
  name          = "ChatLogger",
  version       = "0.1",
  description   = "Logs all chat messages on a per-server basis"
)
class ChatLogger extends HysteriaModule {

  private val serverDataHook = {(data: ServerData) =>

    getLogger.fine(s"Configuring chat logger for server ${data.mcData.serverIP}")

    val chatLogFile     = new File(data.storageFolder, "chat.log")
    if(!chatLogFile.exists()) chatLogFile.createNewFile()

    val chatLogHandler  = new FileHandler(chatLogFile.getAbsolutePath, true)
    chatLogHandler.setFormatter(ChatLogger.ChatFormatter)

    val chatLogger      = Logger.getLogger(s"Server Chat Logger for ${data.mcData.serverIP}")
    chatLogger.setUseParentHandlers(false)

    chatLogger.addHandler(chatLogHandler)

    data.addHelper(classOf[ServerChatLogger], new ServerChatLogger(chatLogger))

    getLogger.info(s"Chat logfile for ${data.mcData.serverIP} is located at ${chatLogFile.getAbsolutePath}")
    chatLogger.info(s"Logging initialized at ${new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssXXX").format(Calendar.getInstance.getTime)}")
  }

  private val chatReceiveHook = EventClient[ChatMessageReceiveEvent](event => getCurrentChatLogger.info(Formatting.stripFormatting(event.message)))

  // Module Implementation
  //--------------------------------------------------------------------------------------------------------------------

  override def disableModule(): Unit = {
    super.disableModule()

    serverDataManager.removeInitializeHook(serverDataHook)
    eventSystem.unSubscribe(classOf[ChatMessageReceiveEvent], chatReceiveHook)
  }

  override def enableModule(): Unit = {
    super.enableModule()

    serverDataManager.registerInitializeHook(serverDataHook)
    eventSystem.subscribe(classOf[ChatMessageReceiveEvent], chatReceiveHook)
  }

  // Helpers
  //--------------------------------------------------------------------------------------------------------------------

  def getCurrentChatLogger: Logger = currentServer.get.getHelper(classOf[ServerChatLogger]).logger
}
object ChatLogger {
  private[ChatLogger] final class ServerChatLogger(val logger: Logger)

  private[ChatLogger] object ChatFormatter extends Formatter {
    private val dateFormat  = new SimpleDateFormat("'at' hh:mm:ss 'on' yyyy-MM-dd")
    private val muleDate    = new Date

    override def format(record: LogRecord): String = synchronized {
      muleDate.setTime(record.getMillis)

      s"[Chat ${dateFormat.format(muleDate)}] ${record.getMessage}\n"
    }
  }
}