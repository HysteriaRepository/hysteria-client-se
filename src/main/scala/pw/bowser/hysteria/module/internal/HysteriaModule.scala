package pw.bowser.hysteria.module.internal

import pw.bowser.hysteria.engine.{HysteriaServices, HysteriaBroker}
import pw.bowser.hysteria.module.ModuleWithServices
import com.google.inject.{Inject, Injector}
import pw.bowser.hysteria.inject.{Config, PluginRel}
import java.io.File
import pw.bowser.libhysteria.config.Configuration
import pw.bowser.minecraft.wrapper.WrapperConversions

/**
 * Module with more knowledge of hysteria and HysteriaServices
 * Date: 2/4/14
 * Time: 3:00 PM
 */
abstract class HysteriaModule extends ModuleWithServices with HysteriaServices with WrapperConversions {

  @Inject @PluginRel private val dataFolderImpl: File    = null

  @Inject @Config private val configFolderImpl: File = null

  @Inject @PluginRel protected val configuration: Configuration = null

  protected final def injector: Injector = HysteriaBroker.hysteria.injector

  protected final def dataFolder: File    = dataFolderImpl

  protected final def configFolder: File  = configFolderImpl

}
