package pw.bowser.hysteria.module.internal.hacks.world

import pw.bowser.hysteria.commands.companion.Command
import pw.bowser.hysteria.util.commands.ToggleFlag
import pw.bowser.hysteria.events.EventClient
import pw.bowser.hysteria.eventsimpl.network.PacketReceiveEvent
import net.minecraft.network.play.server.S12PacketEntityVelocity
import net.minecraft.client.entity.AbstractClientPlayer
import pw.bowser.libhysteria.modules.ModuleManifest
import pw.bowser.hysteria.module.internal.HysteriaModule
import pw.bowser.libhysteria.toggles.TogglesMixin

/**
 * Date: 3/13/14
 * Time: 3:54 PM
 */
@ModuleManifest(
  groupId     = "pw.bowser",
  name        = "CancelVelocity",
  version     = "0.1",
  commands    = Array("cv"),
  description = "Prevents velocity from being applied to the player"
)
class CancelVelocity extends HysteriaModule with TogglesMixin {
  private val command = Command(Array("cv"), ToggleFlag(Array("@if_none", "t"), this))

  private val packetIncoming = EventClient[PacketReceiveEvent]({packetEvent =>
    packetEvent.packet match {
      case veloPacket: S12PacketEntityVelocity =>
        if(minecraft.theWorld.getEntityByID(veloPacket.entityID).isInstanceOf[AbstractClientPlayer] && isEnabled) // TODO getEntityByID => wrapper
          packetEvent.cancel()
      case _ => // Ignore
    }
  })

  override def enableModule(): Unit = {
    super.enableModule()

    toggleRegistry.enrollToggleable(this)
    commandDispatcher.registerCommand(command)
    eventSystem.subscribe(classOf[PacketReceiveEvent], packetIncoming)
  }

  override def disableModule(): Unit = {
    super.disableModule()

    toggleRegistry.disenrollToggleable(this)
    commandDispatcher.unRegisterCommand(command)
    eventSystem.unSubscribe(classOf[PacketReceiveEvent], packetIncoming)
  }

  /**
   * Get the name of the toggleable
   *
   * @return name
   */
  def displayName: String = "CancelVelocity"

  def distinguishedName: String = "pw.hysteria.cancel_velocity"
}
