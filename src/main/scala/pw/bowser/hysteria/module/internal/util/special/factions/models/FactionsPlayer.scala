package pw.bowser.hysteria.module.internal.util.special.factions.models

import java.io.{OutputStream, InputStream}
import scala.util.parsing.json.JSONObject
import pw.bowser.hysteria.util.scala.json.Formatters
import pw.bowser.libhysteria.storage.marshal.Marshalling

/**
 * Represents a factions player
 * Date: 4/8/14
 * Time: 12:03 PM
 */
case class FactionsPlayer(name: String, title: String, faction: String, factionRank: String) extends Marshalling {
  /**
   * Marshal self to output stream
   *
   * @param stream stream to write to
   */
  def writeTo(stream: OutputStream): Unit = {
    stream.write(JSONObject(Map(
      "name"    -> name,
      "title"   -> title,
      "faction" -> faction,
      "rank"    -> factionRank
    )).toString(Formatters.prettyFormatter).getBytes)
  }

  /**
   * Load self from input stream
   *
   * @param stream stream to read from
   */
  def readFrom(stream: InputStream): Unit = ???

  /**
   * Get the handle of the marshallable (for ref like filename etc)
   *
   * @return handle
   */
  def getHandle: String = s"$name.player.json"
}
