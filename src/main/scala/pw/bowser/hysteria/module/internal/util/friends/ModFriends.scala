package pw.bowser.hysteria.module.internal.util.friends

import pw.bowser.hysteria.events.EventClient
import pw.bowser.util.{MVFLex, JaroWinkler}
import pw.bowser.hysteria.commands.HysteriaCommand
import pw.bowser.hysteria.commands.companion.{Flag, Command}
import pw.bowser.hysteria.config.ConfigurationFlag
import pw.bowser.hysteria.util.commands.ToggleFlag
import pw.bowser.hysteria.chat.Formatting
import net.minecraft.util.{EnumChatFormatting, ChatComponentText}
import pw.bowser.minecraft.ForkedChatLine
import java.util.logging.Level
import pw.bowser.minecraft.players.PlayerFriendship
import scala.collection.JavaConversions.mapAsJavaMap
import pw.bowser.hysteria.eventsimpl.world.ClientPlayerAttackEvent
import net.minecraft.entity.player.EntityPlayer
import java.util.regex.Pattern
import pw.bowser.libhysteria.modules.ModuleManifest
import pw.bowser.hysteria.module.internal.HysteriaModule
import pw.bowser.libhysteria.toggles.Toggles
import pw.bowser.hysteria.eventsimpl.chat.ChatMessageReceiveEvent

/**
 * Date: 2/16/14
 * Time: 3:58 PM
 */
@ModuleManifest(
  groupId     = "pw.hysteria",
  name        = "FriendsList",
  version     = "0.1",
  commands    = Array("friends", "np"),
  description = "Manage friends, and name protection"
)
class ModFriends extends HysteriaModule {

  val nameProtectToggle = Toggles("pw.hysteria.nameprotect", "NameProtect", {}, {})

  val chatMessageReceiptListener = EventClient[ChatMessageReceiveEvent]({event =>

    var compoProcessed = new ChatComponentText(Formatting.RED + "[HYS] Message Hidden due to an Error!")
    try {
      compoProcessed = new ChatComponentText(processChatMessage(event.message))
    } catch {
      case t: Throwable =>
        getLogger.log(Level.WARNING, "Not creating a protected chat message due to an error", t)
    }

    val unsplitLine =
      new ForkedChatLine(minecraft.guiIngame.getUpdateCounter, 0, compoProcessed, event.chatComponent, nameProtectToggle.isEnabled)


    val linesProcessed  = minecraftToolkit.makeChatLines(compoProcessed.getFormattedText)
    val linesUnsafe     = minecraftToolkit.makeChatLines(event.chatComponent.getFormattedText)

    val nextChatLines   = (linesProcessed zip linesUnsafe).map({ case(clean, dirty) =>
      new ForkedChatLine(minecraft.guiIngame.getUpdateCounter, 0,
                         new ChatComponentText(clean), new ChatComponentText(dirty), nameProtectToggle.isEnabled)
    })

    val unsplitSizeNow  = minecraft.guiIngame.getChatGUI.unsplitChatLines.size()
    val splitSizeNow    = minecraft.guiIngame.getChatGUI.splitChatLines.size()

    minecraft.guiIngame.getChatGUI.unsplitChatLines.add(unsplitLine)
    nextChatLines.foreach(minecraft.guiIngame.getChatGUI.splitChatLines.add(0, _))

    if(unsplitSizeNow > 100)
      minecraft.guiIngame.getChatGUI.unsplitChatLines.remove(unsplitSizeNow - 1)
    if(splitSizeNow > 100)
      minecraft.guiIngame.getChatGUI.splitChatLines.remove(splitSizeNow - 1)
  }, 50)

  val chatMessageReceiveCanceller = EventClient[ChatMessageReceiveEvent](_.cancel(), 0)

  val playerAttackOtherReceiver = EventClient[ClientPlayerAttackEvent]({event =>
    if(event.target.isInstanceOf[EntityPlayer] && PlayerDataFor(event.target.asInstanceOf[EntityPlayer].commandSenderName).friendshipOrd == PlayerFriendship.FRIEND) event.cancel()
  })

  var npCommand: HysteriaCommand  = null

  var friendsCommand: HysteriaCommand = null

  var friendsCommandSimple: HysteriaCommand = null

  override def initModule(): Unit = {
    super.initModule()

    configuration.setPropertyIfNew("np.minimum_similarity", 0.80D)

    npCommand = Command(
      Array("np", "nameprotect"),
      ToggleFlag(Array("@if_none", "toggle"), nameProtectToggle),
      ConfigurationFlag(Array("s", "similarity"), configuration, "np.minimum_similarity", {(args: Array[String]) => args(0).toDouble})
    )

    friendsCommand = Command(
      Array("frutil", "fru"),
      Flag(
        Array("@if_none", "list"),
        {args =>
          tellPlayer(
            s"Players: ${playerData.knownPlayerData.values.map(pld => pld.friendshipOrd.prefix + pld.nameShown).mkString(Formatting.RESET + ", ")}"
          )
        },
        null,
        "Lists loaded player data"
      ),
      Flag(
        Array("remove", "r"),
        {args =>
          val pData = playerData.lookupPlayer(args(0))
          if(pData.isModified){
            playerData.storageService.getMediumFor(pData).deleteData()
            pData.reInitializeData()

            tellPlayer(s"Reinitialized PlayerData for ${args(0)}")
          } else tellPlayer(s"Data for ${args(0)} has not been modified")
        },
        "<Real Name>",
        "Remove a player"
      ),
      Flag(
        Array("transprop", "trans", "tp"),
        {args =>
          val pdConfigTrans = playerData.lookupPlayer(args(0)).transientInfo
          if(args.length > 2) pdConfigTrans.put(args(1), args(2))
          tellPlayer(pdConfigTrans.get(args(1)) match {
            case None => s"Property ${Formatting.italicize(args(1))} is not set"
            case value: Some[String] => s"Property ${Formatting.italicize(args(1))} is set to ${Formatting.italicize(value.get)}"
          })
        },
        "<Real Name> <Transient Property Name> [Transient Property Value]",
        "Get(2) or Set(3) a transient string property for the player"
      ),
      Flag(
        Array("removetrans", "rt"),
        {args =>
          val pdConfigTrans = playerData.lookupPlayer(args(0)).transientInfo
          tellPlayer(pdConfigTrans.get(args(1)) match {
            case None => s"Property ${Formatting.italicize(args(1))} is not set"
            case value: Some[String] =>
              pdConfigTrans.remove(args(1))
              s"Property ${Formatting.italicize(args(1))} removed"
          })
        },
        "<Real Name> <Transient Property Name>",
        "Remove a transient property for the player"
      ),
      Flag(
        Array("prop", "p"),
        {args =>
          val pdConfig = playerData.lookupPlayer(args(0)).playerInfo
          if(args.length > 2) pdConfig.put(args(1), args(2))
          tellPlayer(pdConfig.get(args(1)) match {
            case None => s"Property ${Formatting.italicize(args(1))} is not set"
            case value: Some[String] => s"Property ${Formatting.italicize(args(1))} is set to ${Formatting.italicize(value.get)}"
          })
        },
        "<Real Name> <Persistent Property Name> [Persistent Property Value]",
        "Get(2) or Set(3) a persistent string property for the player"
      ),
      Flag(
        Array("rmprop", "rp"),
        {args =>
          val pdConfig = playerData.lookupPlayer(args(0)).playerInfo
          tellPlayer(pdConfig.get(args(1)) match {
            case None => s"Property ${Formatting.italicize(args(1))} is not set"
            case value: Some[String] =>
              pdConfig.remove(args(1))
              s"Property ${Formatting.italicize(args(1))} removed"
          })
        },
        "<Real Name> <Persistent Property Name>",
        "Remove a persistent property for the player"
      ),
      Flag(
        Array("lookup", "l"),
        {args =>
          val pdPlayer = playerData.lookupPlayer(args(0))
          tellPlayer(s"${Formatting.ITALIC} Player ${pdPlayer.nameShown}")
          tellPlayer(if(pdPlayer.isModified) "Player has been modified" else "Player has not been modified")
          tellPlayer(pdPlayer.friendshipOrd match {
            case PlayerFriendship.ENEMY   => s"Player is an ${Formatting.RED + Formatting.ITALIC}Enemy"
            case PlayerFriendship.FRIEND  => s"Player is a ${Formatting.BRIGHT_GREEN + Formatting.ITALIC}Friend"
            case PlayerFriendship.NEUTRAL => s"Player is Neutral"
          })
          tellPlayer(s"Handle: ${args(0)} => ${pdPlayer.nameShown}")
        },
        "<Real Name>",
        "Look up a player (this will also force the player to be loaded from disk, if applicable)"
      ),
      Flag(
        Array("loadtrans", "lt"),
        {args =>
          playerData.configureTransientDataFor(playerData.lookupPlayer(args(0)))
          tellPlayer(s"Began td reload")
        },
        "<Player>",
        "Reload Transient Data for Player"
      ),
      Flag(
        Array("loadperst", "lp"),
        {args =>
          playerData.configurePersistentDataFor(playerData.lookupPlayer(args(0)))
          tellPlayer(s"Began pd reload")
        },
        "<Player>",
        "Reload Persistent Data for Player"
      ),
      Flag(
        Array("lex", "eval"),
        {args =>
          val pd = playerData.lookupPlayer(args(0))
          tellPlayer(s"MVFLex(${args(0)}) = ${MVFLex(args(1),
                     Map(
                       "session" -> mapAsJavaMap(pd.transientInfo),
                       "data" -> mapAsJavaMap(pd.playerInfo),
                       "name" -> pd.nameShown)
                      )
                    }"
          )
        },
        "<Player> <MVFLex Expression>",
        "Evaluate an MVFLex expression in the player context"
      )
    )

    commandDispatcher.registerCommand(
      Command(
        Array("jaro"),
        "<String> <String>",
        "Calculate Jaro-Winkler similarity of two strings.",
        Flag(
          Array("@default", "@if_none"),
          {args =>
            tellPlayer(s"JaroWinkler(${args(0)}, ${args(1)}) = ${JaroWinkler(args(0), args(1))}")
          }
        )
      )
    )

    friendsCommandSimple = Command(
      Array("fr", "nl", "namelist", "friends"),
      Flag(
        Array("@if_none", "list"),
        {args =>
          tellPlayer(
            s"Friends: ${playerData.knownPlayerData.values.filter(_.isModified).map(pld => pld.friendshipOrd.prefix + pld.nameShown).mkString(Formatting.RESET + ", ")}"
          )
        },
        null,
        "Lists loaded player data"
      ),
      Flag(
        Array("nick", "n"),
        {args =>
          val pdConfig = playerData.lookupPlayer(args(0)).playerInfo
          if(args.length > 1) pdConfig.put("player.nickname", args(1))
          tellPlayer(pdConfig.get("player.nickname") match {
            case None => s"Property ${Formatting.italicize(args(0))} is not set"
            case value: Some[String] => s"Property ${Formatting.italicize("player.nickname")} is set to ${Formatting.italicize(value.get)}"
          })
        },
        "<Real Name> [New Nickname]",
        "Get(1) or Set(2) a player's nickname"
      ),
      Flag(
        Array("clearnick", "cn"),
        {args =>
          playerData.lookupPlayer(args(0)).playerInfo.remove("player.nickname")
          tellPlayer("Player's nickname was removed")
        }
      ),
      Flag(
        Array("friendship", "f"),
        {args =>
          val pdConfig = playerData.lookupPlayer(args(0))
          if(args.size > 1)
            pdConfig.playerInfo.put("player.friendship", args(1))

          tellPlayer(s"Player friendship level is `${pdConfig.friendshipOrd.prefix + pdConfig.friendshipOrd.name}${Formatting.RESET}`")
        },
        "<Player> [friend|enemy|neutral]",
        "Get(1) or Set(2) a player's friendship level"
      )
    )

  }

  override def enableModule(): Unit = {
    super.enableModule()

    commandDispatcher.registerCommand(npCommand)
    commandDispatcher.registerCommand(friendsCommand)
    commandDispatcher.registerCommand(friendsCommandSimple)

    eventSystem.subscribe(classOf[ChatMessageReceiveEvent], chatMessageReceiptListener)
    eventSystem.subscribe(classOf[ChatMessageReceiveEvent], chatMessageReceiveCanceller)
    eventSystem.subscribe(classOf[ClientPlayerAttackEvent], playerAttackOtherReceiver)

    toggleRegistry.enrollToggleable(nameProtectToggle)
  }

  override def disableModule(): Unit = {
    eventSystem.unSubscribe(classOf[ChatMessageReceiveEvent], chatMessageReceiptListener)
    eventSystem.unSubscribe(classOf[ChatMessageReceiveEvent], chatMessageReceiveCanceller)
    eventSystem.unSubscribe(classOf[ClientPlayerAttackEvent], playerAttackOtherReceiver)

    commandDispatcher.unRegisterCommand(npCommand)
    commandDispatcher.unRegisterCommand(friendsCommand)
    commandDispatcher.unRegisterCommand(friendsCommandSimple)

    toggleRegistry.disenrollToggleable(nameProtectToggle)

    super.disableModule()
  }

  // Utility Methods
  //--------------------------------------------------------------------------------------------------------------------

  /**
   * Perform lexical processing on chat messages and return fixed data
   * @param cm  chat message
   * @return    lexed message
   */
  def processChatMessage(cm: String): String = {
    var message = cm
    playerData.knownPlayerData.foreach(dPair => message = message.replaceAll(s"(?i)${Pattern.quote(dPair._1)}", dPair._2.nameShown))

    val lexicalComponentsUnsafe = message.split(ModFriends.STRING_GLUE_STRIP)

    val lexicalComponentsSafe = lexicalComponentsUnsafe.map({piece =>
      var pNew = piece

      playerData.knownPlayerData.foreach({dPair =>

        if(dPair._1 != dPair._2.nameShown){

          val strCheck  = EnumChatFormatting.getTextWithoutFormattingCodes(piece).trim
          val strDist   = JaroWinkler(strCheck.toLowerCase, dPair._1.toLowerCase)
          if(strDist >= configuration.getDouble("np.minimum_similarity") && strCheck.length > 0){

            pNew = dPair._2.nameShown

          }

        }

      })

      pNew
    })

    val lexed = lexicalComponentsSafe.mkString("")
    lexed
  }
}

object ModFriends {
  val STRING_GLUE_STRIP = "(?<=[\\s])"
}
