package pw.bowser.hysteria.module.internal.util

import com.google.inject.Inject
import pw.hysteria.input.dashfoo.command.parsing.CommandRegexProvider
import pw.hysteria.input.dashfoo.command.{Command, Flag, IsFlag, CommandDispatcher}
import pw.bowser.hysteria.commands.HysteriaCommand
import pw.bowser.hysteria.engine.HysteriaServices
import pw.bowser.hysteria.chat.Formatting
import java.util
import scala.collection.JavaConversions._
import scala.collection.mutable

class CommandHelp @Inject() (cmf: CommandRegexProvider, cd: CommandDispatcher) extends HysteriaCommand(Array("h", "help"), cmf) with HysteriaServices {

  @IsFlag(
    handles = Array("c", "command"),
    getHelp = "<handle: String>",
    getDescription  = "Lookup a command by its handle"
  )
  def commandLookup(cName: String) {
    val command = cd.getLoadedCommands.find(pair => pair._1 == cName)

    if(command == None) tellPlayer(s"Command `$cName` not found")
    else {

      tellPlayer(s"Command: ${Formatting.ITALIC}${command.get._2.getHandles.mkString(" | ")}")
      if(command.get._2.getHelpProvider.getHelp != null) tellPlayer(s"Usage: ${command.get._2.getHelpProvider.getHelp}")
      if(command.get._2.getHelpProvider.getDescription != null) tellPlayer(command.get._2.getHelpProvider.getDescription)

      command.get._2 match {
        case hc: HysteriaCommand =>
          tellPlayer(s"${Formatting.ITALIC}Flags:")

          val flagsSoFar = new mutable.ArrayBuffer[Flag]()
          hc.flagger.getFlags.foreach({fpair =>
            if(!flagsSoFar.contains(fpair._2)){
              var helpFor = s"> ${fpair._2.getHandles.mkString(", ")} "
              if(fpair._2.getHelpProvider != null){
                if(fpair._2.getHelpProvider.getHelp != null) helpFor += s"${fpair._2.getHelpProvider.getHelp} "
                if(fpair._2.getHelpProvider.getDescription != null) helpFor += s"- ${fpair._2.getHelpProvider.getDescription}"
              }

              tellPlayer(helpFor)
            }
            flagsSoFar.append(fpair._2)
          })
        case _ =>
          tellPlayer(s"${Formatting.GREY + Formatting.ITALIC}Command does not support flag inspection")
      }

    }
  }

  @IsFlag(
    handles = Array("@if_none", "list"),
    getDescription = "List all known commands"
  )
  def listCommands() {
    var commandsList = "Commands: "
    val commandsListed = new mutable.ArrayBuffer[Command]
    cd.getLoadedCommands.foreach({pair =>
      if(!commandsListed.contains(pair._2)) commandsList += pair._1 + ", "
      commandsListed.append(pair._2)
    })
    tellPlayer(commandsList)
  }

  @IsFlag(
    handles = Array("h", "help"),
    getDescription = "Get help"
  )
  def getHelpWith() {
    commandLookup("help")
  }

  def getDescription: String = "Lists and looks up commands. Use the -c flag to specify the command to look up"

  def getHelp: String = null

}
