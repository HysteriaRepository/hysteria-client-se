package pw.bowser.hysteria.module.internal.hacks.world

import pw.bowser.hysteria.module.internal.HysteriaModule
import pw.bowser.hysteria.util.commands.ToggleFlag
import net.minecraft.client.Minecraft
import pw.bowser.hysteria.gui.overlay.{TextHUDPosition, TextHUDBlock}
import scala.collection.JavaConversions._
import net.minecraft.entity.player.EntityPlayer
import pw.bowser.hysteria.gui.overlay.TextHUDPosition.TextHUDPosition
import pw.bowser.hysteria.engine.HysteriaServices
import pw.bowser.hysteria.commands.companion.{Flag, Command}
import pw.bowser.util.{MCPlayerContext, MVFTemplate}
import pw.bowser.hysteria.config.ConfigurationFlag
import pw.bowser.hysteria.events.EventClient
import pw.bowser.hysteria.eventsimpl.world.{WorldPreExitEvent, WorldEnterEvent}
import java.util.ConcurrentModificationException
import pw.bowser.hysteria.chat.Formatting
import pw.bowser.libhysteria.modules.ModuleManifest
import pw.bowser.libhysteria.toggles.TogglesMixin

/**
 * Player radar, simply put
 * Date: 2/13/14
 * Time: 3:12 PM
 */
@ModuleManifest(
  groupId     = "pw.hysteria",
  name        = "TextRadar",
  description = "Text radar that lists nearby players",
  version     = "2.7",
  commands    = Array("radar", "tr")
)
class ModRadar extends HysteriaModule with TogglesMixin {

  // Fields
  //--------------------------------------------------------------------------------------------------------------------

  var block: RadarTextBlock = null

  val cacheJob = new RadarCacheJob(minecraft, this)
  var cacheThread: Option[Thread] = None

  val eventOnEnterWorld =
    EventClient[WorldEnterEvent]({
      e =>
        cacheJob.startJob()
        cacheThread = Some(new Thread(cacheJob, "Radar Cache Thread"))
        cacheThread.get.start()
    })

  val eventOnBeforeExitWorld =
    EventClient[WorldPreExitEvent]({
      e =>
        println(cacheThread)
        cacheThread match {
        case t: Some[Thread] =>
          cacheJob.endJob()
          t.get.join()
          cacheThread = None
        case None => getLogger.severe("The RadarCacheJob was not active before a world leave!")
      }
    })

  // Module
  //--------------------------------------------------------------------------------------------------------------------


  val defaultFormat =
    "@{c_grey}" +
      "@if{tData['fbans'] != null}" +
      "@{tData.fbans.isBanned() ? 'B: ' + tData.fbans.?totalBans() : ''}" +
      "@{tData.fbans.hasAlts()  ? 'A: ' + tData.fbans.?totalAlts() : ''}" +
      "@end{}" +
      "@{player.?vanished() ? c_gold + '(Invisible)' : ''}" +
      " @{info.friendshipOrd().prefix()}@{info.nameShown()} @{c_reset}- @{player.distance()}"

  override def initModule(){
    super.initModule()

    configuration.setPropertyIfNew("radar.corner",   "top.right")
    configuration.setPropertyIfNew("radar.order",    3)
    configuration.setPropertyIfNew("radar.format",   defaultFormat)
    configuration.setPropertyIfNew("radar.max_dist", 100)

    block = new RadarTextBlock(cacheJob, configuration.getInt("radar.order"))

    commandDispatcher.registerCommand(
      Command(
        Array("radar", "tradar", "tr"),
        ToggleFlag(Array("@if_none", "t"), this),
        Flag(
        Array("prio"), {
          args =>
            configuration.setProperty("radar.order", args(0).toInt)
            block.theSortOrder = configuration.getInt("radar.order")
            headsUpDisplay.add(enumBlockPosition, block)
            tellPlayer(s"Radar sort order set to ${args(0)}")
        },
        "<Level: Int>",
        "Set the sort level for the block"
        ),
        Flag(
        Array("pos"), {
          args =>
            args(0) match {
              case "top.left" | "top.right" | "bottom.left" | "bottom.right" =>
                headsUpDisplay.remove(enumBlockPosition, block)
                configuration.setProperty("radar.corner", args(0))
                headsUpDisplay.add(enumBlockPosition, block)
              case _ =>
                tellPlayer(s"${args(0)} is not a valid corner")
            }
        },
        "<Position: top.left|top.right|bottom.left|bottom.right>",
        "Set the overlay corner in which the block will reside"
        ),
        ConfigurationFlag(Array("f", "format"),   configuration, "radar.format"),
        ConfigurationFlag(Array("d", "distance"), configuration, "radar.max_dist", ConfigurationFlag.Transforms.INT)
      )
    )
  }

  override def enableModule() {
    super.enableModule()

    eventSystem.subscribe(classOf[WorldEnterEvent], eventOnEnterWorld)
    eventSystem.subscribe(classOf[WorldPreExitEvent], eventOnBeforeExitWorld)

    toggleRegistry.enrollToggleable(this)
  }

  override def disableModule() {
    toggleRegistry.disenrollToggleable(this)

    eventSystem.unSubscribe(classOf[WorldEnterEvent], eventOnEnterWorld)
    eventSystem.unSubscribe(classOf[WorldPreExitEvent], eventOnBeforeExitWorld)

    super.disableModule()
  }

  // Formatting
  //--------------------------------------------------------------------------------------------------------------------

  def format: String = configuration.getString("radar.format")
  def maxDistance: Int = configuration.getInt("radar.max_dist")

  // Utility
  //--------------------------------------------------------------------------------------------------------------------

  def enumBlockPosition: TextHUDPosition = configuration.getString("radar.corner") match {
    case "top.left"       => TextHUDPosition.TOP_LEFT
    case "bottom.left"    => TextHUDPosition.BOTTOM_LEFT
    case "bottom.right"   => TextHUDPosition.BOTTOM_RIGHT
    case "top.right" | _  => TextHUDPosition.TOP_RIGHT
  }

  // Toggles mixin
  //--------------------------------------------------------------------------------------------------------------------

  override def enable(){
    headsUpDisplay.add(enumBlockPosition, block)
  }

  override def disable(){
    headsUpDisplay.remove(enumBlockPosition, block)
  }

  /**
   * Get the name of the toggleable
   *
   * @return name
   */
  def displayName: String = "TextRadar"

  /**
   * Should return the distinguished name of the toggles
   * The distinguished name should be unique to the toggles, ie, `pw.bowser.mods.mod.distinguished_name_here`
   * @return
   */
  override def distinguishedName: String = "pw.hysteria.text_radar"
}

// Radar Cache Thread
//----------------------------------------------------------------------------------------------------------------------

/**
 * Constructs the actual radar 'list' from the world
 *
 * @param mc  minecraft instance
 * @param mod module instance
 */
final class RadarCacheJob(mc: Minecraft, mod: ModRadar) extends Runnable {

  private var textCache = Array[String]()

  private var running = true

  def run(): Unit = while (running) {

    try {
      if (mc.theWorld != null) {
        textCache = mc.theWorld.loadedEntityList
          .filter(pl => pl.isInstanceOf[EntityPlayer])
          .filter(pl => pl != mc.thePlayer)
          .filter(pl => pl.asInstanceOf[EntityPlayer].isEntityAlive)
          .filter(pl => pl.asInstanceOf[EntityPlayer].getDistanceToEntity(mc.thePlayer) <= mod.maxDistance)
          .map(pl => MVFTemplate(mod.format, MCPlayerContext(pl.asInstanceOf[EntityPlayer])))
          .toArray

        Thread.sleep(20L)
      }
      else Thread.sleep(5000L)
    } catch {
      case _: ConcurrentModificationException => // We read when an entity was added to the world. don't give a shit.
      case t: Throwable =>
        textCache = Array(Formatting.GOLD + "Radar Thread Error")
        t.printStackTrace()
        Thread.sleep(8000L)
    }

  }

  // Utility
  //--------------------------------------------------------------------------------------------------------------------

  def distanceTo(e: EntityPlayer): Double = mc.thePlayer.getDistanceToEntity(e)

  // Interaction
  //--------------------------------------------------------------------------------------------------------------------

  def text: Array[String] = textCache

  def startJob() = running = true

  def endJob() = running = false


}

// Radar Itself
//----------------------------------------------------------------------------------------------------------------------

/**
 * Renders radar data cached by the cache thread
 *
 * @param cacheJob  cache job
 * @param theSortOrder sorting order
 */
final class RadarTextBlock(cacheJob: RadarCacheJob, var theSortOrder: Int) extends TextHUDBlock with HysteriaServices {

  /**
   * Lines of text belonging to the block
   *
   * @return lines
   */
  def blockLines: Array[String] = cacheJob.text

  def colour: Int = 0xFFFFFF

  /**
   * Position priority (upward) in which the block would like to appear
   *
   * @return sort order
   */
  def sortOrder: Int = theSortOrder
}
