package pw.bowser.hysteria.module.internal.hacks.other

import pw.bowser.hysteria.commands.HysteriaCommand
import pw.bowser.hysteria.commands.companion.Command
import pw.bowser.hysteria.util.commands.ToggleFlag
import pw.bowser.hysteria.minecraft.TickReceiver
import net.minecraft.network.play.client.C03PacketPlayer
import net.minecraft.init.Blocks
import pw.bowser.libhysteria.modules.ModuleManifest
import pw.bowser.hysteria.module.internal.HysteriaModule
import pw.bowser.libhysteria.toggles.TogglesMixin

/**
 * WaterWalk
 * @author zencantsnipe
 * Date: 5/04/14
 * Time: 4:20 PM
 */

@ModuleManifest(
  groupId     = "pw.hysteria.zencantsnipe",
  name        = "WaterWalk",
  version     = "0.1",
  commands    = Array("ww", "jesus"),
  description = "Makes liquids into solids and sets the player slightly below the surface to bypass NCP."
)
class WaterWalk extends HysteriaModule with TogglesMixin with TickReceiver{

  var command: HysteriaCommand = Command(Array("ww", "jesus"), ToggleFlag(Array("@if_none", "t"), this))

  def onTick(): Unit = if (minecraft.theWorld != null
                            && minecraft.theWorld.getBlockAt(minecraft.thePlayer.posX.toInt - 1,
                                                            minecraft.thePlayer.posY.toInt - 2,
                                                            minecraft.thePlayer.posZ.toInt - 1) == Blocks.water) {

      minecraftToolkit.sendPacket(new C03PacketPlayer.C06PacketPlayerPosLook(
        minecraft.thePlayer.posX, minecraft.thePlayer.posY - 0.0000001, minecraft.thePlayer.posZ,
        minecraft.thePlayer.rotationYaw, minecraft.thePlayer.rotationPitch, false))

      minecraftToolkit.sendPacket(new C03PacketPlayer.C04PacketPlayerPosition(
        minecraft.thePlayer.posX, minecraft.thePlayer.posY - 0.0000001, minecraft.thePlayer.posZ, false))

  }

  override def enableModule(): Unit = {
    super.enableModule()
    toggleRegistry.enrollToggleable(this)
    commandDispatcher.registerCommand(command)
  }

  override def disableModule(): Unit = {
    super.disableModule()
    toggleRegistry.disenrollToggleable(this)
    commandDispatcher.unRegisterCommand(command)
  }

  /*
  debugging
   */
  //override def canPersist: Boolean = false

  // Toggles Implementation
  //--------------------------------------------------------------------------------------------------------------------

  /**
   * Get the name of the toggleable
   *
   * @return name
   */
  def displayName: String = "WaterWalk"

  /**
   * Should return the distinguished name of the toggles
   * The distinguished name should be unique to the toggles, ie, `pw.bowser.mods.mod.distinguished_name_here`
   * @return
   */
  override def distinguishedName: String = "pw.hysteria.zencantsnipe.water_walk"

  override def enable(): Unit = {
    tickService.register(this)
  }

  override def disable(): Unit = {
    tickService.remove(this)
  }

  override def conflictingToggles: List[String] = List("pw.hysteria.auto_swim")
}
