package pw.bowser.hysteria.util.gson

import com.google.gson._
import java.lang.reflect.Type

/**
 * Date: 1/29/14
 * Time: 9:18 AM
 */
class OptionSerializer[A <: AnyRef](val optType: Class[A] = classOf[AnyRef]) extends JsonSerializer[Option[A]] with JsonDeserializer[Option[A]] {

  def serialize(p1: Option[A], p2: Type, p3: JsonSerializationContext): JsonElement = {
    p1 match {
      case Some(_)  => p3.serialize(p1.get)
      case None     => JsonNull.INSTANCE
    }
  }

  def deserialize(p1: JsonElement, p2: Type, p3: JsonDeserializationContext): Option[A] = {

    if(p1.isInstanceOf[JsonNull]){
      None
    } else {
      val data: A = p3.deserialize(p1, optType)
//      println(data)
      Some(data)
    }

//    val mapJ: JsonObject        = p1.getAsJsonObject
//    val optKind: JsonPrimitive  = mapJ.getAsJsonPrimitive("kind")
//
//    optKind.getAsString match {
//      case OptionType.SOME  =>
//        val valueData: A  = p3.deserialize(mapJ.get("value"), optType)
//        Option(valueData)
//      case OptionType.NONE  => None
//    }
  }

  object OptionType {

    val SOME: String = "Some"
    val NONE: String = "None"

  }

}
