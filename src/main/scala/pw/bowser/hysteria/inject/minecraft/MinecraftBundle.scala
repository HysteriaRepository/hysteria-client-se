package pw.bowser.hysteria.inject.minecraft

import com.google.inject.AbstractModule
import pw.bowser.hysteria.minecraft.{TickGroup, RenderQueue}
import net.minecraft.client.Minecraft
import pw.bowser.hysteria.keyboard.BindingManager
import pw.bowser.minecraft.MinecraftToolkit
import pw.bowser.hysteria.minecraft.abstractionlayer.HysteriaMinecraftToolkit

/**
 * Holds context data for minecraft renderables
 *
 * Date: 1/29/14
 * Time: 10:26 PM
 */
class MinecraftBundle(val mc: Minecraft) extends AbstractModule {

  val globalRenderQueue: RenderQueue = new RenderQueue
  val playingRenderQueue: RenderQueue = new RenderQueue
  val idleRenderQueue: RenderQueue = new RenderQueue
  val dimensionRenderQueue: RenderQueue = new RenderQueue

  val tickGroup: TickGroup = new TickGroup

  def configure(): Unit = {

    /*
     * Minecraft itself
     */

    bind(classOf[Minecraft]) toInstance mc

    /*
     * Render task queues
     */

    bind(classOf[RenderQueue]) toInstance globalRenderQueue
    bind(classOf[RenderQueue]) annotatedWith classOf[AnyState] toInstance globalRenderQueue
    bind(classOf[RenderQueue]) annotatedWith classOf[InGame] toInstance playingRenderQueue
    bind(classOf[RenderQueue]) annotatedWith classOf[OutOfGame] toInstance idleRenderQueue
    bind(classOf[RenderQueue]) annotatedWith classOf[Dimension] toInstance dimensionRenderQueue

    /*
     * Keyboard
     */

    bind(classOf[BindingManager]) toInstance new BindingManager

    /*
     * Tick groups
     */

    bind(classOf[TickGroup]) toInstance tickGroup

    /*
     * Engine interaction abstraction layer
     */

    bind(classOf[MinecraftToolkit]) to classOf[HysteriaMinecraftToolkit]

  }
}
