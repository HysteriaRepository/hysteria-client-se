package pw.bowser.hysteria.keyboard

import scala.collection.mutable
import org.lwjgl.input.Keyboard

/**
 * Manages the state of many `KeyBindings`
 *
 * Date: 2/3/14
 * Time: 5:14 PM
 */
final class BindingManager {

  private val bindings: mutable.Set[KeyBinding] = new mutable.HashSet[KeyBinding]()

  def registerBinding(binding: KeyBinding)    = bindings.add(binding)
  def deRegisterBinding(binding: KeyBinding)  = bindings.remove(binding)

  /**
   * Queries the state of the keyboard as it relates to the key sequences of key bindings
   */
  def queryKeyState() = bindings.foreach({binding =>

    /*
     * Check to see if the binding is in timeout
     * If it is not, process it
     */

    if(binding.timeoutCounter == 0){

      /*
       * Depress bindings
       */

      if(binding.keySequence.forall(Keyboard.isKeyDown)){

        /*
         * Check to see if the binding is a toggleable binding or not
         * If it is, invert the state of the binding
         *
         * Otherwise, set the state of the binding to PRESSED
         */
        if(binding.isToggle) binding.state match {
            case BindingState.PRESSED   => binding.setState(BindingState.RELEASED)
            case BindingState.RELEASED  => binding.setState(BindingState.PRESSED)
          }
        else binding.setState(BindingState.PRESSED)

        /*
         * Fill the timeout counter so that we don't rapidly engage key bindings
         */
        binding.timeoutCounter = binding.maxTimeout

        /*
         * Alert the binding to the fact that it has been depressed
         */
        binding.onDepress()

        /*
         * If we wanted to, we could exit the method on the first positive binding match, but that could be bad
         */
      }

      /*
       * Release bindings
       */

      if(!binding.keySequence.forall(Keyboard.isKeyDown)){
        /*
         * Set the binding state to RELEASED if it is not toggleable
         */
        if(!binding.isToggle) binding.setState(BindingState.RELEASED)
      }

    } else
      /*
       * Decrement the timeout counter so that it will eventually zero out
       */
      binding.timeoutCounter -= 1

  })

}
