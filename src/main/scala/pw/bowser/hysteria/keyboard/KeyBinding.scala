package pw.bowser.hysteria.keyboard

import pw.bowser.hysteria.keyboard.BindingState.BindingState


/**
 * A KeyBinding
 *
 * Date: 2/3/14
 * Time: 11:37 AM
 */
abstract class KeyBinding(private val seq: Array[Int]) {

  /**
   * Sequence of keys that must be engaged
   *
   * @return sequence
   */
  final def keySequence: Array[Int] = seq

  /**
   * Get a description of the key binding
   *
   * @return description
   */
  def description: String

  /**
   * Invoke the key binding action
   */
  def onDepress(): Unit

  //--------------------------------------------------------------------------------------------------------------------

  def isToggle: Boolean = false

  private[keyboard] var timeoutCounter: Int = 0

  def maxTimeout: Int = 0

  //--------------------------------------------------------------------------------------------------------------------

  private var bindingState: BindingState = BindingState.RELEASED

  private[keyboard] def setState(state: BindingState): Unit = bindingState = state

  def state: BindingState = bindingState

}

object KeyBinding {

  def apply(ch: Int, action: => Unit): KeyBinding = apply(ch, action, "Anonymous Binding")

  def apply(ch: Int, action: => Unit, desc: String): KeyBinding = apply(Array(ch), action, desc)

  def apply(seq: Array[Int], action: => Unit): KeyBinding = apply(seq, action, "Anonymous Binding")

  def apply(seq: Array[Int], action: => Unit, desc: String): KeyBinding = new KeyBinding(seq) {
      /**
       * Get a description of the key binding
       *
       * @return description
       */
      def description: String = desc

      /**
       * Invoke the key binding action
       */
      def onDepress(): Unit = action
    }

}

object BindingState extends Enumeration {
  type BindingState = Value
  val RELEASED, PRESSED = Value
}
