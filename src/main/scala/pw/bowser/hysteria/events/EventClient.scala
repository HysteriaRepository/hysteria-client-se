package pw.bowser.hysteria.events

/**
 * Shorthand event creation
 * Date: 2/1/14
 * Time: 11:37 PM
 */
object EventClient {

  def apply[ET <: Event](clientImpl: ET => Unit, priority: Int = 1): Client[ET] = {
    new Client[ET] {
      def processEvent(event: ET): Unit = clientImpl(event)

      def getClientPriority: Int = priority
    }
  }

}
