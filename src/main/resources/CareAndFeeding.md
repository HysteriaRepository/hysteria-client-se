Care And Feeding of your Hysteria Client v8
===========================================

Hysteria8 is a highly advanced and complicated framework and minecraft hack. 
With fully programmatic and pluggable storage media systems and other wonderful features, things can get very complicated.
  
Right now you're probably thinking 'Fuck that shit!! How do I the hacks?". And that's the point of this document.

How do you the hax?
===================

Hysteria8 is very different from most other things of this sort in that it is command-oriented.
While a module could add a key-binding, most mods will add a command. In order to bind a key to a mod, you would instead bind your key sequence to the toggle command of the module.

But how? Well it's quite simple. Let's say you want to use FullBright (`-fb`).

Depending on which hand you prefer to pleasure yourself with whilst operating your computer, you should carefully select the optimal key or key-sequence for your binding.

Let's say you finger your asshole with your right index finger and ring finger. You probably won't want to use anything on the right-hand portion of the keyboard (unless, of course, you smoke crack).
I've chosen "B" based on these parameters, but you can realistically have as many keys in a sequence as your keyboard and medium will support, like `NONE+CONTROL_L+ALT_L+C+1+2+F+COMMA+SHIFT_R`.
In order to bind "B" to `fb`, do the following:

    -kb -s B "-fb"

It's very important that you place the quote symbols around `-fb` (assuming that you are some idiot that doesn't RTFM and do not have custom command formatting set up), otherwise the command parser will think that you mean something else and immediately file suit against for for sexual harassment.	

The client should now say something to the effect of

    [HYS] Sequence *B* bound to *-fb*

Now you can sit at your computer whilst toggling fullbright and fingering your asshole (with your **right** hand, naturally) because you feel like AVO.
`-fb` is one of many commands that you will probably want to use.

Oh, and FullBright isn't persistent, so it will not be on if you close the client with it on. Please do not bitch to me about this, as I am too lazy to create a safe way to turn it on when starting the client.
If you want to bitch and moan so much, please decompile Hysteria8.jar, modify ModFullBright.scala and set canPersist to true.

Here are a few commands that you should know. If you read and understand the list, you won't feel so bad about not RTFM.

* -saveall - Saves fucking everything. Yes, fucking everything. No exceptions.
* -loadall - Loads fucking everything. Yes, this will overwrite your current settings if you made any modifications without leaving the world, the game, or running -saveall.
* -help - The help command
   * -help -l <cmd>	- Get help for a command. Don't forget to leave the handle off (`-fb` becomes `fb`).
* -tr - Radar
* -fr - Manages friends. Please `-help -c fr`
* -frutil - Please, don't bother asking with help for this.
* -np - Toggle nameprotect
    * -np -s [double] - Set the Jaro-Winkler index minimum for nameprotect error correction in chat.
* -nf - No, this will never work with NoCheat.
* -ii - Throw out tat you don't want. `-help -c ii`.
* -cv - This works because MyPictures is a fucking idiot.
* -gl - Glide. NoCheat-friendly.
* -fbans - Be like YiffCraft.

Care and Feeding of your Koopa
==============================

If you appreciate the client, and you will (please do not call me on skype at 3 AM and beg for features), do the following.

1. Go to the bank and withdraw €100,00.
2. Use €25,37 to purchase a goat from an impoverished southeast asian farmer.
3. Dye the goat pink, and name it something saucy.
4. Ignore the last three steps.
5. Remove all of your clothes.
6. Buy a DSLR.
7. Use said DSLR to take a full selfie.
8. Send said photo `king@bowser.pw`
9. Catch a plane to Stockholm, and leave the camera in post drop box #6969.
10. Drop by Bowser's apartment ;).
