package pw.bowser.hysteria.module;

import com.google.inject.Inject;
import pw.bowser.hysteria.inject.PluginRel;
import pw.bowser.libhysteria.modules.Module;
import pw.bowser.libhysteria.modules.ModuleException;
import pw.bowser.libhysteria.modules.ModuleInitializationException;
import pw.bowser.libhysteria.storage.marshal.registrar.StorageService;

/**
 * Module that is instantiated with knowledge of given module services and additional services
 *
 * Date: 1/23/14
 * Time: 7:59 PM
 */
public abstract class ModuleWithServices extends Module {

    @Inject
    private StorageService storageService = null;

    @Inject
    @PluginRel
    private StorageService arbStorage = null;

    @Override
    public void initModule() throws ModuleInitializationException {
        if(storageService != null){
            storageService.load();
        }
    }

    @Override
    public void enableModule() throws ModuleException {
        if(storageService != null){
            storageService.load();
        }
    }


    @Override
    public void disableModule() throws ModuleException {
        if(storageService != null){
            storageService.save();
        }
    }


    protected final StorageService getStorageService(){
        if(storageService == null) throw new IllegalAccessError("StorageService is unavailable.");
        return storageService;
    }


    protected final StorageService getDataStorageService(){
        if(arbStorage == null) throw new IllegalAccessError("StorageService is unavailable");
        return arbStorage;
    }

}
