package pw.bowser.hysteria.minecraft;

import java.util.ArrayList;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * Calls a bunch of {@link TickReceiver}s
 * Date: 1/23/14
 * Time: 10:35 AM
 */
public final class TickGroup implements TickReceiver {

    private ArrayList<TickReceiver> children    = new ArrayList<>();
    private Queue<TickReceiver> joinQueue       = new ArrayBlockingQueue<>(10);
    private Queue<TickReceiver> leaveQueue      = new ArrayBlockingQueue<>(10);

    public void register(TickReceiver receiver){
        joinQueue.add(receiver);
    }

    public void remove(TickReceiver receiver){
        leaveQueue.add(receiver);
    }

    @Override
    public void onTick() {
        // Add new friends from the queue
        while (!joinQueue.isEmpty()) {
            TickReceiver joining = joinQueue.remove();
            if(!children.contains(joining)) children.add(joining);
        }

        // Remove elements
        while(!leaveQueue.isEmpty()) {
            children.remove(leaveQueue.remove());
        }

        // Call TickReceivers
        for(TickReceiver child : children) child.onTick();
    }
}
