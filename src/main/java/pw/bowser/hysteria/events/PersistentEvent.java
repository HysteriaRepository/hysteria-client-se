package pw.bowser.hysteria.events;

/**
 * Event that cannot be cancelled
 *
 * Date: 1/16/14
 * Time: 10:14 PM
 */
public abstract class PersistentEvent extends Event {

    @Override
    public final void cancel() {
        throw new UnsupportedOperationException("Event may not be cancelled");
    }

}
