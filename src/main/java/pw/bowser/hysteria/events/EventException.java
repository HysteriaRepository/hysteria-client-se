package pw.bowser.hysteria.events;

/**
 * Describes an error that occurred while processing an event
 * Date: 1/16/14
 * Time: 8:32 PM
 */
public class EventException extends Exception {

    public final Event event;

    public EventException(String desc, Event rel){
        this(desc, null, rel);
    }

    public EventException(String desc, Exception why, Event rel){
        super(desc, why);
        event = rel;
    }

}
