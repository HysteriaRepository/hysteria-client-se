package pw.bowser.hysteria.events;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Default event hub. Synchronized.
 *
 * Date: 1/16/14
 * Time: 3:14 PM
 */
public class EventHub {

    protected final Map<Class<? extends Event>, ArrayList<Client<?>>> subscribers;

    public EventHub(){
        this(new HashMap<Class<? extends Event>, ArrayList<Client<?>>>());
    }

    protected EventHub(Map<Class<? extends Event>, ArrayList<Client<?>>> mapImpl){
        this.subscribers = mapImpl;
    }

    /**
     * Broadcast an event
     * @param e event
     * @throws EventException when an error occurs during broadcast
     */
    @SuppressWarnings("unchecked") // We know we won't have an issue
    public <ET extends Event> ET broadcast(ET e) throws EventException {
        if(subscribers.containsKey(e.getClass())){
            for(Client c : subscribers.get(e.getClass())){
                if(e.isCancelled()) break;
                c.processEvent(e);
            }
        }

        return e;
    }

    public final <T extends Event> void subscribe(Class<T> event, Client<T> client) {
        if(!subscribers.containsKey(event)) subscribers.put(event, new ArrayList<Client<?>>());
        subscribers.get(event).add(client);
        sortClients(event);
    }

    public final <T extends Event> void unSubscribe(Class<T> event, Client<T> client) {
        if(subscribers.containsKey(event)){
            subscribers.get(event).remove(client);
            if(subscribers.get(event).isEmpty()) subscribers.remove(event);
        }
        sortClients(event);
    }

    public final void unSubscribeAll(Class<? extends Event> event){
        if(subscribers.containsKey(event)) subscribers.remove(event);
    }

    /**
     * Subscribe all annotated methods in the class
     *
     * @param target class to search
     */
    public void subscribe(Object target){

        for(Method checking : target.getClass().getMethods()) {
            if(checking.isAnnotationPresent(OnEvent.class)){
                OnEvent anno = checking.getAnnotation(OnEvent.class);
                if(!subscribers.containsKey(anno.eventClass())) subscribers.put(anno.eventClass(), new ArrayList<Client<?>>());
                subscribers.get(anno.eventClass()).add(Client.MethodClient.fromMethod(checking, target, anno.eventClass(), anno.priority()));
            }
        }

        sortAllClients();

    }

    public final void sortAllClients(){
        for(Class<? extends Event> clazz : subscribers.keySet()) sortClients(clazz);
    }

    protected void sortClients(Class<? extends Event> eClass){
        if(subscribers.get(eClass) != null)
            Collections.sort(subscribers.get(eClass), new Client.ClientComparator());
    }


}
