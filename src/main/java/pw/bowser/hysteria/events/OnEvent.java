package pw.bowser.hysteria.events;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotates methods that may receive event messages
 *
 * Date: 1/16/14
 * Time: 9:40 PM
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface OnEvent {

    /**
     * Class of the event received by the annotated method
     *
     * @return event class
     */
    Class<? extends Event> eventClass();

    /**
     * Event priority
     *
     * @return priority
     */
    int priority() default 0;

}
