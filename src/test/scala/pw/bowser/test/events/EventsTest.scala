package pw.bowser.test.events

import org.junit.Test
import pw.bowser.hysteria.events._

/**
 * Event system test
 *
 * Date: 2/1/14
 * Time: 11:48 PM
 */
class EventsTest {

  @Test def testEventHub() {
    val eventExchange: EventHub = new EventHub()

    var eventGot = false

    val client: Client[TestEventNormal] = new Client[TestEventNormal] {
      def processEvent(event: EventsTest.this.type#TestEventNormal): Unit = eventGot = true

      def getClientPriority: Int = 1
    }
    
    eventExchange.subscribe(classOf[TestEventNormal], client)
    
    eventExchange.broadcast(new TestEventNormal)
    
    assert(eventGot)
  }

  @Test def testEventCanCancel() {
    val eventExchange: EventHub = new EventHub()

    var eventGot = false

    /*
     * Client0 will receive the event and then cancel it
     */

    val client0: Client[TestEventNormal] = new Client[TestEventNormal] {
      def processEvent(event: TestEventNormal): Unit = event.cancel()

      def getClientPriority: Int = 1
    }

    /*
     * Client1 should never receive the event if we cancel it
     */

    val client1: Client[TestEventNormal] = new Client[TestEventNormal] {
      def processEvent(event: TestEventNormal): Unit = eventGot = true

      def getClientPriority: Int = 0
    }

    /*
     * Adding the clients should sort the clients
     */
    
    eventExchange.subscribe(classOf[TestEventNormal], client0)
    eventExchange.subscribe(classOf[TestEventNormal], client1)

    /*
     * Broadcast the event
     */
    
    val event = new TestEventNormal
    
    eventExchange.broadcast(event)

    /*
     * Test that we did not get the event, and that the event was cancelled
     */
    
    assert(eventGot == false)
    assert(event.isCancelled)
  }

  @Test(expected = classOf[UnsupportedOperationException]) def testEventPersistent() {
    val eventExchange: EventHub = new EventHub()

    val client: Client[TestEventPersistent] = new Client[TestEventPersistent] {
      def getClientPriority: Int = 1

      def processEvent(event: TestEventPersistent): Unit = event.cancel()
    }

    eventExchange.subscribe(classOf[TestEventPersistent], client)

    eventExchange.broadcast(new TestEventPersistent)
  }

  @Test def testPrioritySorting() {
    val eventExchange: EventHub = new EventHub()

    var first   = false
    var second  = false
    var third   = false

    val client1: Client[TestEventNormal] = EventClient({e => assert(first && second); third = true}, 1)
    val client2: Client[TestEventNormal] = EventClient({e => assert(first); second = true}, 2)
    val client3: Client[TestEventNormal] = EventClient({e => first = true}, 3)

    eventExchange.subscribe(classOf[TestEventNormal], client1)
    eventExchange.subscribe(classOf[TestEventNormal], client2)
    eventExchange.subscribe(classOf[TestEventNormal], client3)

    eventExchange.broadcast(new TestEventNormal)

    assert(first && second && third)
  }

  @Test def testSubscribeClass() {
    val eventExchange: EventHub = new EventHub()
    val clientClass = new EventAnnotationTest
    eventExchange.subscribe(clientClass)

    eventExchange.broadcast(new TestEventNormal)

    assert(clientClass.eventReceived)
  }


  class TestEventNormal extends Event {
    def getName: String = "Normal Test Event"
  }

  class TestEventPersistent extends PersistentEvent{
    def getName: String = "Persistent Test Event"
  }
  
  class EventAnnotationTest {
    
    var eventReceived = false
    
    @OnEvent(eventClass = classOf[TestEventNormal])
    def onEvent(event: Event)= eventReceived = true
    
  }

}
